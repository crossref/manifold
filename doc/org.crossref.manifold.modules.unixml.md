# Package org.crossref.manifold.modules.unixml

Ingest Items and their metadata from Crossref UNIXML and UNIXSD.

This uses the Cayenne parser, which translates Crossref UNIXML into Item Trees. Currently it is run as a command-line
tool by supplying the a .tar.gz snapshot file to the `UNIXML_INGEST_SNAPSHOT=<path>.tar.gz`.

## Configuration

- `UNIXML_INGEST_SNAPSHOT` - Path to read XML Snapshot from on startup.