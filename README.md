# Crossref Manifold

<img src="doc/graphics/celebration.png" width="200px" alt="Manifold logo" align="right">

- Comprehensive [Developer Docs](https://crossref.gitlab.io/manifold/) — read these first!
- [SONAR Code Quality](https://sonarcloud.io/dashboard?id=org.crossref%3Amanifold)

Crossref Manifold is the API where research objects meet. It keeps track of metadata and connections between research
objects. It accepts data from Crossref members, Crossref Agents, and other members of the scholarly community and brings
them together in one single research graph.

Crossref Manifold combines data from a range of sources and makes it available in a range of formats. It will power a
number of future APIs, such as the REST API, Content Negotiation, Funder Registry, Cited-By, Event Data and more
besides.

The Crossref Manifold is made up of a Kernel layer, which handles Item Trees, Item Graph, Relationship types, internal
Events. The Kernel also provides REST APIs which use this language.

Product features are implemented as Modules. These are responsible for translating data formats and vocabularies in and
out of the Kernel, reporting, etc.

**See the [Developer Docs](https://crossref.gitlab.io/manifold/) for loads more info.**

## Getting started

### Prerequisites

- [IntelliJ IDEA](https://www.jetbrains.com/idea/download/): Our recommended IDE.
- [PostgreSQL](https://www.postgres.app): Our database of choice. Version 13.8 to 15.x.
- [Colima](https://github.com/abiosoft/colima): You need a working Docker engine. We recommend Colima, an open-source
  Docker alternative.
- [AWS CLI](https://aws.amazon.com/cli/): Required for AWS services configuration locally.
- [LocalStack](https://localstack.cloud/): To simulate AWS services locally.

Start Colima `colima start`

Start Localstack `localstack start` or `docker run -d  localstack/localstack`

### Run the setup script

The bash helper script `./setup-localstack.sh` will

1. Create the database defined in `DB_URI_RW` if it doesn't exist
2. Setup the AWS S3 buckets and SQS queues with localstack
3. Create the IntelliJ run configuration and/or Output the environment variable string you'll need to paste into your
   IntelliJ run configuration

### Setup IntelliJ

There is an example run configuration in `.run/Manifold.run.xml.dist` which is used by the setup script to create a run
configuration for you.
Alternatively, you can copy to `.run/Manifold.run.xml.dist` and update with the environment variables applicable to your
setup.

Or, to setup the run configuration for the project and integration tests manually:

1. Copy the env var string output by `./setup-localstack.sh`
2. Open IntelliJ and open Run -> Edit Configurations
3. Create a Kotlin configuration called `Manifold`
4. Paste the environment variable string into the `Environment Variables` box
5. Create a JUnit configuration called `RunCucumberTestsIT`
6. Paste the environment variable string into the `Environment Variables` box

## How to run

This is a Spring Boot application. You can run it in IntelliJ, or at the command line with:

```shell
mvn spring-boot:run
```

Various configuration options are available in the Kernel and the various modules. They include:

## Common configuration options

### AWS

Manifold uses S3 and SQS. It is compatible with LocalStack. Uses the standard AWS JDK options, or supply:

| Key                   | Required | Description                                                          |
|-----------------------|----------|----------------------------------------------------------------------|
| AWS_ACCESS_KEY_ID     | N        | Required only if AWS isn't configured by the AWS metadata service.   |
| AWS_SECRET_ACCESS_KEY | N        | Required only if AWS isn't configured by the AWS metadata service.   |
| AWS_ENDPOINT_OVERRIDE | N        | Useful for localstack config                                         |
| AWS_REGION            | N        | Required only if AWS isn't configured by the AWS metadata service    |
| SQS_ENABLED           | N        | Needed for anything that consumes from an SQS queue.                 |
| S3_ENABLED            | N        | Needed for anything that connects to S3 (e.g. rendering, ingestion). |

### Database

Manifold requires PostgreSQL.

| Key       | Required | Description                                                                                                                  |
|-----------|----------|------------------------------------------------------------------------------------------------------------------------------|
| DB_URI_RW | Y        | read-write database URI. e.g. `jdbc:postgresql://HOST/DATABASE?user=USER&password=PASSWORD`                                  |
| DB_URI_RO | Y        | read-only database URI. e.g. `jdbc:postgresql://HOST/DATABASE?user=USER&password=PASSWORD&readOnly=true&readOnlyMode=always` |

### APIs

| Key                 | Required | Description                                                                                     |
|---------------------|----------|-------------------------------------------------------------------------------------------------|
| API_BASE_URL        | N        | Base URL the API expects to run at, for constructing responses. e.g. `https://api.crossref.org` |
| RELATIONSHIPS_API   | N        | If 'true', enable /beta/relationships API endpoint.                                             |
| RENDERING_ITEMS_API | N        | If 'true', enable the /beta/items API endpoint.                                                 | 
| RENDERING_WORKS_API | N        | If 'true' enable the /beta/works API endpoint.                                                  |                                         

### Item Tree Matching

This is a core part of the pipeline. At least one node must be enabled to do this.

| Key                    | Required | Description                                                                                                    |
|------------------------|----------|----------------------------------------------------------------------------------------------------------------|
| MATCHING_ENABLED       | N        | Queue processor for matching enabled.                                                                          |
| MATCHING_BATCH_SIZE    | N        | How many item trees to render in a batch.                                                                      |
| MATCHING_INITIAL_DELAY | N        | Initial delay before starting the update scheduler.                                                            | 
| MATCHING_FIXED_DELAY   | N        | Sleep between runs in the update scheduler.                                                                    |
| MATCHING_PARTITIONS    | N        | Comma-separated list of queue partitions that will be processed by this instance. Values: basic, ror, preprint |
| MATCHING_PREPRINT_API  | N        | Preprint matching enabled if set. A preprint matching service URL (only compatible with Marple currently)      |

### Item Tree Updating

This is a core part of the pipeline.

| Key                                     | Required | Description                                                                                                            |
|-----------------------------------------|----------|------------------------------------------------------------------------------------------------------------------------|
| ITEM_TREE_UPDATE_ENABLED                | N        | Queue processor for updating item trees enabled.                                                                       |
| ITEM_TREE_UPDATE_BATCH_SIZE             | N        | How many item trees to render in a batch. Around 100 is recommended.                                                   |
| ITEM_TREE_UPDATE_INITIAL_DELAY          | N        | Initial delay before starting the update scheduler.                                                                    | 
| ITEM_TREE_UPDATE_FIXED_DELAY            | N        | Sleep between runs in the update scheduler.                                                                            |
| ITEM_TREE_UPDATE_PRODUCE_ITEM_TREE_JSON | N        | Boolean, default false. sStore the Item Tree JSON in the database. Needed for when Manifold produces metadata content. |  

s

### Metadata Rendering

All of these are required to run the renderer.

| Key                                      | Required | Description                                             |
|------------------------------------------|----------|---------------------------------------------------------|
| RENDERING_RENDER_S3_BUCKET               | N        | S3 bucket name to render metadata to                    |
| RENDERING_RENDER_QUEUE_PROCESSOR_ENABLED | N        | Enable rendering on this instance.                      |
| RENDERING_BATCH_SIZE                     | N        | Number of documents to render from the queue per batch. |
| RENDERING_RENDERER_ENABLED               | N        | Enable the item renderer.                               | 

### Relationships API

| Key                       | Required | Description                                                           |
|---------------------------|----------|-----------------------------------------------------------------------|
| MAX_RETURNED_ROWS         | N        | Maximum number of rows that can be requested.                         |
| DEFAULT_RETURNED_ROWS     | N        | Default number of rows that are requested.                            |
| RA_LOOKUP_ALL_CRON        | N        | Schedule for refreshing registration agencies for _ALL_ DOI prefixes. | 
| RA_LOOKUP_NEW_CRON        | N        | Schedule for refreshing registration agencies for new DOI prefixes.   | 
| DATACITE_TYPE_LOOKUP_CRON | N        | Schedule for looking up the work type of DataCite DOIs (first try)    |
| DATACITE_TYPE_RETRY_CRON  | N        | Schedule for looking up the work type of DataCite DOIs (retries)      |
| SYNC_RELATIONSHIPS_CRON   | N        | Schedule for synchronising relationships API view                     |
| REL_SYNC_BATCH_SIZE       | N        | The number of update activities to include in each relationship sync  |

## Crossref Organizations

Ingest member data from Crossref's SugarCRM instance. All of these are required to run the ingester.

| Key                                         | Required | Description                                                                   |
|---------------------------------------------|----------|-------------------------------------------------------------------------------|
| CROSSREF_ORGANIZATIONS_SUGAR_URL            | N        | SugarCRM API URL. e.g. `https://crossref.sugarondemand.com/rest/v11/`         |
| CROSSREF_ORGANIZATIONS_SUGAR_USERNAME       | N        | Username for SugarCRM.                                                        |
| CROSSREF_ORGANIZATIONS_SUGAR_PASSWORD       | N        | Password for SugarCRM.                                                        |
| CROSSREF_ORGANIZATIONS_SUGAR_IMPORT_CRON    | N        | See https://www.quartz-scheduler.org/ NB this is different to GNU Cron syntax |
| CROSSREF_ORGANIZATIONS_SUGAR_IMPORT_ENABLED | N        | Enable sugar importer                                                         |

### Event Data

| Key                             | Required | Description                                                                                  |
|---------------------------------|----------|----------------------------------------------------------------------------------------------|
| EVENT_DATA_INGEST_JSON_SNAPSHOT | N        | Filename to ingest an Event Data JSON bulk snapshot.                                         |
| EVENT_DATA_CONVERT_RECORDS_SRC  | N        | Input filename for archive of evidence records for conversion to envelope batch archive.     |
| EVENT_DATA_CONVERT_RECORDS_DST  | N        | Output filename for conversion to envelope batch archive.                                    |  
| EVENT_DATA_CONVERT_DATACITE_SRC | N        | Input filename for archive of Datacite events for conversion to envelope batch archive.      |
| EVENT_DATA_CONVERT_DATACITE_DST | N        | Output filename for archive of Datacite events for conversion to envelope batch archive.     |
| EVENT_DATA_BUS_CONTROLLER       | N        | Boolean flag for enabling the Eventdata event bus.                                           |
| EVENT_DATA_SOURCE_ALLOW_LIST    | N        | Comma-separated list of source names to allow on event bus or snapshot ingestion.            |
| EVENT_DATA_JWT_SECRETS          | N        | JWT secret for verifying Events sent to Event Bus serivce.                                   |
| EVENT_DATA_S3_EVIDENCE_QUEUE    | N        | SQS queue name for S3 bucket updates. Listens on this for Event Data Evidence Records.       |
| EVENT_DATA_S3_EVENTS_QUEUE      | N        | SQS queue name for S3 bucket updates. Listens on this for Event Data Events.                 |
| EVENT_DATA_AGENTS_ALLOW_LIST    | N        | Comma-separated list of agent names to allow on S3 events and/or evidence records ingestion. |

### ROR

| Key                          | Required | Description                                                                |
|------------------------------|----------|----------------------------------------------------------------------------|
| UNIXML_S3_NOTIFICATION_QUEUE | N        | SQS notification queue for an S3 bucket. Will ingest a Crossref XML files. |

### Crossref UniXML continual ingestion

| Key                        | Required | Description                                                   |
|----------------------------|----------|---------------------------------------------------------------|
| UNIXML_INGEST_XML_SNAPSHOT | N        | Local filename of XML TGZ snapshot directly. Runs at startup. |

### Crossref UniXML one-time snapshots

| Key                        | Required | Description                                                                |
|----------------------------|----------|----------------------------------------------------------------------------|
| UNIXML_INGEST_XML_SNAPSHOT | N        | Local filename of XML TGZ snapshot directly. Runs at startup.              |
| UNIXML_CONVERT_SNAPSHOT    | N        | Local filename XML TGZ snapshot to convert. Runs at startup.               |
| UNIXML_CONVERT_SKIP        | N        | Skip processing the first N files from a TGZ snapshot.                     |
| UNIXML_CONVERT_MAX         | N        | Process up to N files from a TGZ snapshot (including skipped files).       |
| UNIXML_CONVERT_ID          | N        | Optional id for the exported archive (resulting filename: "${id}.tar.gz"). |

## Tuning and one-off tasks.

### Envelope Batch Bulk ingester

| Key                      | Required | Description                                                                            |
|--------------------------|----------|----------------------------------------------------------------------------------------|
| BULK_INGESTER_BATCH_SIZE | N        | Offline ingestion batch size. Accumulates this many in RAM before writing to database. |
| BULK_INGESTER_PATH       | N        | If supplied, will ingest envelope batch archive from this path.                        |

### SQS queue

Used for various operations, including ingesting from S3 buckets.

| Key                   | Required | Description                           |
|-----------------------|----------|---------------------------------------|
| SQS_BATCH_SIZE        | N        | Number of messages to poll. Max 10.   |
| SQS_WAIT_TIME_SECONDS | N        | Pause between polls.                  | 
| SQS_PARALLEL_POLLERS  | N        | Number of pollers to run in parallel. | 

## Metrics

The application collects and exposes open metrics at http://localhost:8080/actuator/prometheus/.

A docker-compose setup is included for running Prometheus and Grafana for easy access to the metrics in development. To
run it use:

```shell
docker-compose up -d
```

- Prometheus: http://localhost:4490
- Grafana: http://localhost:4491
    - default user: admin
    - default pass: admin

## Tests

The project includes unit and integration tests under `src/test/kotlin` and `src/test-integration/kotlin` respectively.
Right-clicking on any of these directories provides the option to run the included tests. Integration tests need an
initial setup though.

### Unit tests

To execute the unit tests we can run the maven test goal from within our IDE or as `mvn test` in the CLI.

Alternatively we can right-click on `src/test/kotlin` and select Run 'Tests in kotlin'.

These 2 environment variables must be set:

```
ITEM_TREE_UPDATE_ENABLED=true
MATCHING_ENABLED=true
```

There are not further dependencies for running the unit tests.

### Integration tests

Integration tests require a set of services to be present in the development environment.
The [Integration](https://gitlab.com/crossref/integration) project can take care of that.

Using the Integration project one can set up a fully working environment for integration testing:

```shell
git clone https://gitlab.com/crossref/integration.git
cd integration
./integration up -var-file manifold-ci.tfvars
```

To run the tests we need to provide an appropriate DB_URI_RW env var value.
For now the integration tests are not configured to run as maven goals in the pom file, so they will not run with maven
verify through the IDE.

We can still right-click on `src/test-integration/kotlin` and select Run 'Tests in kotlin' though.
In that case make sure you have configured your DB_URI_RW env var.

To run the integration tests in the CLI we can use:

```shell
DB_URI_RW="jdbc:postgresql://localhost:5432/nexus?user=nexus&password=nexus" mvn test-compile failsafe:integration-test failsafe:verify
```

## Updating local environment

Sometimes there are changes to Manifold or the database schema that break your local Manifold installation.
Some common errors and how to fix them:

1) Symptoms: tests fail with `flyway` errors about schema migration version mismatch.

   Fix: run this command in the project dir (replace `<project dir>` with your local path):

   `mvn "-Dflyway.url=jdbc:postgresql://localhost:5432/manifold?user=manifold&password=manifold" "-Dflyway.locations=filesystem:<project dir>/src/main/resources/db/migrations" flyway:repair`
2) Symptoms: tests fail with SQL errors about existing relations or tables.

   Fix (**Warning** - will delete all data from your local DB):
    - run in the project dir: `psql`
    - execute in the psql prompt: `DROP DATABASE manifold;`
    - run `./setup-localstack.sh` to re-create the database