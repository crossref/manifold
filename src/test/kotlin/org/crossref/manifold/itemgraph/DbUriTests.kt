package org.crossref.manifold.itemgraph

import org.crossref.manifold.identifiers.IdentifierParser
import org.crossref.manifold.itemtree.Identifier
import org.crossref.manifold.itemtree.logger
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test

class DbUriTests {
    /**
     * The most important characteristic is that an Identifier can round-trip through a DbUri and back to an identical
     * identifier.
     *
     * These inputs are pre-encoded URIs, which means they should come out the same.
     */
    @Test
    fun roundTrip() {
        val uris = listOf(
            "https://doi.org/10.5555/123456789",
            "https://doi.org/././.",
            "https://doi.org///",
            "https://doi.org//",
            "https://doi.org/",
            "https://doi.org",
            "http://example.com:8080/this%21/that%21?what%21=is%21[20:23]#this%21",
            "http://example.com:8080/this/that?what=is#this?%21",
            "http://example.com:8080/this/that??what=is#this%21",
            "http://example.com/this/that??what=is[20:23]#this!",
            "http://example.com/this/that[2023]",
            "urn:ietf:rfc:2648",
            "urn:eic:10X1001A1001A450",
            "frog",
            "fro%5Bg%5D?what=is-a-f%5Br%5Dog",

            "https://example.com/this%3Fthat?this",

            // From the identifier tests.
            "ark:12345/x6np1wh8k/c3/s5.v7.xsl",
            "https://arxiv.org/abs/2201.00025",
            "https://id.crossref.org/accession/accession-4",
            "https://id.crossref.org/prefix/10.5555",
            "https://doi.org/10.1002/(sici)1099-050x(199823/24)37:3/4%3C197::aid-hrm2%3E3.0.co;2-%23",
            "https://jag.journalagent.com/sislietfaltip/pdfs/SETB_57_2_182_188[A].pdf",
            "[2022]43-16, ",
            "https://doi.org/10.5555/123456789\\",

            "https://doi.org/10.1002/(sici)1099-050x(199823/24)37:3/4%3C197::aid-hrm2%3E3.0.co;2-%23\\",
            "https://doi.org/10.1002/(sici)1099-050x(199823/24)37:3/4%3C197::aid-hrm2%3E3.0.co;2-%23^",
            "https://doi.org/10.1002/(sici)1099-050x(199823/24)37:3/4%3C197::aid-hrm2%3E3.0.co;2-%23`",
            "https://doi.org/10.1002/(sici)1099-050x(199823/24)37:3/4%3C197::aid-hrm2%3E3.0.co;2-%23{",
            "https://doi.org/10.1002/(sici)1099-050x(199823/24)37:3/4%3C197::aid-hrm2%3E3.0.co;2-%23%7C",
            "https://doi.org/10.1002/(sici)1099-050x(199823/24)37:3/4%3C197::aid-hrm2%3E3.0.co;2-%23}",
            "https://doi.org/10.1675/1524-4695(2003)026[0119:iosoga]2.0.co;2"
        )

        uris.forEach {
            val input = IdentifierParser.parse(it)
            val dbUri = ItemDao.DbUri.fromIdentifier(input)
            val result = dbUri.toIdentifier()

            Assertions.assertEquals(input, result)
        }
    }

    @Test
    fun fuzzyTestAsciiCharactersRoundTrip() {

        fun parse(url: String): Identifier? {
            return try {
                val doi = IdentifierParser.parse(url)
                val dbUri = ItemDao.DbUri.fromIdentifier(doi)

                return dbUri.toIdentifier()
            } catch (e: Exception) {
                null
            }
        }


        val baseUrls = listOf(
            "http://xyz.com/test/this-",
            "https://xyz.com/test/this-",
            "ftp://xyz.com/test/this-",
            "xyz.com/test/this-",
            "frog",
            "MOST [2019] ",
            "https://jag.journalagent.com/sislietfaltip/pdfs/SETB_57_2_182_188[A].pdf"
        )

        // Printable and extended ASCII characters subset
        val asciiChars = (32..255).map { it.toChar() }

        // Test all characters one by one
        for (char in asciiChars) {
            for (url in baseUrls) {
                val input = url + char
                val parsed = parse(input)
                Assertions.assertTrue(parsed != null)
            }
        }

        // Check against random strings
        for (url in baseUrls) {
            val input = url + (1..100).map { asciiChars.random() }.joinToString("")
            val parsed = parse(input)
            logger.info(input)
            Assertions.assertTrue(parsed != null)
        }
    }

    /**
     * Pairs of different but similar URIs should be represented as not equal.
     */
    @Test
    fun different() {
        val pairs = listOf(
            // Scheme
            "http://example.com" to "https://example.com",

            // Slash
            "https://example.com" to "https://example.com/",

            // Port
            "https://example.com" to "https://example.com:80/",

            // Empty fragment
            "https://example.com/" to "https://example.com/#",

            // Not empty fragment
            "https://example.com/#this" to "https://example.com/#that"
        )

        pairs.forEach {
            val input = IdentifierParser.parse(it.first)
            val dbUri = ItemDao.DbUri.fromIdentifier(input)
            val result = dbUri.toIdentifier()

            val unexpected = IdentifierParser.parse(it.second)
            Assertions.assertNotEquals(unexpected, result)
        }
    }

    @Test
    fun acceptableChange() {

        val pairs = listOf(
            // The user parts are removed by the IdentifierParser, but it's good to demonstrate that user parts
            // are removed end-to-end.
            "http://joe:password@www.example.com" to "http://www.example.com"
        )

        pairs.forEach {
            val input = IdentifierParser.parse(it.first)
            val dbUri = ItemDao.DbUri.fromIdentifier(input)
            val back = dbUri.toIdentifier()

            val expected = IdentifierParser.parse(it.second)
            Assertions.assertEquals(expected, back)
        }
    }
}