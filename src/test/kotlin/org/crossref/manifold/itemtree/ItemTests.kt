package org.crossref.manifold.itemtree

import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

internal class ItemTest {
    @Test
    fun withPk() {
        val item = Item()

        assertEquals(item.pk, null, "Should be created with no Pk")

        val withPk = item.withPk(500)
        assertEquals(500, withPk.pk, "withPk should attach PK")
        assertEquals(null, item.pk, "withPk should not affect original")
    }

    @Test
    fun blankNode() {
        val item = Item()
        assertTrue(item.identifiers.isEmpty())
        assertEquals(true, item.hasNoIdentifiers(), "Item with no Identifiers is considered a blank node.")
    }

    @Test
    fun `No exceptions thrown when trying to fetch from empty properties`() {

        val item = Item()

        assert(item.properties.isEmpty())

        assertNull(item.getPropertyText("a"))
        assertNull(item.getPropertyObject("a"))
        assertNull(item.getPropertyArrayFirstValue("a"))
        assertNull(item.getProperty("a"))
        assertNull(item.getPropertyArray("a"))
    }
}
