package org.crossref.manifold.itemtree

import com.fasterxml.jackson.databind.json.JsonMapper
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule
import org.crossref.manifold.itemtree.compact.CompactItem
import org.crossref.manifold.itemtree.compact.CompactItemTree
import org.crossref.manifold.itemtree.compact.CompactProperties
import org.crossref.manifold.itemtree.compact.CompactRelationship
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test
import java.time.OffsetDateTime
import java.time.ZoneOffset

class CompactItemTreeTests {
    companion object
    {
        // Just need some placeholder values.
        const val DISCUSSES = 9000
        const val CITATION = 90001
        const val FUNDER = 90002
    }

    private val nodes = CompactItem(
        listOf(
            CompactRelationship(
                CompactItem(
                    listOf(
                      CompactRelationship(
                          CompactItem(
                              listOf(
                                  CompactRelationship(
                                      CompactItem(
                                          listOf(CompactRelationship(
                                              CompactItem(emptyList(), emptyList(), 104),
                                              null,
                                              CITATION
                                          )),
                                          emptyList(),
                                          103
                                      ),
                                      null,
                                      DISCUSSES
                                  )

                              ),
                              emptyList(),
                              102
                          ),
                          null,
                          DISCUSSES
                      )
                    ),
                    emptyList(),
                    101
                ),
                null,
                CITATION
            ),

            CompactRelationship(
                CompactItem(emptyList(), emptyList(), 105),
                null,
                CITATION
            ),

            CompactRelationship(
                CompactItem(listOf(CompactRelationship(CompactItem(emptyList(), emptyList(), 106), null, CITATION)), emptyList(), 107),
                null,
                FUNDER
            ),

            CompactRelationship(
                CompactItem(emptyList(), emptyList(), 108),
                null,
                FUNDER
            )
        ),
        listOf(CompactProperties(jsonObjectFromMap(mapOf("type" to "work", "subtype" to "journal-article")))),
        100
    )
//
//    // This one has null relationships to model a resolved tree, but one that doesn't have the relationship strings or identifier types.
//    // These are rehydrated at a later point, and this is designed to test the round-trip only of the serialized part.
//    private val nodes = Item().withPk(100)
//        .withPropertiesFromMap(mapOf("type" to "work", "subtype" to "journal-article"))
//        .withRelationships(
//            listOf(
//                Relationship(
//                    null,
//                    Item().withPk(101)
//                        .withRelationships(
//                            listOf(
//                                Relationship(
//                                    null,
//                                    Item().withPk(102)
//                                        .withRelationships(listOf(Relationship(null, Item().withPk(103)).withPk(CITATION)))
//                                ).withPk(DISCUSSES)
//                            )
//                        )
//                ).withPk(CITATION),
//
//                Relationship(
//                    null,
//                    Item().withPk(104)
//                ).withPk(CITATION),
//
//                Relationship(
//                    null,
//                    Item().withPk(105)
//                ).withPk(FUNDER),
//
//                Relationship(
//                    // This relationship should be returned as it's unresolved.
//                    null,
//                    Item().withPk(106)
//
//                        .withRelationships(
//                            listOf(
//                                Relationship(
//                                    null, Item().withPk(107)
//                                ).withPk(CITATION)
//                            )
//                        )
//                ).withPk(FUNDER)
//            )
//        )

    @Test
    fun sequenceAndLookup() {
        val sequence = nodes.toSequence()

        assertEquals(9, sequence.count(), "Every node should be returned in sequence.")
        assertEquals(9, sequence.toSet().count(), "Every node should be different")

        val tree = CompactItemTree(nodes)

        assertEquals(nodes, tree.root, "Constructor should wrap root node unchanged")
        val nodeIndexes = CompactItemTree.constructNodeIndexes(tree.root)

        for (i in 0..8) {
            val gotNode = tree.getNode(i, nodeIndexes)
            assertNotNull(gotNode, "Should return an item for every position, but not for $i.")

            val gotPosition = tree.getPosition(gotNode!!, nodeIndexes)
            assertNotNull(gotNode, "Should return a position for every item.")
            assertEquals(gotPosition, i, "Lookup by position should round-trip.")
        }
    }

    /**
     * A tree can be serialized and then deserialized, and is returned identical.
     */
    @Test
    fun compactSerialization() {
        val jsonMapper = JsonMapper.builder().addModule(JavaTimeModule()).build()

        val tree = CompactItemTree(nodes)
        val nodeIndexes = CompactItemTree.constructNodeIndexes(tree.root)

        val result = jsonMapper.writeValueAsString(tree)

        val deserialized = jsonMapper.readValue(result, CompactItemTree::class.java)

        assertEquals(tree, deserialized, "Round-tripped tree should be identical.")

        // Retrieval via the getNode should also return identical.
        for ((item, position) in tree.getNodes(nodeIndexes)) {
            assertEquals(
                tree.getNode(position, nodeIndexes),
                item,
                "items retrieved via getNodes should have same indexes as getNode"
            )
            assertEquals(
                tree.getNode(position, nodeIndexes),
                deserialized?.getNode(position, nodeIndexes),
                "Index $position should return the same from original and round-tripped tree."
            )
        }
    }

    @Test
    fun comparison() {
        val tree = CompactItemTree(nodes)
        assertTrue(tree.root.equalsIgnoringTime(tree.root), "Tree is equal to itself")

        val shallowA = CompactItem(
            pk = 100,
            rels = listOf(
                CompactRelationship(
                    obj = CompactItem(pk = 100),
                    relTypePk = CITATION,
                    assertedAt = OffsetDateTime.of(2001, 1, 1, 1, 1, 1, 1, ZoneOffset.UTC))),
            properties =  listOf(CompactProperties(jsonObjectFromMap(mapOf("type" to "work", "subtype" to "journal-article")), assertedAt = OffsetDateTime.of(2022, 1, 1, 1, 1, 1, 1, ZoneOffset.UTC))),
        )
        val shallowB = CompactItem(
            pk = 100,
            rels = listOf(
                CompactRelationship(
                    obj = CompactItem(pk = 100),
                    relTypePk = CITATION,
                    assertedAt = OffsetDateTime.of(2002, 2, 2, 2, 2, 2, 2, ZoneOffset.UTC))),
            properties =  listOf(CompactProperties(jsonObjectFromMap(mapOf("type" to "work", "subtype" to "journal-article")), assertedAt = OffsetDateTime.of(2022, 1, 1, 1, 1, 1, 1, ZoneOffset.UTC))),
        )
        assertTrue(shallowA.equalsIgnoringTime(shallowB), "Shallow trees identical.")
        assertTrue(shallowB.equalsIgnoringTime(shallowA), "Shallow trees identical.")

        assertFalse(shallowA.equalsIgnoringTime(tree.root), "Different trees should be not be equal")
        assertFalse(tree.root.equalsIgnoringTime(shallowA), "Different trees should be not be equal")

        val deepA = CompactItem(
            pk = 100,
            rels = listOf(
                CompactRelationship(
                    obj = CompactItem(pk = 100, rels =  listOf(
                        CompactRelationship(
                            CompactItem(
                                pk = 103,
                                rels =  listOf(CompactRelationship(
                                    CompactItem(pk = 104),
                                    null,
                                    CITATION
                                )),
                            ),
                            null,
                            DISCUSSES,
                            assertedAt = OffsetDateTime.of(2001, 1, 1, 1, 1, 1, 1, ZoneOffset.UTC)
                        )

                    ),),
                    relTypePk = CITATION,
                    assertedAt = OffsetDateTime.of(2001, 1, 1, 1, 1, 1, 1, ZoneOffset.UTC))),
            properties =  listOf(CompactProperties(jsonObjectFromMap(mapOf("type" to "work", "subtype" to "journal-article")), assertedAt = OffsetDateTime.of(2022, 1, 1, 1, 1, 1, 1, ZoneOffset.UTC))),
        )
        val deepB = CompactItem(
            pk = 100,
            rels = listOf(
                CompactRelationship(
                    obj = CompactItem(pk = 100, rels =  listOf(
                        CompactRelationship(
                            CompactItem(
                                pk = 103,
                                rels =  listOf(CompactRelationship(
                                    CompactItem(pk = 104),
                                    null,
                                    CITATION
                                )),
                            ),
                            null,
                            DISCUSSES,
                            assertedAt = OffsetDateTime.of(2002, 2, 2, 2, 2, 2, 2, ZoneOffset.UTC)
                        )

                    ),),
                    relTypePk = CITATION,
                    assertedAt = OffsetDateTime.of(2002, 2, 2, 2, 2, 2, 2, ZoneOffset.UTC))),
            properties =  listOf(CompactProperties(jsonObjectFromMap(mapOf("type" to "work", "subtype" to "journal-article")), assertedAt = OffsetDateTime.of(2022, 1, 1, 1, 1, 1, 1, ZoneOffset.UTC))),
        )
        assertTrue(deepA.equalsIgnoringTime(deepB), "Deep trees identical.")
        assertTrue(deepB.equalsIgnoringTime(deepA), "Deep trees identical.")
    }

    /**
     * Graft can combine item trees, merging relationships and properties.
     */
    @Test
    fun graft() {
        val earlyDate = CompactItemTree(CompactItem(
            pk = 100,
            rels = listOf(
                CompactRelationship(
                    obj = CompactItem(pk = 200),
                    relTypePk = CITATION,
                    assertedAt = OffsetDateTime.of(2001, 1, 1, 1, 1, 1, 1, ZoneOffset.UTC))),
            properties =  listOf(CompactProperties(jsonObjectFromMap(mapOf("type" to "work", "subtype" to "journal-article", "unique-field-1" to "value-field-1")),
                assertedAt = OffsetDateTime.of(2001, 1, 1, 1, 1, 1, 1, ZoneOffset.UTC))),
        ))

        val lateDate = CompactItemTree(CompactItem(
            pk = 100,
            rels = listOf(
                CompactRelationship(
                    obj = CompactItem(pk = 200),
                    relTypePk = CITATION,
                    assertedAt = OffsetDateTime.of(2003, 2, 2, 2, 2, 2, 2, ZoneOffset.UTC))),
            properties =  listOf(CompactProperties(jsonObjectFromMap(mapOf("type" to "work", "subtype" to "book", "unique-field-2" to "value-field-2")),
                assertedAt = OffsetDateTime.of(2003, 1, 1, 1, 1, 1, 1, ZoneOffset.UTC))),
        ))

        val nullDate = CompactItemTree(CompactItem(
            pk = 100,
            rels = listOf(
                CompactRelationship(
                    obj = CompactItem(pk = 300),
                    relTypePk = CITATION,
                    assertedAt = null)),
            properties =  listOf(CompactProperties(jsonObjectFromMap(mapOf("type" to "work", "subtype" to "dataset", "unique-field-3" to "value-field-3")),
                assertedAt = null)),
        ))

        val result = CompactItemTree.graft(listOf(earlyDate, lateDate, nullDate), emptyList())

        assertEquals(100, result?.root?.pk, "Root node should be unchanged." )
        assertEquals(1, result?.root?.properties?.count(), "All properties should be combined at the root node." )
        assertNull(result?.root?.properties?.first()?.assertedAt, "The assertion date should be removed as it's a composite of many." )
        assertNull(result?.root?.properties?.first()?.assertedBy, "The 'assertedBy' should be removed as it's a composite of many." )

        val expectedProperties = listOf(
            CompactProperties(jsonObjectFromMap(mapOf(
                "type" to "work",
                // Select the later property subtype assertion.
                "subtype" to "book",
                // Select the unique fields from all 3.
                "unique-field-1" to "value-field-1",
                "unique-field-2" to "value-field-2",
                "unique-field-3" to "value-field-3"
            ))))
        assertEquals(expectedProperties, result?.root?.properties, "Properties should be combined as expected.")

        val expectedRels = listOf(
            CompactRelationship(
                obj = CompactItem(pk = 200),
                relTypePk = CITATION,
                assertedAt = OffsetDateTime.of(2001, 1, 1, 1, 1, 1, 1, ZoneOffset.UTC)),
                    CompactRelationship(
                    obj = CompactItem(pk = 200),
            relTypePk = CITATION,
            assertedAt = OffsetDateTime.of(2003, 2, 2, 2, 2, 2, 2, ZoneOffset.UTC)),
            CompactRelationship(
                obj = CompactItem(pk = 300),
                relTypePk = CITATION,
                assertedAt = null)
        )
        assertEquals(expectedRels, result?.root?.rels, "Relationships should be concatenated, unchanged")
    }
}