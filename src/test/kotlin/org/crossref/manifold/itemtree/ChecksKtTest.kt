package org.crossref.manifold.itemtree

import org.crossref.manifold.identifiers.IdentifierParser
import org.junit.jupiter.api.Assertions.assertFalse
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test

internal class ChecksKtTest {

    @Test
    fun `allItems should call at all levels of the tree`() {

        // Tree with two branches in the middle to test that we recurse down and across.
        val bottomItem = Item().withIdentifier(IdentifierParser.parse("bottom"))
        val middleItem1 = Item().withIdentifier(IdentifierParser.parse("middle1"))
        val middleItem2 =
            Item().withIdentifier(IdentifierParser.parse("middle2"))
                .withRelationships(listOf(Relationship("cites", bottomItem, assertedAt = null)))
        val topItem = Item().withIdentifier(IdentifierParser.parse("top")).withRelationships(
            listOf(
                Relationship("mentions", middleItem1, assertedAt = null),
                Relationship("mentions", middleItem2, assertedAt = null)
            )
        )

        // A selection of functions to try which trigger at different levels of the tree.
        assertTrue(allItems(topItem) { _ -> true }, "allItems returns true if all checks return true")

        assertFalse(allItems(topItem) { _ -> false }, "allItems returns false if all checks return false")

        assertFalse(allItems(topItem) { it != middleItem1 }, "allItems returns false if middle level returns false")

        assertFalse(allItems(topItem) { it != bottomItem }, "allItems returns false if bottom level returns false")
    }

    @Test
    fun `allItemsResolved fails if any Items are unresolved`() {
        val all = ItemTree(
            Item().withPk(1).withIdentifier(
                IdentifierParser.parse("http://example.com/abc")
            ).withRelationships(
                listOf(
                    Relationship(
                        "mentions", Item().withPk(2).withIdentifier(
                            IdentifierParser.parse("http://example.com/def")
                        ), assertedAt = null
                    ),
                    Relationship(
                        "mentions", Item().withPk(3).withIdentifier(
                        IdentifierParser.parse("http://example.com/hij")
                    )
                        .withRelationships(
                            listOf(
                                Relationship(
                                    "mentions",
                                    // No id here!
                                    Item().withIdentifier(
                                        IdentifierParser.parse("http://example.com/klm")
                                    ),
                                    assertedAt = null
                                )
                            )
                        ), assertedAt = null
                    )
            )
        )
        )

        assertFalse(allItemsResolved(all), "allIdentifiersResolved returns false if any Item has no PK.")
    }

    @Test
    fun `allItemsResolved succeeds if all Items are resolved`() {
        val all = ItemTree(
            Item().withPk(1).withIdentifier(
                IdentifierParser.parse("http://example.com/abc")
            ).withRelationships(
                listOf(
                    Relationship(
                        "mentions", Item().withPk(2).withIdentifier(
                            IdentifierParser.parse("http://example.com/def")
                        ), assertedAt = null
                    ),
                    Relationship(
                        "mentions", Item().withPk(3).withIdentifier(
                        IdentifierParser.parse("http://example.com/hij")
                    )
                        .withRelationships(
                            listOf(
                                Relationship(
                                    "mentions",
                                    Item().withPk(5)
                                        .withIdentifier(
                                            IdentifierParser.parse("http://example.com/klm")
                                        ),
                                    assertedAt = null
                                )
                            )
                        ), assertedAt = null
                    )
                )
            )
        )

        assertTrue(allItemsResolved(all), "allIdentifiersResolved returns true if all Items have no PK.")
    }

    @Test
    fun `allItemIdentifiersResolved fails if any ItemIdentifiers are unresolved`() {
        val all = ItemTree(
            Item().withPk(1).withIdentifier(
                IdentifierParser.parse("http://example.com/abc")
            ).withRelationships(
                listOf(
                    Relationship(
                        "mentions", Item().withPk(2).withIdentifier(
                            IdentifierParser.parse("http://example.com/def")
                        ), assertedAt = null
                    ),
                    Relationship(
                        "mentions", Item().withPk(3).withIdentifier(
                        IdentifierParser.parse("http://example.com/hij")
                    )
                        .withRelationships(
                            listOf(
                                Relationship(
                                    "mentions",
                                    // No PK on the identifier! Though there is one on the item, that's irrelevant.
                                    Item().withPk(4).withIdentifier(
                                        IdentifierParser.parse("http://example.com/klm")
                                    ),
                                    assertedAt = null
                                )
                            )
                        ), assertedAt = null
                    )
                )
            )
        )

        assertFalse(
            allItemIdentifiersResolved(all),
            "allIdentifiersResolved returns false if any Item has no PK."
        )
    }

    @Test
    fun `allItemIdentifiersResolved succeeds if all ItemIdentifiers are resolved`() {
        val all = ItemTree(
            Item().withPk(1).withIdentifier(
                IdentifierParser.parse("http://example.com/abc").withPk(1001)
            ).withRelationships(
                listOf(
                    Relationship(
                        "mentions", Item().withPk(2).withIdentifier(
                            IdentifierParser.parse("http://example.com/def").withPk(1002)
                        ), assertedAt = null
                    ),
                    Relationship(
                        "mentions",
                        Item().withPk(3).withIdentifier(
                            IdentifierParser.parse("http://example.com/hij").withPk(1003)
                        )
                            .withRelationships(
                                listOf(
                                    Relationship(
                                        "mentions",
                                        Item().withPk(5)
                                            .withIdentifier(
                                                IdentifierParser.parse("http://example.com/klm").withPk(1005)
                                            ),
                                        assertedAt = null
                                    )
                                )
                            ),
                        assertedAt = null
                    )
                )
            )
        )

        assertTrue(
            allItemIdentifiersResolved(all),
            "allIdentifiersResolved returns true if all Items have no PK."
        )
    }
}