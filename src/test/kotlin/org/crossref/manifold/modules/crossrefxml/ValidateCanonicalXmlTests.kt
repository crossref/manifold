package org.crossref.manifold.modules.crossrefxml

import org.crossref.manifold.modules.crossrefxml.XmlUtil.getXmlFile
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.ValueSource


/**
 * Validate canonical XML files that were manually created, not rendered by code. Eventually we want our rendering code
 * to produce XMLs identical to these, so we need to make sure they're valid.
 */
class ValidateCanonicalXmlTests {
    private val validator = XmlSchemaValidator()

    @ParameterizedTest(name = "Validating {0}")
    @ValueSource(
        strings = [
            "journal_article_1.xml",
            "journal_article_2.xml",
            "journal_article_3.xml",
            "journal_article_4.xml",
        ]
    )
    fun testJournalUniXsd(fileName: String) {
        testValidUniXsdXml("journals/$fileName")
    }

    @ParameterizedTest(name = "Validating {0}")
    @ValueSource(
        strings = [
            "journal_article_1.deposit.xml",
            "journal_article_2.deposit.xml",
            "journal_article_3.deposit.xml",
            "journal_article_4.deposit.xml",
        ]
    )
    fun testJournalDeposit(fileName: String) {
        testValidDepositXml("journals/$fileName")
    }

    @ParameterizedTest(name = "Validating {0}")
    @ValueSource(
        strings = [
            "book_chapter_1.xml",
            "book_chapter_2.xml",
            "book_chapter_3.xml",
            "book_chapter_4.xml",
        ]
    )
    fun testBookUniXsd(fileName: String) {
        testValidUniXsdXml("books/$fileName")
    }

    @ParameterizedTest(name = "Validating {0}")
    @ValueSource(
        strings = [
            "book_chapter_1.deposit.xml",
            "book_chapter_2.deposit.xml",
            "book_chapter_3.deposit.xml",
            "book_chapter_4.deposit.xml",
        ]
    )
    fun testBookDeposit(fileName: String) {
        testValidDepositXml("books/$fileName")
    }

    private fun testValidDepositXml(fileName: String) {
        val input = getXmlFile(fileName)?.readText()
        if (input != null) checkValidResult(validator.validateDepositXml(input))
    }

    private fun testValidUniXsdXml(fileName: String) {
        val input = getXmlFile(fileName)?.readText()
        if (input != null) checkValidResult(validator.validateUniXsdXml(input))
    }

    private fun checkValidResult(result: XmlValidationResult) {
        val errors = result.prettyPrintErrors().asSequence().toList().joinToString(prefix = "\n", separator = "\n")
        Assertions.assertTrue(
            result.success,
            "XML is invalid. Errors: $errors"
        )
        Assertions.assertTrue(
            result.exceptions.isEmpty(),
            "Valid XML should contain no validation errors. Actual errors: $errors"
        )
    }
}