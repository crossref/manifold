package org.crossref.manifold.modules.crossrefxml

import org.crossref.manifold.modules.crossrefxml.XmlUtil.getXmlFile
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.xml.sax.SAXParseException

/**
 * Validate rendered XML files with corresponding XSD schemas.
 * Since XMLs we currently render are invalid (due to some info missing from ItemTrees), these tests serve as
 * a supplement to `DepositXmlRenderTests` and `UniXsdRoundtripTests`.
 * We validate all `invalid` XML files that represent current rendering results, and check that we get specific errors.
 * We expect that these tests will gradually shrink as we improve the rendering code, and eventually will be removed in
 * favor of checking that rendered XMLs are 100% valid according to schema.
 */
class ValidateRenderedXmlTests {
    private val validator = XmlSchemaValidator()

    @Test
    fun testUniXsdJournalArticle1() {
        val result = getUniXsdValidationResult("journals/journal_article_1.unixsd.invalid.xml")
        Assertions.assertEquals(1, result.exceptions.size)
        assertXmlError(result.exceptions[0], 10, 29, UniXsdError.DOI_RECORD_TYPE)
    }

    @Test
    fun testUniXsdJournalArticle2() {
        val result = getUniXsdValidationResult("journals/journal_article_2.unixsd.invalid.xml")
        Assertions.assertEquals(1, result.exceptions.size)

        assertXmlError(result.exceptions[0], 9, 29, UniXsdError.DOI_RECORD_TYPE)
    }

    @Test
    fun testUniXsdJournalArticle3() {
        val result = getUniXsdValidationResult("journals/journal_article_3.unixsd.invalid.xml")
        Assertions.assertEquals(1, result.exceptions.size)

        assertXmlError(result.exceptions[0], 9, 29, UniXsdError.DOI_RECORD_TYPE)
    }

    @Test
    fun testUniXsdJournalArticle4() {
        val result = getUniXsdValidationResult("journals/journal_article_4.unixsd.invalid.xml")
        Assertions.assertEquals(2, result.exceptions.size)

        assertXmlError(result.exceptions[0], 9, 29, UniXsdError.DOI_RECORD_TYPE)
        assertXmlError(result.exceptions[1], 47, 48, UniXsdError.EMPTY_CONTRIBUTORS)
    }

    @Test
    fun testUniXsdBookChapter1() {
        val result = getUniXsdValidationResult("books/book_chapter_1.unixsd.invalid.xml")
        Assertions.assertEquals(1, result.exceptions.size)

        assertXmlError(result.exceptions[0], 9, 29, UniXsdError.DOI_RECORD_TYPE)
    }

    @Test
    fun testUniXsdBookChapter2() {
        val result = getUniXsdValidationResult("books/book_chapter_2.unixsd.invalid.xml")
        Assertions.assertEquals(1, result.exceptions.size)

        assertXmlError(result.exceptions[0], 9, 29, UniXsdError.DOI_RECORD_TYPE)
    }

    @Test
    fun testUniXsdBookChapter3() {
        val result = getUniXsdValidationResult("books/book_chapter_3.unixsd.invalid.xml")
        Assertions.assertEquals(1, result.exceptions.size)

        assertXmlError(result.exceptions[0], 9, 29, UniXsdError.DOI_RECORD_TYPE)
    }

    @Test
    fun testUniXsdBookChapter4() {
        val result = getUniXsdValidationResult("books/book_chapter_4.unixsd.invalid.xml")
        Assertions.assertEquals(3, result.exceptions.size)

        assertXmlError(result.exceptions[0], 9, 29, UniXsdError.DOI_RECORD_TYPE)

        assertXmlError(result.exceptions[1], 85, 69, UniXsdError.STANDARDS_BODY_CHILDREN)
        assertXmlError(result.exceptions[2], 85, 69, UniXsdError.STANDARDS_BODY_VALUE)
    }

    @Test
    fun testDepositJournalArticle2() {
        val result = getDepositValidationResult("journals/journal_article_2.deposit.invalid.xml", expectValid = true)
        Assertions.assertEquals(0, result.exceptions.size)
    }

    @Test
    fun testDepositJournalArticle3() {
        val result = getDepositValidationResult("journals/journal_article_3.deposit.invalid.xml", expectValid = true)
        Assertions.assertEquals(0, result.exceptions.size)
    }

    @Test
    fun testDepositJournalArticle4() {
        val result = getDepositValidationResult("journals/journal_article_4.deposit.invalid.xml")
        Assertions.assertEquals(1, result.exceptions.size)

        assertXmlError(result.exceptions[0], 49, 32, DepositXmlError.EMPTY_CONTRIBUTORS)
    }

    @Test
    fun testDepositBookChapter2() {
        val result = getDepositValidationResult("books/book_chapter_2.deposit.invalid.xml", expectValid = true)
        Assertions.assertEquals(0, result.exceptions.size)
    }

    @Test
    fun testDepositBookChapter3() {
        val result = getDepositValidationResult("books/book_chapter_3.deposit.invalid.xml", expectValid = true)
        Assertions.assertEquals(0, result.exceptions.size)
    }

    @Test
    fun testDepositBookChapter4() {
        val result = getDepositValidationResult("books/book_chapter_4.deposit.invalid.xml")
        Assertions.assertEquals(2, result.exceptions.size)

        assertXmlError(result.exceptions[0], 87, 69, DepositXmlError.STANDARDS_BODY_CHILDREN)
        assertXmlError(result.exceptions[1], 87, 69, DepositXmlError.STANDARDS_BODY_VALUE)
    }

    private fun getUniXsdValidationResult(fileName: String, expectValid: Boolean = false): XmlValidationResult {
        val input = getXmlFile(fileName)!!.readText()
        val result = validator.validateUniXsdXml(input)
        checkValidationResult(result, expectValid)
        return result
    }

    private fun getDepositValidationResult(fileName: String, expectValid: Boolean = false): XmlValidationResult {
        val input = getXmlFile(fileName)!!.readText()
        val result = validator.validateDepositXml(input)
        checkValidationResult(result, expectValid)
        return result
    }

    private fun checkValidationResult(result: XmlValidationResult, expectValid: Boolean = false) {
        if (!expectValid) {
            Assertions.assertFalse(result.success, "XML must be invalid")
            for (e in result.prettyPrintErrors()) {
                println(e)
            }
        } else {
            Assertions.assertTrue(result.success, "XML must be valid")
        }
    }

    private fun assertXmlError(exception: SAXParseException, line: Int, column: Int, message: String) {
        Assertions.assertEquals(line, exception.lineNumber)
        Assertions.assertEquals(column, exception.columnNumber)
        Assertions.assertEquals(message, exception.message)
    }
}