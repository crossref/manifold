package org.crossref.manifold.modules.crossrefxml

object DepositXmlError {

    const val EMPTY_CONTRIBUTORS = "cvc-complex-type.2.4.b: The content of element 'contributors' is not complete. " +
            "One of '{\"http://www.crossref.org/schema/5.3.1\":organization, " +
            "\"http://www.crossref.org/schema/5.3.1\":person_name, " +
            "\"http://www.crossref.org/schema/5.3.1\":anonymous}' is expected."

    const val STANDARDS_BODY_CHILDREN =
        "cvc-complex-type.2.3: Element 'standards_body' cannot have character [children], " +
                "because the type's content type is element-only."

    const val STANDARDS_BODY_VALUE =
        "cvc-complex-type.2.4.b: The content of element 'standards_body' is not complete. " +
                "One of '{\"http://www.crossref.org/schema/5.3.1\":standards_body_name}' is expected."
}