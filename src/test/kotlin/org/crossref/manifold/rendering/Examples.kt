package org.crossref.manifold.rendering

import com.fasterxml.jackson.databind.JsonNode
import org.crossref.manifold.identifiers.IdentifierParser
import org.crossref.manifold.itemtree.Item
import org.crossref.manifold.itemtree.ItemTree
import org.crossref.manifold.itemtree.Relationship
import org.crossref.manifold.util.BasicJsonMapper

object Examples {

    val preprint = ItemTree(
        Item()
            .withIdentifier(IdentifierParser.parse("10.5555/12345678"))
            .withRelationships(
            listOf(
                Relationship(
                    "author", Item().withPropertiesFromMap(
                        mapOf(
                            "first-name" to "Author-A-first-name",
                            "last-name" to "Author-A-last-name"
                        )
                    )
                ),
                Relationship(
                    "author", Item()
                        .withRelationship(Relationship(
                            "object",
                            Item().withIdentifier(IdentifierParser.parse("https://orcid.org/0000-0002-1825-0097"))))
                        .withPropertiesFromMap(
                        mapOf(
                            "first-name" to "Author-B-first-name",
                            "last-name" to "Author-B-last-name",
                        )
                    )
                ),
                Relationship(
                    "author", Item().withPropertiesFromMap(
                        mapOf(
                            "name" to "Brown University"
                        )
                    )
                )
            )
        ).withPropertiesFromMap(
            mapOf(
                "content-type" to "preprint",
                "title-long" to BasicJsonMapper.mapper.readValue(
                    """[{"value": "Title A"}, {"value": "Title B"}]""",
                    JsonNode::class.java
                ),
                "published-online" to BasicJsonMapper.mapper.readValue(
                    """[{"source":"date-parts","date-time":"2024-01-05T00:00:00.000Z","timestamp":1704412800000,"date-parts":[2024,1,5]}]""",
                    JsonNode::class.java
                ),
                "posted" to BasicJsonMapper.mapper.readValue(
                    """[{"source":"date-parts","date-time":"2023-01-05T00:00:00.000Z","timestamp":1672876800000,"date-parts":[2023,1,5]}]""",
                    JsonNode::class.java
                )
            )
        )

    )
}