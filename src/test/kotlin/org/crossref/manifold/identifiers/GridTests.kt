package org.crossref.manifold.identifiers

import org.junit.jupiter.api.Test

class GridTests {

    @Test
    fun canonical() {
        IdentifierAssertions.assertNormalizesUnchanged(
            "https://grid.ac/institutes/grid.238252.c",
            IdentifierType.GRID
        )
    }

    /**
     * Remove trailing slash.
     */
    @Test
    fun trailingSlash() {
        IdentifierAssertions.assertNormalizesTo(
            "https://grid.ac/institutes/grid.238252.c",
            IdentifierType.GRID,
            "https://grid.ac/institutes/grid.238252.c/",
        )
    }


    /**
     * Cope with the raw and URL form.
     */
    @Test
    fun normalize() {
        IdentifierAssertions.assertNormalizesTo(
            "https://grid.ac/institutes/grid.238252.c",
            IdentifierType.GRID,
            "https://grid.ac/institutes/grid.238252.c",
            "grid.238252.c"
        )
    }
}