package org.crossref.manifold.identifiers

import org.crossref.manifold.identifiers.parsers.CrossrefContainer
import org.crossref.manifold.itemtree.Identifier
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import java.net.URI

class CrossrefContainerTests {

    @Test
    fun canonical() {
        // Container ID from legacy "journal" model.
        IdentifierAssertions.assertNormalizesUnchanged(
            "https://id.crossref.org/container/J1234",
            IdentifierType.CROSSREF_CONTAINER
        )

        // Container ID from legacy "book" model.
        IdentifierAssertions.assertNormalizesUnchanged(
            "https://id.crossref.org/container/B1234",
            IdentifierType.CROSSREF_CONTAINER
        )

        // Container ID from legacy "series" model.
        IdentifierAssertions.assertNormalizesUnchanged(
            "https://id.crossref.org/container/S1234",
            IdentifierType.CROSSREF_CONTAINER
        )
    }

    /**
     * Remove trailing slash.
     */
    @Test
    fun trailingSlash() {
        IdentifierAssertions.assertNormalizesTo(
            "https://id.crossref.org/container/S1234",
            IdentifierType.CROSSREF_CONTAINER,
            "https://id.crossref.org/container/S1234/",
        )
    }


    /**
     * Container IDs have their initial letter upper case.
     */
    @Test
    fun containerInitialUpperCase() {
        IdentifierAssertions.assertNormalizesTo(
            "https://id.crossref.org/container/B5",
            IdentifierType.CROSSREF_CONTAINER,
            "https://id.crossref.org/container/B5",
            "HTTPS://ID.CROSSREF.ORG/CONTAINER/B5",
            "https://id.crossref.org/container/b5",
        )

        IdentifierAssertions.assertNormalizesTo(
            "https://id.crossref.org/container/J6",
            IdentifierType.CROSSREF_CONTAINER,
            "https://id.crossref.org/container/J6",
            "HTTPS://ID.CROSSREF.ORG/CONTAINER/J6",
            "https://id.crossref.org/container/j6",
        )

        IdentifierAssertions.assertNormalizesTo(
            "https://id.crossref.org/container/S7",
            IdentifierType.CROSSREF_CONTAINER,
            "https://id.crossref.org/container/S7",
            "HTTPS://ID.CROSSREF.ORG/CONTAINER/S7",
            "https://id.crossref.org/container/s7",
        )
    }

    @Test
    fun constructors() {
        Assertions.assertEquals(
            Identifier(URI("https://id.crossref.org/container/B1234"), IdentifierType.CROSSREF_CONTAINER),
            CrossrefContainer.fromBookId("1234")
        )

        Assertions.assertEquals(
            Identifier(URI("https://id.crossref.org/container/S1234"), IdentifierType.CROSSREF_CONTAINER),
            CrossrefContainer.fromSeriesId("1234")
        )

        Assertions.assertEquals(
                    Identifier(URI("https://id.crossref.org/container/J1234"), IdentifierType.CROSSREF_CONTAINER),
            CrossrefContainer.fromJournalId("1234")
        )


    }

    /**
     * Similar but not container Ids.
     */
    @Test
    fun edgeCases() {
        // No 'S', 'B', or 'J'.
        IdentifierAssertions.assertNormalizesTo(
            "https://id.crossref.org/container/999",
            IdentifierType.URI,
            "https://id.crossref.org/container/999"
        )

        IdentifierAssertions.assertNormalizesTo(
            "https://id.crossref.org/container/",
            IdentifierType.URI,
            "https://id.crossref.org/container/"
        )

        IdentifierAssertions.assertNormalizesTo(
            "https://id.crossref.org/container",
            IdentifierType.URI,
            "https://id.crossref.org/container"
        )

        IdentifierAssertions.assertNormalizesTo(
            "https://id.crossref.org/",
            IdentifierType.URI,
            "https://id.crossref.org/"
        )

        IdentifierAssertions.assertNormalizesTo(
            "https://id.crossref.org",
            IdentifierType.URI,
            "https://id.crossref.org"
        )
    }
}