package org.crossref.manifold.identifiers

import org.junit.jupiter.api.Test

/**
 * Tests for [Arxiv]
 */
class ArxivTests {

    /**
     * Recognise Arxiv URIs and don't change them.
     */
    @Test
    fun canonical() {
        // Without version.
        IdentifierAssertions.assertNormalizesUnchanged(
            "https://arxiv.org/abs/2201.00025",
            IdentifierType.ARXIV
        )
        // With version.
        IdentifierAssertions.assertNormalizesUnchanged(
            "https://arxiv.org/abs/2201.00025v1",
            IdentifierType.ARXIV
        )
    }

    /**
     * Remove trailing slash.
     */
    @Test
    fun trailingSlash() {
        IdentifierAssertions.assertNormalizesTo(
            "https://arxiv.org/abs/2201.00025v1",
            IdentifierType.ARXIV,
            "https://arxiv.org/abs/2201.00025v1/",
        )
    }

    /**
     * Test for identifiers using the old structure.
     * See https://info.arxiv.org/help/faq/references.html
     */
    @Test
    fun old() {
        // New style
        IdentifierAssertions.assertNormalizesTo(
            "https://arxiv.org/abs/0706.1234v1",
            IdentifierType.ARXIV,
            "arXiv:0706.1234v1",
            "https://arxiv.org/abs/0706.1234v1"
        )

        // Old style
        IdentifierAssertions.assertNormalizesTo(
            "https://arxiv.org/abs/math/9910001",
            IdentifierType.ARXIV,
            "arXiv:math/9910001",
            "https://arxiv.org/abs/math/9910001"
        )

        // With version
        IdentifierAssertions.assertNormalizesTo(
            "https://arxiv.org/abs/math/9910001v1",
            IdentifierType.ARXIV,
            "arXiv:math/9910001v1",
            "https://arxiv.org/abs/math/9910001v1"
        )

        IdentifierAssertions.assertNormalizesTo(
            "https://arxiv.org/abs/hep-th/9901001v3",
            IdentifierType.ARXIV,
            "arXiv:hep-th/9901001v3",
            "https://arxiv.org/abs/hep-th/9901001v3"
        )
    }

    @Test
    fun scheme() {
        IdentifierAssertions.assertNormalizesTo(
            "https://arxiv.org/abs/2201.00025v1",
            IdentifierType.ARXIV,
            "https://arxiv.org/abs/2201.00025v1",
            "http://arxiv.org/abs/2201.00025v1",
            "arXiv:2201.00025v1",
        )
    }
}