package org.crossref.manifold.identifiers

import org.junit.jupiter.api.Test

class HandleTests {

    /**
     * Recognise canonical Handles and don't change them.
     */
    @Test
    fun canonical() {
        IdentifierAssertions.assertNormalizesUnchanged(
            "https://hdl.handle.net/10.SERV/CROSSREF",
            IdentifierType.HANDLE
        )

        IdentifierAssertions.assertNormalizesUnchanged(
            "https://hdl.handle.net/20.1000/100",
            IdentifierType.HANDLE
        )

        // By default, Handles should be case-sensitive.
        // https://datatracker.ietf.org/doc/html/rfc3650#section-3
        IdentifierAssertions.assertNormalizesUnchanged(
            "https://hdl.handle.net/10.AbCdE/FgH",
            IdentifierType.HANDLE
        )
    }

    /**
     * DOIs are a subset of Handles. But if expressed as a handle, represent as a Handle identifier.
     */
    @Test
    fun doi() {
        IdentifierAssertions.assertNormalizesUnchanged(
            "https://hdl.handle.net/10.5555/12345678",
            IdentifierType.HANDLE
        )
    }

    @Test
    fun edgeCases() {
        IdentifierAssertions.assertNormalizesUnchanged(
            "https://hdl.handle.net/",
            IdentifierType.URI
        )

        IdentifierAssertions.assertNormalizesUnchanged(
            "https://hdl.handle.net",
            IdentifierType.URI
        )

        // This isn't one, but there's no way to tell. So it's treated as a Handle.
        IdentifierAssertions.assertNormalizesUnchanged(
            "https://hdl.handle.net/static/images/res_tool.gif",
            IdentifierType.HANDLE
        )
    }
}