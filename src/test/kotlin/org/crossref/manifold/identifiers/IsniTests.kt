package org.crossref.manifold.identifiers

import org.junit.jupiter.api.Test

/**
 * Tests for [Isni].
 */
class IsniTests {

    /**
     * Recognise ISNI and don't change them.
     */
    @Test
    fun canonical() {
        IdentifierAssertions.assertNormalizesUnchanged(
            "https://isni.org/isni/0000000080456315",
            IdentifierType.ISNI
        )

        IdentifierAssertions.assertNormalizesUnchanged(
            "https://isni.org/isni/000000012407618X",
            IdentifierType.ISNI
        )
    }

    /**
     * Remove trailing slash.
     */
    @Test
    fun trailingSlash() {
        IdentifierAssertions.assertNormalizesTo(
            "https://isni.org/isni/0000000080456315",
            IdentifierType.ISNI,
            "https://isni.org/isni/0000000080456315/",
        )
    }

    /**
     * Don't match an ISNI ID if it isn't expressed as a full URI.
     */
    @Test
    fun needsUri() {
        IdentifierAssertions.assertNormalizesTo(
            "000000012407618X",
            IdentifierType.URI,
            "000000012407618X"
        )
    }

    /**
     * If the checksum doesn't match, then it's not an ISNI ID.
     */
    @Test
    fun needsChecksum() {
        IdentifierAssertions.assertNormalizesTo(
            "https://isni.org/isni/0000000080499995",
            IdentifierType.URI,
            "https://isni.org/isni/0000000080499995"
        )
    }


    @Test
    fun edgeCases() {
        IdentifierAssertions.assertNormalizesTo(
            "https://isni.org/isni/",
            IdentifierType.URI,
            "https://isni.org/isni/"
        )

        IdentifierAssertions.assertNormalizesTo(
            "https://isni.org/isni",
            IdentifierType.URI,
            "https://isni.org/isni"
        )

        IdentifierAssertions.assertNormalizesTo(
            "https://isni.org/",
            IdentifierType.URI,
            "https://isni.org/"
        )

        IdentifierAssertions.assertNormalizesTo(
            "https://isni.org",
            IdentifierType.URI,
            "https://isni.org"
        )
    }

}