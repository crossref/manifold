package org.crossref.manifold.testcontainers

import io.mockk.every
import io.mockk.mockkConstructor
import io.mockk.slot
import org.crossref.manifold.identifiers.IdentifierParser
import org.crossref.manifold.ingestion.Envelope
import org.crossref.manifold.ingestion.EnvelopeBatch
import org.crossref.manifold.ingestion.EnvelopeBatchProvenance
import org.crossref.manifold.ingestion.UserAgentParser
import org.crossref.manifold.itemgraph.ItemGraph
import org.crossref.manifold.itemgraph.ItemTreeAssertion
import org.crossref.manifold.itemgraph.ItemTreeUpdater
import org.crossref.manifold.itemtree.Item
import org.crossref.manifold.itemtree.ItemTree
import org.crossref.manifold.lookup.*
import org.crossref.manifold.matching.MatcherService
import org.crossref.manifold.modules.consts.Items
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.jdbc.UncategorizedSQLException
import org.springframework.jdbc.core.JdbcTemplate
import org.springframework.test.context.DynamicPropertyRegistry
import org.springframework.test.context.DynamicPropertySource
import org.springframework.transaction.PlatformTransactionManager
import org.springframework.transaction.support.TransactionTemplate
import org.testcontainers.containers.PostgreSQLContainer
import org.testcontainers.junit.jupiter.Container
import org.testcontainers.junit.jupiter.Testcontainers
import java.time.OffsetDateTime

@SpringBootTest
@AutoConfigureMockMvc
@Testcontainers
class PostgresTests {
    @Autowired
    private lateinit var jdbcTemplate: JdbcTemplate

    @Autowired
    private lateinit var raLookupService: RALookupService

    @Autowired
    private lateinit var raLookupDao: RALookupDao

    @Autowired
    private lateinit var dataCiteTypeLookupService: DataCiteTypeLookupService

    @Autowired
    private lateinit var dataCiteTypeLookupDao: DataCiteTypeLookupDao

    @Autowired
    private lateinit var itemGraph: ItemGraph

    @Autowired
    private lateinit var transactionManager: PlatformTransactionManager

    @Autowired
    private lateinit var itemTreeMatcher: MatcherService

    @Autowired
    private lateinit var itemTreeUpdater: ItemTreeUpdater

    companion object {
        @Container
        val postgreSQLContainer = PostgreSQLContainer<Nothing>("postgres:15").apply {
            withUrlParam("user", username)
            withUrlParam("password", password)
        }

        @DynamicPropertySource
        @JvmStatic
        fun registerDynamicProperties(registry: DynamicPropertyRegistry) {
            registry.add("spring.datasource.rw.url", postgreSQLContainer::getJdbcUrl)
            registry.add("spring.datasource.ro.url") {
                "${postgreSQLContainer.jdbcUrl}&readOnly=true&readOnlyMode=always"
            }
        }
    }

    @BeforeEach
    fun init() {
        jdbcTemplate.execute("TRUNCATE ra_lookup RESTART IDENTITY CASCADE")
        jdbcTemplate.execute("TRUNCATE registration_agency RESTART IDENTITY CASCADE")
        jdbcTemplate.execute("TRUNCATE item_identifier RESTART IDENTITY CASCADE")
        jdbcTemplate.execute("TRUNCATE item RESTART IDENTITY CASCADE")
        // Make sure item PKs and identifier PKs are out of sync to avoid hiding any bugs.
        jdbcTemplate.execute("ALTER SEQUENCE item_identifier_pk_seq RESTART WITH 1000000")
        val prefixMappings =
            mapOf("10.1234" to null, "10.5555" to "Crossref", "10.5678" to "DataCite", "10.2468" to "DataCite")
        mockkConstructor(RAResolver::class)
        val doiPrefixesSlot = slot<Set<String>>()
        every {
            anyConstructed<RAResolver>().lookupRAs(capture(doiPrefixesSlot))
        } answers {
            doiPrefixesSlot.captured.map { it to prefixMappings[it] }
        }
        mockkConstructor(DataCiteTypeResolver::class)
        val doisSlot = slot<Set<String>>()
        every {
            anyConstructed<DataCiteTypeResolver>().lookupTypes(capture(doisSlot))
        } answers {
            doisSlot.captured.map { it to if (it == "10.2468/12345678") null else "Dataset" }
        }
    }

    @Test
    fun `prefix transitions from untried to tried after first lookup`() {
        raLookupDao.addPrefixes(setOf("10.1234"))
        Assertions.assertEquals(
            setOf("10.1234"),
            raLookupDao.getUntriedRALookups(),
            "Expected prefix 10.1234 to be untried."
        )
        Assertions.assertEquals(
            emptySet<String>(),
            raLookupDao.getTriedRALookups(),
            "Expected prefix 10.1234 to be untried."
        )
        raLookupService.lookupUntriedRAs()
        Assertions.assertEquals(
            setOf("10.1234"),
            raLookupDao.getTriedRALookups(),
            "Expected prefix 10.1234 to be tried."
        )
        Assertions.assertEquals(
            emptySet<String>(),
            raLookupDao.getUntriedRALookups(),
            "Expected prefix 10.1234 to be tried."
        )
    }

    @Test
    fun `prefix is removed after maximum unsuccessful tries`() {
        raLookupDao.addPrefixes(setOf("10.1234"))
        raLookupService.lookupUntriedRAs()
        var tryCount = 1
        while (tryCount < RALookupService.MAX_DAILY_TRIES) {
            Assertions.assertEquals(
                setOf("10.1234"),
                raLookupDao.getTriedRALookups(),
                "Expected prefix 10.1234 to be tried $tryCount times."
            )
            raLookupService.lookupTriedRAs()
            tryCount++
        }
        Assertions.assertEquals(
            emptySet<String>(),
            raLookupDao.getTriedRALookups(),
            "Expected prefix 10.1234 to be removed."
        )
        Assertions.assertEquals(
            emptySet<String>(),
            raLookupDao.getUntriedRALookups(),
            "Expected prefix 10.1234 to be removed."
        )
    }

    @Test
    fun `prefix is retained after successful tries`() {
        raLookupDao.addPrefixes(setOf("10.5555"))
        raLookupService.lookupUntriedRAs()
        var tryCount = 1
        while (tryCount < RALookupService.MAX_DAILY_TRIES) {
            Assertions.assertEquals(
                setOf("10.5555"),
                raLookupDao.getTriedRALookups(),
                "Expected prefix 10.5555 to be tried $tryCount times."
            )
            raLookupService.lookupTriedRAs()
            tryCount++
        }
        Assertions.assertEquals(
            setOf("10.5555"),
            raLookupDao.getTriedRALookups(),
            "Expected prefix 10.5555 to be tried."
        )
        Assertions.assertEquals(
            emptySet<String>(),
            raLookupDao.getUntriedRALookups(),
            "Expected prefix 10.5555 to be tried."
        )
    }

    @Test
    fun `DOI transitions from untried to tried after first lookup`() {
        itemGraph.ingest(listOf(envelopeBatch("10.2468/12345678")))
        raLookupService.lookupUntriedRAs()
        itemTreeMatcher.run()
        itemTreeUpdater.run()
        Assertions.assertEquals(
            emptySet<Long>(),
            dataCiteTypeLookupDao.getTriedTypeLookups(0),
            "Expected DOI 10.2468/12345678 to be untried."
        )
        dataCiteTypeLookupService.lookupUntriedTypes()
        Assertions.assertEquals(
            setOf(identifierPk()),
            dataCiteTypeLookupDao.getTriedTypeLookups(0),
            "Expected DOI 10.2468/12345678 to be tried."
        )
    }

    @Test
    fun `DOI is ignored after maximum unsuccessful tries`() {
        itemGraph.ingest(listOf(envelopeBatch("10.2468/12345678")))
        raLookupService.lookupUntriedRAs()
        itemTreeMatcher.run()
        itemTreeUpdater.run()
        dataCiteTypeLookupService.lookupUntriedTypes()
        var tryCount = 1
        while (tryCount < DataCiteTypeLookupService.MAX_DAILY_TRIES) {
            Assertions.assertEquals(
                emptySet<Long>(),
                dataCiteTypeLookupDao.getTriedTypeLookups(DataCiteTypeLookupService.MAX_DAILY_TRIES),
                "Expected DOI 10.2468/12345678 to be tried $tryCount times."
            )
            dataCiteTypeLookupService.lookupTriedTypes()
            tryCount++
        }
        Assertions.assertEquals(
            setOf(identifierPk()),
            dataCiteTypeLookupDao.getTriedTypeLookups(DataCiteTypeLookupService.MAX_DAILY_TRIES),
            "Expected DOI 10.2468/12345678 to be ignored."
        )
    }

    @Test
    fun `DOI is retained after successful try`() {
        itemGraph.ingest(listOf(envelopeBatch("10.5678/12345678")))
        raLookupService.lookupUntriedRAs()
        Assertions.assertEquals(
            emptySet<Long>(),
            dataCiteTypeLookupDao.getTriedTypeLookups(0),
            "Expected DOI 10.5678/12345678 to be untried."
        )
        dataCiteTypeLookupService.lookupUntriedTypes()
        Assertions.assertEquals(
            emptySet<Long>(),
            dataCiteTypeLookupDao.getTriedTypeLookups(0),
            "Expected DOI 10.5678/12345678 to be untried."
        )
    }

    @Test
    fun `read-write transactions are correctly routed`() {
        val transactionTemplate = TransactionTemplate(transactionManager)
        transactionTemplate.execute {
            jdbcTemplate.update("INSERT INTO item(created_at) VALUES (NOW())")
        }
    }

    @Test
    fun `read-only transactions are correctly routed`() {
        val transactionTemplate = TransactionTemplate(transactionManager)
        transactionTemplate.isReadOnly = true
        val exception = transactionTemplate.execute {
            Assertions.assertThrows(UncategorizedSQLException::class.java) {
                jdbcTemplate.update("INSERT INTO item(created_at) VALUES (NOW())")
            }
        }

        Assertions.assertTrue(exception!!.message!!.contains("cannot execute INSERT in a read-only transaction"))
    }

    private fun envelopeBatch(doi: String): EnvelopeBatch =
        EnvelopeBatch(
            listOf(
                Envelope(
                    listOf(
                        ItemTree(
                            Item()
                                .withIdentifier(IdentifierParser.parse(doi))
                        )
                    ),
                    ItemTreeAssertion(
                        OffsetDateTime.now(),
                        Item().withIdentifier(IdentifierParser.parse(Items.CROSSREF_AUTHORITY)),
                    ),
                )
            ),
            EnvelopeBatchProvenance(
                UserAgentParser.parse("DataCite Type Lookup Tests/1.0"),
                OffsetDateTime.now().toString()
            ),
        )

    private fun identifierPk(): Long =
        jdbcTemplate.queryForObject(
            "SELECT pk FROM item_identifier WHERE scheme_specific_part = ?",
            Long::class.java,
            "/10.2468/12345678"
        )
}
