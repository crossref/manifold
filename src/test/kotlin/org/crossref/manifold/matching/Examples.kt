package org.crossref.manifold.matching

import com.fasterxml.jackson.databind.JsonNode
import org.crossref.manifold.identifiers.IdentifierParser
import org.crossref.manifold.itemtree.Item
import org.crossref.manifold.itemtree.ItemTree
import org.crossref.manifold.itemtree.Relationship
import org.crossref.manifold.util.BasicJsonMapper

object Examples {
    /**
     * Sample Relationship PKs for use in tests.
     * The values are arbitrary.
     */
    const val OBJECT_REL = 1
    const val STEWARD_REL = 2
    const val HAS_REVIEW_REL = 3
    const val CONTRIBUTOR_REL = 4
    const val CITATION_REL = 5
    const val FUNDING_REL = 6
    const val FUNDER_REL = 7
    const val SAME_AS_REL = 8

    /**
     * Sample Item PKs
     */
    const val CROSSREF_ITEM_PK = 9999L

    val ORG_1234 =
        Item(listOf(IdentifierParser.parse("https://id.crossref.org/org/1234")), emptyList(), emptyList(), 9876)

    /**
     * A tree that exhibits:
     * Simple relationships:
     *  - 10001 steward 20001
     *  - 10001 has-review 10002
     *
     * Matched reified relationships
     *  - 10001 contributor 30001
     *  - 10001 contributor 30002
     *  - 10001 contributor 30003
     *  - 10001 citation 10003 - matched by Crossref
     *  - 10001 citation 10004
     *  - 10001 citation 10005
     *
     *  Unmatched / unlinked reified relationships
     *  - 10001 contributor ?
     *  - 10001 contributor ?
     *  - 10001 citation ?
     *  - 10001 citation ?
     *
     */
    val tree =
        ItemTree(
            Item().withPk(10001)
                .withRelationships(
                    listOf(
                        Relationship("steward", Item().withPk(20001), assertedAt = null).withPk(STEWARD_REL),
                        Relationship("has-review", Item().withPk(10002), assertedAt = null).withPk(HAS_REVIEW_REL),

                        Relationship(
                            "contributor",
                            Item().withPropertiesFromMap(mapOf("name" to "Contributor A"))
                                .withRelationship(
                                    Relationship(
                                        "object",
                                        Item().withPk(30001),
                                        assertedAt = null
                                    ).withPk(OBJECT_REL)
                                ),
                            assertedAt = null
                        ).withPk(CONTRIBUTOR_REL),

                        Relationship(
                            "contributor",
                            Item().withPropertiesFromMap(mapOf("name" to "Contributor B"))
                                .withRelationship(
                                    Relationship(
                                        "object",
                                        Item().withPk(30002),
                                        assertedAt = null
                                    ).withPk(OBJECT_REL)
                                ),
                            assertedAt = null
                        ).withPk(CONTRIBUTOR_REL),

                        Relationship(
                            "contributor",
                            Item().withPropertiesFromMap(mapOf("name" to "Contributor C"))
                                .withRelationship(
                                    Relationship(
                                        "object",
                                        Item().withPk(30003),
                                        assertedAt = null
                                    ).withPk(OBJECT_REL)
                                ),
                            assertedAt = null
                        ).withPk(CONTRIBUTOR_REL),

                        Relationship(
                            "citation",
                            Item().withPropertiesFromMap(mapOf("name" to "Citation A", "doi-asserted-by" to "crossref"))
                                .withRelationship(
                                    Relationship(
                                        "object",
                                        Item().withPk(10003),
                                        assertedAt = null
                                    ).withPk(OBJECT_REL)
                                ),
                            assertedAt = null
                        ).withPk(CITATION_REL),

                        Relationship(
                            "citation",
                            Item().withPropertiesFromMap(mapOf("name" to "Citation B"))
                                .withRelationship(
                                    Relationship(
                                        "object",
                                        Item().withPk(10004),
                                        assertedAt = null
                                    ).withPk(OBJECT_REL)
                                ),
                            assertedAt = null
                        ).withPk(CITATION_REL),

                        Relationship(
                            "citation",
                            Item().withPropertiesFromMap(mapOf("name" to "Citation C"))
                                .withRelationship(
                                    Relationship(
                                        "object",
                                        Item().withPk(10005),
                                        assertedAt = null
                                    ).withPk(OBJECT_REL)
                                ),
                            assertedAt = null
                        ).withPk(CITATION_REL),

                        Relationship(
                            "contributor",
                            Item().withPropertiesFromMap(mapOf("name" to "Unmatched Contributor D")),
                            assertedAt = null
                        ).withPk(CONTRIBUTOR_REL),

                        Relationship(
                            "contributor",
                            Item().withPropertiesFromMap(mapOf("name" to "Unmatched Contributor E")),
                            assertedAt = null
                        ).withPk(CONTRIBUTOR_REL),

                        Relationship(
                            "citation",
                            Item().withPropertiesFromMap(mapOf("name" to "Unmatched Citation D")),
                            assertedAt = null
                        ).withPk(CITATION_REL),

                        Relationship(
                            "citation",
                            Item().withPropertiesFromMap(mapOf("name" to "Unmatched Citation E")),
                            assertedAt = null
                        ).withPk(CITATION_REL)
                    )
                )
        )

    val funding =
        ItemTree(
            Item().withPk(10001)
                .withRelationships(
                    listOf(

                        // Reified 'funding' object.
                        // These aren't currently matched. But, as the reified object has no identifier and no PK, they
                        // mean a null PK. Include as a regression test to ensure an NPE isn't triggered.
                        Relationship(
                            "funding", relTypePk = FUNDING_REL, obj =
                            Item()
                                .withPropertiesFromMap(mapOf("type" to "award"))
                                .withRelationship(
                                    Relationship(
                                        "funder", relTypePk = FUNDER_REL,
                                        obj = Item().withPk(40000)
                                            .withIdentifier(IdentifierParser.parse("https://doi.org/10.13039/1234"))
                                    )
                                )
                        ),

                        // Direct funder as they appear in articles.
                        Relationship(
                            "funder", relTypePk = FUNDER_REL,
                            obj = Item().withPk(40001)
                                .withIdentifier(IdentifierParser.parse("https://doi.org/10.13039/5678"))
                        ),


                        // Link to funder where there's no ID, so no PK.
                        Relationship(
                            "funder", relTypePk = FUNDER_REL,
                            obj = Item()
                        )
                    )
                )
        )

    val preprintMatchingJournalArticle = ItemTree(
        Item().withRelationships(
            listOf(
                Relationship(
                    "author", Item().withPropertiesFromMap(
                        mapOf(
                            "first-name" to "Author-A-first-name",
                            "last-name" to "Author-A-last-name"
                        )
                    )
                ),
                Relationship(
                    "author", Item()
                        .withRelationship(Relationship(
                            "object",
                            Item().withIdentifier(IdentifierParser.parse("https://orcid.org/0000-0002-1825-0097"))))
                        .withPropertiesFromMap(
                        mapOf(
                            "first-name" to "Author-B-first-name",
                            "last-name" to "Author-B-last-name"
                        )
                    )
                ),
                Relationship(
                    "author", Item().withPropertiesFromMap(
                        mapOf(
                            "name" to "Brown University"
                        )
                    )
                )
            )
        ).withPropertiesFromMap(
            mapOf(
                "title-long" to BasicJsonMapper.mapper.readValue(
                    """[{"value": "Title A"}, {"value": "Title B"}]""",
                    JsonNode::class.java
                ),
                "published-online" to BasicJsonMapper.mapper.readValue(
                    """[{"source":"date-parts","date-time":"2024-01-05T00:00:00.000Z","timestamp":1704412800000,"date-parts":[2024,1,5]}]""",
                    JsonNode::class.java
                ),
                "posted" to BasicJsonMapper.mapper.readValue(
                    """[{"source":"date-parts","date-time":"2023-01-05T00:00:00.000Z","timestamp":1672876800000,"date-parts":[2023,1,5]}]""",
                    JsonNode::class.java
                )
            )
        )

    )

}