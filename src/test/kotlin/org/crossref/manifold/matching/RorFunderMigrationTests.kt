package org.crossref.manifold.matching

import io.mockk.every
import io.mockk.mockk
import org.crossref.manifold.identifiers.IdentifierType
import org.crossref.manifold.itemgraph.RelationshipDao
import org.crossref.manifold.itemgraph.Resolver
import org.crossref.manifold.itemgraph.SimpleRelationship
import org.crossref.manifold.itemtree.Identifier
import org.crossref.manifold.itemtree.Item
import org.crossref.manifold.matching.Examples.CROSSREF_ITEM_PK
import org.crossref.manifold.matching.Examples.FUNDER_REL
import org.crossref.manifold.matching.Examples.SAME_AS_REL
import org.crossref.manifold.matching.Examples.funding
import org.crossref.manifold.matching.strategies.RorFunderMigration
import org.crossref.manifold.registries.RelationshipTypeRegistry
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test
import org.springframework.stereotype.Service
import java.net.URI
import java.time.OffsetDateTime

@Service
class RorFunderMigrationTests
{
    @Test
    fun simple() {

        val itemTreeAssertingParty = 10L
        val assertedAt = OffsetDateTime.now()
        val assertionPk = 99L

        val context = MatcherContext(CROSSREF_ITEM_PK)
        val resolver = mockk<Resolver>()
        val relationshipDao = mockk<RelationshipDao>()
        val relationshipTypeRegistry = mockk<RelationshipTypeRegistry>()

        every {
            relationshipTypeRegistry.resolve("same-as")
        } returns SAME_AS_REL

        every {
            relationshipTypeRegistry.resolve("funder")
        } returns FUNDER_REL

        every {
            resolver.resolveRO(Identifier(uri= URI("https://doi.org/10.13039/1234"), type= IdentifierType.DOI, pk=null))
        } returns Item().withPk(40000).withIdentifier(Identifier(URI("https://doi.org/10.13039/1234"), type= IdentifierType.DOI))

        every {
            resolver.resolveRO(Identifier(uri= URI("https://doi.org/10.13039/5678"), type= IdentifierType.DOI, pk=null))
        } returns Item().withPk(40001).withIdentifier(Identifier(URI("https://doi.org/10.13039/5678"), type= IdentifierType.DOI))


        every {
            relationshipDao.fetchSimpleRelationships(
                objectItemPk = 40000,
                relationshipTypePk = SAME_AS_REL,
                assertedByPk = CROSSREF_ITEM_PK,
                limit = 1000
            )
        } returns listOf(SimpleRelationship(40000, SAME_AS_REL, 50000))

        every {
            relationshipDao.fetchSimpleRelationships(
                objectItemPk = 40001,
                relationshipTypePk = SAME_AS_REL,
                assertedByPk = CROSSREF_ITEM_PK,
                limit = 1000
            )
        } returns listOf(SimpleRelationship(40000, SAME_AS_REL, 50000))

        val matcher = RorFunderMigration(resolver, relationshipDao, relationshipTypeRegistry)

        val result = matcher.match(funding, assertionPk, assertedAt, itemTreeAssertingParty, context)

        assertTrue(
            result.all { it.itemTreeAssertingParty == itemTreeAssertingParty },
            "ItemTreeAssertingParty should match what's passed in."
        )

        assertEquals(
            setOf(
                RelationshipMatch(
                    // Item Tree asserting party, root node, hash should be passed through
                    itemTreeAssertingParty, assertedAt, 10001, assertionPk,

                    // Node position should be 0, as these are at the root (i.e. first) node.
                    0,

                    // Crossref matched this.
                    CROSSREF_ITEM_PK, RorFunderMigration.SCOPE,

                    // Relationship triple should be as found.
                    10001, FUNDER_REL, 40000, true
                )
            ),
            result.toSet(),
            "Expected to find the funding relationship to the ROR identifier."
        )
    }
}
