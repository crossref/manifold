CREATE INDEX rendered_item_item_pk_internal_content_type_current_idx
ON rendered_item (
    item_pk,
    internal_content_type_id,
    current
);