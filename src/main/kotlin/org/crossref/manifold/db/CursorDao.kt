package org.crossref.manifold.db

import org.springframework.jdbc.core.JdbcTemplate
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Repository
import org.springframework.transaction.annotation.Propagation
import org.springframework.transaction.annotation.Transactional
import java.util.concurrent.TimeUnit

data class Fetched<T>(
    val rows: List<T>,
    val fetchSize: Int,
    val nextCursor: String?,
)

@Repository
class CursorDao(
    private val jdbcTemplate: JdbcTemplate,
) {
    @Scheduled(fixedRate = 1, timeUnit = TimeUnit.MINUTES)
    fun closeUnusedCursors() {
        jdbcTemplate.update(
            "DELETE FROM open_cursor WHERE last_used_at < NOW() - INTERVAL '5 minutes'"
        )
    }

    /*
     * It's not necessary for these cursor maintenance functions to be transactional but they are called from within
     * transactional functions, some of which are read-only. Explicitly setting the propagation behaviour to
     * NOT_SUPPORTED ensures that the cursors are updated even when the calling transaction is read-only.
     */

    @Transactional(propagation = Propagation.NOT_SUPPORTED)
    fun addCursor(cursor: String, lastKey: Long) {
        jdbcTemplate.update(
            "INSERT INTO open_cursor (cursor_id, last_used_at, last_key) " +
                    "VALUES (?, NOW(), ?)", cursor, lastKey
        )
    }

    @Transactional(propagation = Propagation.NOT_SUPPORTED)
    fun getLastKey(cursor: String): Long =
        jdbcTemplate.queryForObject(
            "UPDATE open_cursor SET last_used_at = NOW() WHERE cursor_id = ? RETURNING last_key",
            Long::class.java,
            cursor
        )
}
