package org.crossref.manifold

/**
 * Two values were in conflict in a given situation.
 */
class ConflictException(override val message: String?) : Exception(message) {
}