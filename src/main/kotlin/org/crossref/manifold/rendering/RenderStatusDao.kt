package org.crossref.manifold.rendering

import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.jdbc.core.JdbcTemplate
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate
import org.springframework.stereotype.Repository
import org.springframework.transaction.annotation.Transactional
import java.sql.ResultSet
import java.time.InstantSource
import java.time.ZoneOffset

@Repository
class RenderStatusDao(
    jdbcTemplate: JdbcTemplate,
) {
    private val npTemplate: NamedParameterJdbcTemplate = NamedParameterJdbcTemplate(jdbcTemplate)

    var logger: Logger = LoggerFactory.getLogger(this::class.java)

    /**
     * Callback with the next chunk of rows from the item status queue.
     * This will co-operate the locks of other concurrent calls, so can be scaled out.
     * Always call callback, even if there are no results.
     * This enables the callback to notice and stop polling if it wants.
     */
    @Transactional
    fun getFromQueue(count: Int, f: ((itemPkBatch: Collection<Long>) -> Unit)) {
        val params = mapOf("count" to count)

        val itemPks = npTemplate.query(
            """            
            DELETE FROM item_render_status_queue
            WHERE pk IN (
              SELECT pk
              FROM item_render_status_queue
              FOR UPDATE SKIP LOCKED
              LIMIT :count
            )
            RETURNING item_pk;""".trimIndent(),
            params
        ) { rs: ResultSet, _: Int -> rs.getLong("item_pk") }.distinct()
        f(itemPks)
    }

    /**
     * Get current length of queue.
     */
    fun queueLength(): Long =
        npTemplate.query("SELECT count(pk) AS count FROM item_render_status_queue") { rs: ResultSet, _: Int ->
            rs.getLong(
                "count"
            )
        }.first()


    /**
     * Enqueue item PKs to be rendered.
     */
    fun enqueue(itemPks: Collection<Long>) {
        itemPks.chunked(1000).forEach { chunk ->
            // Sort to avoid contention.
            val pks = chunk.sorted()
            npTemplate.batchUpdate(
                """INSERT INTO item_render_status_queue (item_pk, updated_at)
                     SELECT :item_pk, :ingested_at
                     WHERE NOT EXISTS (
                     SELECT item_pk FROM item_render_status_queue WHERE item_pk = :item_pk
                  )""".trimIndent(),
                pks.map {
                    mapOf(
                        "item_pk" to it,
                        "ingested_at" to InstantSource.system().instant().atOffset(ZoneOffset.UTC)
                    )
                }.toTypedArray()
            )
        }
    }
}
