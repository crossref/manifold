package org.crossref.manifold.util

object Constants {
    const val BATCH_SIZE = "batch-size"
    const val INITIAL_DELAY = "initial-delay"
    const val FIXED_DELAY = "fixed-delay"
    const val ENABLED = "enabled"
    const val PRODUCE_ITEM_TREE_JSON = "produce-item-tree-json"
}

/**
* Crossref-specific MIME-types
* */
object MimeType {
    const val CITEPROC_JSON = "application/citeproc+json"

    // This is the elastic document before the points of conversion to Citeproc json
    const val CITEPROC_JSON_ELASTIC = "application/vnd.crossref.citeproc.elastic+json"

    const val CITATION_JSON = "application/vnd.crossref.matching.citation+json"

    const val MEMBER_JSON = "application/vnd.crossref.member+json"

    // This is the elastic document before the points of conversion to member json
    const val MEMBER_JSON_ELASTIC = "application/vnd.crossref.member.elastic+json"

    const val ORGANIZATION_JSON = "application/vnd.crossref.org+json"

    // This is the elastic document before the points of conversion
    // to organization json
    const val ORGANIZATION_JSON_ELASTIC = "application/vnd.crossref.org.elastic+json"

    // <crossref_result> XML, a public output format as an alternative to JSON
    const val UNIXSD_XML = "application/vnd.crossref.unixsd+xml"

    const val ITEM_TREE_JSON = "application/vnd.crossref.itemtree+json"

    // <doi_batch> suitable for deposit
    const val DEPOSIT_XML = "application/vnd.crossref.deposit+xml"

    // posted_content of type preprint rendered as JSON.
    // Suitable for Marple's Opensearch index
    const val PREPRINT_MARPLE = "application/vnd.marple.preprint+json"
}