package org.crossref.manifold.util

import com.fasterxml.jackson.core.JsonParser
import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.databind.JsonNode
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.SerializationFeature
import com.fasterxml.jackson.databind.json.JsonMapper
import com.fasterxml.jackson.databind.module.SimpleModule
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule
import com.fasterxml.jackson.module.kotlin.registerKotlinModule
import org.crossref.manifold.itemtree.ItemTree
import org.crossref.manifold.itemtree.ItemTreeDeserializer
import org.crossref.manifold.itemtree.ItemTreeSerializer
import java.io.FileNotFoundException

/** Load JSON file from resource filename or error.
 */
fun loadJsonResource(fileName: String): JsonNode {
    val mapper = ObjectMapper()
    val inputStream = Thread.currentThread().contextClassLoader
        .getResourceAsStream(fileName) ?: throw FileNotFoundException("File $fileName missing")
    return mapper.readTree(inputStream)
}

object BasicJsonMapper{
    val mapper: ObjectMapper = ObjectMapper()
        .registerModule(JavaTimeModule())
        .registerKotlinModule()
        .disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES)
        .disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS)
}

object JsonItemTreeMapper {
    val mapper = JsonMapper.builder()
        .addModule(
            SimpleModule()
                .addDeserializer(ItemTree::class.java, ItemTreeDeserializer())
                .addSerializer(ItemTree::class.java, ItemTreeSerializer())
        )
        .addModule(JavaTimeModule())
        .build().registerKotlinModule()


    /** Mapper that can read JSON files from the Tar file.
     * AUTO_CLOSE_SOURCE is important for tar streams as they are files of files, therefore have specific file-handling
     * semantics. Normal parsing would close the stream prematurely.
     */
    val tarGzMapper = JsonMapper.builder()
        .addModule(SimpleModule().addDeserializer(ItemTree::class.java, ItemTreeDeserializer()))
        .addModule(SimpleModule().addSerializer(ItemTree::class.java, ItemTreeSerializer()))
        .disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES)
        .addModule(JavaTimeModule())
        .configure(JsonParser.Feature.AUTO_CLOSE_SOURCE, false)
        .build().registerKotlinModule()
}