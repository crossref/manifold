package org.crossref.manifold.lookup

import com.fasterxml.jackson.annotation.JsonProperty
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.web.client.RestTemplate

class RAResolver(
    private val raLookupUrl: String = "https://doi.org/doiRA/"
) {
    private val restTemplate = RestTemplate()
    private val logger: Logger = LoggerFactory.getLogger(this::class.java)

    fun lookupRAs(doiPrefixes: Set<String>): List<Pair<String, String?>> =
        restTemplate.getForEntity(
            raLookupUrl + doiPrefixes.joinToString(","),
            Array<RALookup>::class.java
        ).body!!.map {
            if (it.ra == null) {
                logger.warn("DOI: ${it.doi}, Status: ${it.status}")
            }
            it.doi to it.ra
        }

    private data class RALookup(
        @JsonProperty("DOI") val doi: String,
        @JsonProperty("RA") val ra: String? = null,
        val status: String? = null
    )
}
