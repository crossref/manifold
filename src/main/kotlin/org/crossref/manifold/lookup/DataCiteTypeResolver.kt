package org.crossref.manifold.lookup

import com.fasterxml.jackson.databind.ObjectMapper
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.web.client.RestTemplate

class DataCiteTypeResolver(
    private val dataCiteLookupUrl: String = "https://api.datacite.org/dois"
) {
    private val restTemplate = RestTemplate()
    private val logger: Logger = LoggerFactory.getLogger(this::class.java)

    fun lookupTypes(dois: Set<String>): List<Pair<String, String?>> {
        val types = dois.map { doi ->
            val request = "$dataCiteLookupUrl/$doi"
            try {
                val response = ObjectMapper().readTree(
                    restTemplate.getForObject(
                        request,
                        String::class.java
                    )
                )
                try {
                    response.path("data").let {
                        val returnedDoi = it.path("id").asText()
                        if (doi != returnedDoi) {
                            logger.warn(request)
                            logger.warn(response.asText())
                            logger.warn("DOI was changed from $doi to $returnedDoi")
                            doi to null
                        } else {
                            doi to it.path("attributes").path("types")["resourceTypeGeneral"]?.asText()
                        }
                    }
                } catch (e: Exception) {
                    logger.warn(request)
                    logger.warn(response.asText())
                    logger.warn(e.message)
                    doi to null
                }
            } catch (e: Exception) {
                logger.warn(request)
                logger.warn(e.message)
                doi to null
            }
        }
        types.filter { it.second == null }.forEach { logger.warn("Missing type. DOI: ${it.first}") }
        return types
    }
}
