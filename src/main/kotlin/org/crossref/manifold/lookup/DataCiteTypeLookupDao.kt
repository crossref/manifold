package org.crossref.manifold.lookup

import org.springframework.jdbc.core.JdbcTemplate
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate
import org.springframework.stereotype.Repository

@Repository
class DataCiteTypeLookupDao(
    private val jdbcTemplate: JdbcTemplate,
) {
    private val npTemplate: NamedParameterJdbcTemplate = NamedParameterJdbcTemplate(jdbcTemplate)

    /**
     * Get the PKs of the DOIs for which a DataCite type lookup has been tried unsuccessfully at least [minTries] times.
     *
     * This will typically be called with an upper limit beyond which we want to abandon retries, or zero if we want to
     * ignore all previously tried DOIs.
     */
    fun getTriedTypeLookups(minTries: Int): Set<Long> =
        jdbcTemplate.queryForList(
            "SELECT identifier_pk FROM datacite_type_lookup WHERE try_count >= ?",
            Long::class.java, minTries
        ).toSet()

    fun removeTriedTypeLookups(identifierPks: Set<Long>) {
        npTemplate.batchUpdate(
            """
                DELETE FROM datacite_type_lookup
                WHERE identifier_pk = :identifierPk
                """,
            identifierPks.map {
                mapOf("identifierPk" to it)
            }.toTypedArray()
        )
    }

    fun addTriedTypeLookups(identifierPks: Set<Long>) {
        identifierPks.sorted().chunked(1000) { chunk ->
            if (chunk.isNotEmpty()) {
                npTemplate.batchUpdate(
                    """
                        INSERT INTO datacite_type_lookup (identifier_pk)
                        SELECT (:identifierPk)
                        WHERE NOT EXISTS (
                            SELECT identifier_pk
                            FROM datacite_type_lookup 
                            WHERE identifier_pk = :identifierPk
                        )
                        ON CONFLICT (identifier_pk) DO NOTHING
                        """,
                    identifierPks.map {
                        mapOf("identifierPk" to it)
                    }.toTypedArray()
                )
            }
        }
    }

    fun incrementTriedTypeLookups(identifierPks: Set<Long>) {
        npTemplate.batchUpdate(
            """
                UPDATE datacite_type_lookup
                SET try_count = try_count + 1
                WHERE identifier_pk = :identifierPk
                """,
            identifierPks.map {
                mapOf("identifierPk" to it)
            }.toTypedArray()
        )
    }
}
