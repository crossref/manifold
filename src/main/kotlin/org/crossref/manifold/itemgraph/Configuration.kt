package org.crossref.manifold.itemgraph

object Configuration {
    const val MAX_RETURNED_ROWS = "max-returned-rows"
    const val DEFAULT_RETURNED_ROWS = "default-returned-rows"

    const val ITEM_TREE_UPDATE = "item-tree-update"
    const val ENABLED = "enabled"
}
