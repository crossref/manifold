package org.crossref.manifold.itemgraph

import org.crossref.manifold.api.RelationshipTypesView
import org.springframework.jdbc.core.JdbcTemplate
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate
import org.springframework.jdbc.core.simple.SimpleJdbcInsert
import org.springframework.stereotype.Repository

@Repository
class RelationshipTypeDao(
    private val jdbcTemplate: JdbcTemplate,
) {
    private val npTemplate: NamedParameterJdbcTemplate = NamedParameterJdbcTemplate(jdbcTemplate)

    /**
     * Retrieve the pk for this RelType, if known.
     */
    fun resolveRelationshipTypeRO(relationshipType: String): Int? {
        val params = mapOf("value" to relationshipType)

        return npTemplate.queryForList(
            "select pk from relationship_type where value = :value",
            params,
            Int::class.java
        ).firstOrNull()?.toInt()
    }

    /**
     * Retrieve the pk for this RelType.
     * Create if necessary.
     */
    fun resolveRelationshipType(relationshipType: String): Int {
        val params = mapOf("value" to relationshipType)

        return resolveRelationshipTypeRO(relationshipType)
            ?: SimpleJdbcInsert(jdbcTemplate).withTableName("relationship_type")
                .apply { usingGeneratedKeyColumns("pk") }
                .executeAndReturnKey(
                    params
                ).toInt()
    }

    fun getRelationshipTypes(): RelationshipTypesView =
        RelationshipTypesView(jdbcTemplate.queryForList("select value from relationship_type", String::class.java))
}
