package org.crossref.manifold.itemgraph

import com.fasterxml.jackson.databind.ObjectMapper
import org.crossref.manifold.itemtree.compact.CompactItemTree
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.dao.ConcurrencyFailureException
import org.springframework.jdbc.core.JdbcTemplate
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate
import org.springframework.retry.annotation.Backoff
import org.springframework.retry.annotation.Retryable
import org.springframework.stereotype.Repository
import org.springframework.transaction.annotation.Isolation
import org.springframework.transaction.annotation.Transactional
import java.sql.SQLException
import java.time.InstantSource
import java.time.OffsetDateTime
import java.time.ZoneOffset

@Repository
class ItemTreeAssertionDao(
    jdbcTemplate: JdbcTemplate,
    private val instantSource: InstantSource,
    private val mapper: ObjectMapper
) {
    private val npTemplate: NamedParameterJdbcTemplate = NamedParameterJdbcTemplate(jdbcTemplate)
    private var logger: Logger = LoggerFactory.getLogger(this::class.java)

    companion object {
        // We'll be getting arbitrary sized inputs, so need to split them to avoid overflow bugs with over-sized batch.
        const val CHUNK_SIZE = 100
    }

    /**
     * Insert item tree assertions into the item_tree_assertion table.
     *
     * Caller MUST NOT provide duplicate assertions against the same Root Item PK for a given asserting party. This is
     * enforced by a check.
     *
     * This uses Repeatable Read isolation because it deletes and re-inserts. The deletion is necessary to ensure the
     * unique constraint is maintained.
     *
     * @param[treeAssertions] list of pairs of Item Tree, Asserting Party PK.
     * @throws[SQLException] if there was a problem inserting the data.
     */
    @Retryable(retryFor = [ConcurrencyFailureException::class] , maxAttempts = 3, backoff = Backoff(delay = 100))
    @Transactional(isolation = Isolation.REPEATABLE_READ)
    fun assertItemTrees(treeAssertions: Collection<CompactItemTreeAssertion>) {
        logger.info("Assert ${treeAssertions.count()} item trees")

        // This is a hard precondition.
        val dupes = treeAssertions.groupBy { Pair(it.assertingPartyPk, it.rootItemPk) }.filter { it.value.count() > 1 }
        for (dupe in dupes) {
            logger.error("Duplicate assertion for party: ${dupe.key.first}, root: ${dupe.value}): $dupe")
        }
        check(dupes.isEmpty()) { "Duplicate assertions found, stopping. " }

        treeAssertions.chunked(CHUNK_SIZE) { chunk ->
            val props = chunk.map {
                val serialized =  mapper.writer().writeValueAsString(it.tree)
                MapSqlParameterSource(
                    mapOf(
                        "asserted_at" to it.assertedAt,
                        "ingested_at" to instantSource.instant().atOffset(ZoneOffset.UTC),
                        "item_tree" to serialized,
                        "party_pk" to it.assertingPartyPk,
                        "root_item_pk" to it.rootItemPk,
                        "envelope_pk" to it.envelopePk
                    )
                )
            }.toTypedArray()

            npTemplate.batchUpdate(
                """
                INSERT INTO item_tree_assertion (
                    asserted_at,
                    ingested_at,
                    item_tree,
                    party_pk,
                    root_item_pk,
                    envelope_pk) 
                VALUES (
                    :asserted_at,
                    :ingested_at,
                    :item_tree ::jsonb,
                    :party_pk,
                    :root_item_pk,
                    :envelope_pk);""".trimIndent(), props
            )
        }
    }

    /**
     * For a set of itemPks, retrieve all current assertions by any party.
     * Uses the item_tree_assertion_latest index.
     */
    fun getAllCurrentForRoot(itemPks: Set<Long>): Collection<CompactItemTree> =
        if (itemPks.isEmpty()) {
            emptyList()
        } else {
            val itemTrees = mutableSetOf<CompactItemTree>()
            itemPks.chunked(1000) { chunk ->
                itemTrees.addAll(
                    npTemplate.query(
                        """
                        SELECT DISTINCT ON (root_item_pk, party_pk)
                            asserted_at,
                            ingested_at,
                            item_tree,
                            party_pk,
                            root_item_pk,
                            envelope_pk
                        FROM item_tree_assertion
                        WHERE root_item_pk IN (:root_item_pks)
                        ORDER BY root_item_pk, party_pk, ingested_at
                        """.trimIndent(),
                        mapOf("root_item_pks" to chunk)
                    ) { rs, _ ->
                        mapper.readValue(rs.getString("item_tree"), CompactItemTree::class.java)
                    }.filterNotNull()
                )
            }
            itemTrees
        }

    /**
     * Get Item Tree assertions from the current table by the assertion PKs, where they exist.
     * Return CompactItemTreeAssertion, along with the PK of the assertion itself.
     */
    fun getByAssertionPks(assertionPks: Set<Long>): List<Pair<CompactItemTreeAssertion, Long>> =
        if (assertionPks.isEmpty()) {
            emptyList()
        } else {
            val byAssertionPks = mutableListOf<Pair<CompactItemTreeAssertion, Long>>()
            assertionPks.chunked(1000) { chunk ->
                byAssertionPks.addAll(
                    npTemplate.query(
                        """
                        SELECT
                            pk,
                            asserted_at,
                            ingested_at,
                            item_tree,
                            party_pk,
                            root_item_pk,
                            envelope_pk
                        FROM item_tree_assertion
                        WHERE pk IN (:pks)
                        """.trimIndent(),
                        mapOf("pks" to chunk)
                    ) { rs, _ ->
                        // Only if tree is non-null.
                        mapper.readValue(rs.getString("item_tree"), CompactItemTree::class.java)?.let {
                            Pair(
                                CompactItemTreeAssertion(
                                    it,
                                    rs.getLong("party_pk"),
                                    rs.getLong("root_item_pk"),
                                    rs.getObject("asserted_at", OffsetDateTime::class.java),
                                    rs.getLong("envelope_pk"),
                                ),
                                rs.getLong("pk")
                            )
                        }
                    }.filterNotNull()
                )
            }
            byAssertionPks
        }

    /**
     * Given a set of rootPk, partyPk, return the most recent Item Tree for each.
     */
    fun getCurrentByRootParty(rootItemPk: Long, assertingPartyPk: Long): CompactItemTree? =
            npTemplate.query(
                """
            SELECT DISTINCT ON (root_item_pk, party_pk)
                asserted_at,
                ingested_at,
                item_tree,
                party_pk,
                root_item_pk,
                envelope_pk
            FROM item_tree_assertion
            WHERE root_item_pk = :root_item_pk AND party_pk = :party_pk
            ORDER BY root_item_pk, party_pk, ingested_at""".trimIndent(),
                mapOf("root_item_pk" to rootItemPk, "party_pk" to assertingPartyPk)
            ) { rs, _ ->
                mapper.readValue(rs.getString("item_tree"), CompactItemTree::class.java)
            }.firstOrNull()

}

