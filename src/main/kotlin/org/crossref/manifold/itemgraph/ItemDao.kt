package org.crossref.manifold.itemgraph

import org.crossref.manifold.identifiers.IdentifierParser
import org.crossref.manifold.identifiers.IdentifierType
import org.crossref.manifold.identifiers.parsers.Doi
import org.crossref.manifold.identifiers.parsers.Util
import org.crossref.manifold.itemtree.Identifier
import org.crossref.manifold.itemtree.Item
import org.crossref.manifold.lookup.RALookupDao
import org.crossref.manifold.relationship.RelationshipSyncDao
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.jdbc.core.JdbcTemplate
import org.springframework.jdbc.core.PreparedStatementSetter
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate
import org.springframework.stereotype.Repository
import org.springframework.transaction.annotation.Propagation
import org.springframework.transaction.annotation.Transactional
import java.net.URI
import java.net.URISyntaxException
import java.sql.ResultSet


/** Pair of Identifier PK and corresponding Item PK.
 * A [Pair<Long, Long>] would be confusing and error-prone.
 */
data class IdentifierPkItemPk(val identifierPk: Long, val itemPk: Long)

/** DAO for Items and their Identifiers. Because Items are retrieved by their Identifiers these are grouped into the
 * same DAO.
 */
@Repository
class ItemDao(
    private val jdbcTemplate: JdbcTemplate,
    private val raLookupDao: RALookupDao,
    private val relationshipSyncDao: RelationshipSyncDao
) {
    private val npTemplate: NamedParameterJdbcTemplate = NamedParameterJdbcTemplate(jdbcTemplate)

    private val logger: Logger = LoggerFactory.getLogger(this::class.java)

    /**
     * Representation of an Identifier (which wraps URI), as stored in the database. Only used internally in ItemDao.
     * Brought out into a separate class to encapsulate the structure, which is a bit different to URI.
     *
     * URIs can be hierarchical or opaque (i.e. non-hierarchical) and may or may not have hosts (see the documentation
     * of URI). To represent these in a database they must be decomposed in a way that works with both kinds of URI.
     *
     * @param[typePk] The ID of the type, per [org.crossref.manifold.identifiers.IdentifierType]
     * @param[scheme] The URI scheme. Present in most cases, but can be missing in some cases.
     * @param[host] The host. Present in most cases, but can be missing in some cases.
     * @param[port] The host port. If positive, represents the port, if present. -1 represents no port specified.
     * @param[rest] The rest of URI, less the host and port if present. Includes path, query parameters and fragment. Must be URI-encoded.
     */
    data class DbUri(
        val typePk: Int,

        val scheme: String?,

        val host: String?,

        val port: Int,

        val rest: String,
    ) {
        companion object {
            fun fromIdentifier(identifier: Identifier): DbUri {
                // URI always returns a port, even when there's no host.
                // Per URI's behaviour, when no port is supplied it's represented as -1.
                val port = identifier.uri.port

                // Opaque URIs are rare. We take what the java.net.URI library calls the scheme specific part:
                // everything after the scheme, excluding fragment.
                val schemeSpecificPart = if (identifier.uri.isOpaque) {
                    identifier.uri.rawSchemeSpecificPart
                } else {
                    // These are more conventional. 'Rest' is path + query + fragment.
                    // Authority includes any present user, host, port. We'll remove this all, leaving what remains
                    // of the scheme-specific part.
                    // Some valid non-opaque URIs may have no authority, e.g. the string "frog".
                    val toRemove =
                        (identifier.getScheme()?.let { "$it:" } ?: "") +
                                (if (identifier.uri.rawAuthority != null) {
                                    "//${identifier.uri.rawAuthority}"
                                } else {
                                    ""
                                })

                    identifier.uri.toASCIIString().substring(toRemove.length)
                }

                // May be null if it's an opaque URI, or a hierarchical one with a null host.
                val host: String? = identifier.uri.host

                return DbUri(
                    identifier.type.id,
                    identifier.uri.scheme,
                    host,
                    port,
                    schemeSpecificPart
                )
            }
        }

        fun toIdentifier(): Identifier {
            // If this was a hierarchical URI with a host, rebuild that part into the scheme-specific-part.
            val uri = URI(Util.build(this.scheme, this.host, this.port, this.rest))

            return IdentifierParser.fromUri(uri, this.typePk)
        }
    }

    /**
     * If the ItemIdentifier exists, add its PK and return, along with its Item's PK.
     * If not, return the ItemIdentifier unchanged.
     * @return The Identifier and Item PKs, if found.
     *
     */
    @Transactional(propagation = Propagation.SUPPORTS)
    fun findItemAndIdentifierPk(identifier: Identifier): IdentifierPkItemPk? =
        npTemplate.query(
            """WITH
                    scheme AS (SELECT pk FROM scheme WHERE val = :scheme AND val_null = :scheme_null),
                    host AS (SELECT pk FROM host WHERE val = :host AND val_null = :host_null)
                    SELECT iid.pk as identifier_pk, item_pk 
                    FROM item_identifier iid
                    INNER JOIN scheme ON iid.scheme_pk = scheme.pk
                    INNER JOIN host ON iid.host_pk = host.pk
                    WHERE 
                    iid.scheme_specific_part = :scheme_specific_part AND
                    iid.port = :port 
            """.trimIndent(),
            unresolvedRowMapper(DbUri.fromIdentifier(identifier))

        ) { rs: ResultSet, _: Int ->
            IdentifierPkItemPk(identifierPk = rs.getLong("identifier_pk"), itemPk = rs.getLong("item_pk"))
        }.firstOrNull()

    /**
     * For a collection of Identifier values, return a map that can be used to resolve them.
     * Read only, so will retrieve only those Identifiers already known. Those not yet known are not returned.
     * @return map of pairs of Identifier PK and corresponding Item PK.
     *
     * The Identifier object is an immutable data class, so it's fine to use as a key in the resolution map.
     */
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    fun getIdentifierMappingsRO(
        identifiers: Collection<Identifier>,
    ): Map<Identifier, IdentifierPkItemPk> = if (identifiers.isEmpty()) {
        // Special case, the SQL templating doesn't like an empty list.
        emptyMap()
    } else {

        // Each identifier in the list is combined with a unique number which is passed through.
        // This will enable us to match them up the end.
        val indexed = identifiers.mapIndexed { index, identifier -> index to identifier }
        val indexedLookup = indexed.toMap()

        val mappedRows = indexed.map { (index, identifier) -> Pair(index, unresolvedIdentifierRowMapper(identifier)) }

        // Resolve PKs for scheme and host up-front. There's a comparatively small number,
        // and this saves a join in the database and attendant temp table index-building.
        val rows = mappedRows.map { it.second }
        val hostMapping = hostMappingForRows(rows)
        val schemeMapping = schemeMappingForRows(rows)


        // Resolve the PKs and also add an index which will let us collate the result.
        val forInsertion = mappedRows
            .map { (index, row) ->
                row +
                        ("scheme_pk" to schemeMapping[row["scheme"] to row["scheme_null"]]) +
                        ("host_pk" to hostMapping[row["host"] to row["host_null"]]) +
                        ("index" to index)
            }

        jdbcTemplate.execute(
            """
            CREATE TEMP TABLE identifiers_to_resolve (  
                scheme_pk BIGINT,
                host_pk BIGINT,
                port INT NOT NULL,
                scheme_specific_part VARCHAR NOT NULL,
                index INTEGER NOT NULL
            )  ON COMMIT DROP
            """
        )

        npTemplate.batchUpdate(
            """INSERT INTO identifiers_to_resolve (
                scheme_pk, 
                host_pk, 
                port,
                scheme_specific_part,
                index
            ) VALUES (
                :scheme_pk,
                :host_pk,
                :port,
                :scheme_specific_part,
                :index)""".trimMargin(),
            forInsertion.toTypedArray()
        )

        jdbcTemplate.execute(
            "CREATE INDEX ON identifiers_to_resolve (scheme_specific_part, host_pk, scheme_pk, port)",
        )
        jdbcTemplate.execute(
            "ANALYZE identifiers_to_resolve",
        )

        val result = mutableMapOf<Identifier, IdentifierPkItemPk>()

        val preparedStatementSetter = PreparedStatementSetter { ps -> ps.fetchSize = 1000 }
        jdbcTemplate.queryForStream(
            """ 
                SELECT 
                    itr.index AS index,
                    iid.pk AS identifier_pk,
                    item_pk
                FROM identifiers_to_resolve itr
                JOIN item_identifier iid 
                    ON iid.scheme_specific_part = itr.scheme_specific_part
                    AND iid.host_pk = itr.host_pk
                    AND iid.scheme_pk = itr.scheme_pk
                    AND iid.port = itr.port
            """,
            preparedStatementSetter,
        ) { rs: ResultSet, _: Int ->
            val index = rs.getInt("index")

            val identifierPk = rs.getLong("identifier_pk")
            val itemPk = rs.getLong("item_pk")

            index to IdentifierPkItemPk(identifierPk, itemPk)
        }.forEach { (index: Int, ids) ->
            // If the index doesn't exist then there's been a problem. Don't make it worse with a NPE.
            indexedLookup[index]?.let {
                result[it] = ids
            }
        }

        result
    }

    private fun unresolvedIdentifierRowMapper(identifier: Identifier): Map<String, *> {
        // This is checked in calling code.
        val dbIdentifier = DbUri.fromIdentifier(identifier)

        return mapOf(
            "scheme" to (dbIdentifier.scheme ?: ""),
            "scheme_null" to (dbIdentifier.scheme == null),
            "host" to (dbIdentifier.host ?: ""),
            "host_null" to (dbIdentifier.host == null),
            "port" to identifier.getPort(),
            "scheme_specific_part" to dbIdentifier.rest,
            "type_id" to identifier.type.id
        )
    }

    /**
     * @param[identifier] Takes a DbUri so that the data format matches what's in the database.
     */
    private fun unresolvedRowMapper(identifier: DbUri): Map<String, *> =
        mapOf(
            "scheme" to (identifier.scheme ?: ""),
            "scheme_null" to (identifier.scheme == null),
            "host" to (identifier.host ?: ""),
            "host_null" to (identifier.host == null),
            "port" to identifier.port,
            "scheme_specific_part" to identifier.rest,
        )

    private fun identifierRowMapper(rs: ResultSet): Identifier? {
        val typeId = rs.getInt("type_id")
        val scheme = rs.getString("scheme")
        val schemeNull = rs.getBoolean("scheme_null")
        val host = rs.getString("host")
        val hostNull = rs.getBoolean("host_null")
        val port = rs.getInt("port")
        val schemeSpecificPart = rs.getString("scheme_specific_part")

        return try {
            DbUri(
                typeId,
                if (schemeNull) null else scheme,
                if (hostNull) null else host,
                port,
                schemeSpecificPart,
            ).toIdentifier()
        } catch (use: URISyntaxException) {
            logger.warn(use.message)
            null
        }
    }

    /**
     * For a set of ItemPks, retrieve a mapping of each Item PK to its Item PK and resolved identifier.
     */
    fun fetchItemIdentifierPairsForItemPks(itemPks: Set<Long>): Collection<Pair<Long, Identifier>> =
        // Don't let the query get to long, or we'll cause an 'out-of-range integer' error in JDBC.
        itemPks.chunked(1000) { chunk ->
            // Need to guard, Postgres doesn't like an empty list.
            if (chunk.isEmpty()) {
                emptyList()
            } else {
                npTemplate.query(
                    """
                SELECT 
                    itr.pk as identifier_pk,
                    item_pk,
                    s.val as scheme,
                    s.val_null as scheme_null,
                    h.val as host,
                    h.val_null as host_null,
                    port,
                    type_id,
                    scheme_specific_part
                FROM item_identifier itr
                JOIN scheme s ON s.pk = itr.scheme_pk 
                JOIN host h ON h.pk = itr.host_pk
                WHERE item_pk IN (:item_pks)
                """.trimIndent(),
                    mapOf("item_pks" to chunk)
                ) { rs, _ ->
                    val identifier = identifierRowMapper(rs)
                    Pair(
                        rs.getLong("item_pk"),
                        identifier
                    )
                }.filter { it.second != null }.map { it.first to it.second!! }
            }
        }.flatten()

    fun ensure(items: List<Item>) {
        val ids = items.mapNotNull { it.identifiers.firstOrNull() }.distinct()

        val mappedRows = ids.map {
            unresolvedIdentifierRowMapper(it).run {
                // If the identifier is a DOI add its prefix for use during RA lookup.
                if (it.type == IdentifierType.DOI) this + ("doi_prefix" to Doi.doiPrefix(it)) else this
            }
        }

        ensureHostsIn(mappedRows)
        val hostMapping = hostMappingForRows(mappedRows)

        ensureSchemesIn(mappedRows)
        val schemeMapping = schemeMappingForRows(mappedRows)
        // ensureIdentifiersIn(...) returns the mapped rows including their identifier primary keys...
        val identifierPrefixes = ensureIdentifiersIn(mappedRows, hostMapping, schemeMapping)
            // ...so we can extract the identifier primary keys and corresponding DOI prefixes (if any) for RA lookup.
            .filter { it["doi_prefix"] != null }
            .associate { it["identifier_pk"] as Long to it["doi_prefix"] as String }
        raLookupDao.addPrefixes(identifierPrefixes.values.toSet())
        val syncId = relationshipSyncDao.newRelationshipSync()
        raLookupDao.addIdentifierPrefixes(syncId, identifierPrefixes)
        relationshipSyncDao.relationshipSyncReady(syncId)
    }

    /**
     * Get a page of identifiers for the given RA that do not have a known subtype, paired with their identifier PKs
     */
    fun getRADoisWithUnknownTypes(ra: String, startKey: Long, pageSize: Int): List<Pair<Long, Identifier>> =
        jdbcTemplate.query(
            """
            SELECT ii.pk, 
                s.val as scheme,
                s.val_null as scheme_null,
                h.val as host,
                h.val_null as host_null,
                ii.port,
                ii.type_id,
                ii.scheme_specific_part
            FROM item_identifier ii
            JOIN scheme s ON s.pk = ii.scheme_pk 
            JOIN host h ON h.pk = ii.host_pk
            JOIN identifier_ra_lookup iral ON ii.pk = iral.identifier_pk
            JOIN ra_lookup ral ON iral.ra_lookup_pk = ral.pk
            LEFT JOIN item_info itin ON ii.item_pk = itin.root_item_pk
            WHERE ii.pk > ?
            AND ral.registration_agency_pk = (
                SELECT pk FROM registration_agency WHERE name = ?
            ) AND itin.subtype_pk IS NULL
            ORDER BY ii.pk
            LIMIT ?
        """.trimIndent(), { rs, _ ->
                Pair(
                    rs.getLong("pk"),
                    identifierRowMapper(rs)
                )
            }, startKey, ra, pageSize
        ).filter { it.second != null }.map { it.first to it.second!! }

    private fun ensureHostsIn(mappedRows: List<Map<String, *>>) {
        // Get the distinct hosts, with stable order for database contention.
        // Need to keep the host_null field too.
        val hosts = mappedRows.map { row -> mapOf("host" to row["host"], "host_null" to row["host_null"]) }
            .distinct()
            .sortedBy { it["host"] as String? }

        hosts.chunked(1000) { chunk ->
            if (chunk.isNotEmpty()) {
                // If we don't check first we'll use up the sequence for the PK every single time.
                npTemplate.batchUpdate(
                    """INSERT INTO host (val, val_null)
                SELECT :host, :host_null
                WHERE NOT EXISTS (SELECT val, val_null FROM host WHERE val = :host AND val_null = :host_null) 
                ON CONFLICT (val, val_null) DO NOTHING
                """.trimMargin(),
                    chunk.toTypedArray()
                )
            }
        }
    }

    /**
     * For a set of database rows return a mapping of (host, host_null) to host pk.
     * If the entries don't exist, they won't be present in the output mapping.
     */
    private fun hostMappingForRows(mappedRows: List<Map<String, *>>): Map<Pair<String, Boolean>, Long> {
        // Get the distinct hosts, with stable order for database contention.
        // Need to keep the host_null field too.
        val hosts = mappedRows.map { row -> mapOf("host" to row["host"], "host_null" to row["host_null"]) }
            .distinct()
            .sortedBy { it["host"] as String? }

        val resolved = mutableMapOf<Pair<String, Boolean>, Long>()

        for (host in hosts) {
            val result = npTemplate.query(
                """            
            SELECT pk FROM host WHERE val = :host AND val_null = :host_null
            """.trimIndent(),
                host,
            ) { rs: ResultSet, _: Int ->
                rs.getLong("pk")
            }

            // May not be present.
            result.firstOrNull()?.let {
                resolved[Pair(host["host"] as String, host["host_null"] as Boolean)] = it
            }
        }

        return resolved
    }

    private fun ensureSchemesIn(mappedRows: List<Map<String, *>>) {
        // Get the distinct hosts, with stable order for database contention.
        // Need to keep the host_null field too.
        val schemes = mappedRows.map { row -> mapOf("scheme" to row["scheme"], "scheme_null" to row["scheme_null"]) }
            .distinct()
            .sortedBy { it["scheme"] as String? }

        schemes.chunked(1000) { chunk ->

            if (chunk.isNotEmpty()) {

                npTemplate.batchUpdate(
                    """INSERT INTO scheme (val, val_null)
                SELECT :scheme, :scheme_null
                WHERE NOT EXISTS (SELECT val, val_null FROM scheme WHERE val = :scheme AND val_null = :scheme_null)
                ON CONFLICT (val, val_null) DO NOTHING
                """.trimMargin(),
                    schemes.toTypedArray()
                )
            }
        }
    }

    /**
     * For a set of database rows return a mapping of (scheme, scheme_null) to host pk.
     * If the entries don't exist, they won't be present in the output mapping.
     */

    private fun schemeMappingForRows(mappedRows: List<Map<String, *>>): Map<Pair<String, Boolean>, Long> {
        // Get the distinct hosts, with stable order for database contention.
        // Need to keep the host_null field too.
        val schemes = mappedRows.map { row -> mapOf("scheme" to row["scheme"], "scheme_null" to row["scheme_null"]) }
            .distinct()
            .sortedBy { it["scheme"] as String? }

        val resolved = mutableMapOf<Pair<String, Boolean>, Long>()

        for (scheme in schemes) {
            val result = npTemplate.query(
                """            
            SELECT pk FROM scheme WHERE val = :scheme AND val_null = :scheme_null
            """.trimIndent(),
                scheme,
            ) { rs: ResultSet, _: Int ->
                rs.getLong("pk")
            }

            // May not be present.
            result.firstOrNull()?.let {
                resolved[Pair(scheme["scheme"] as String, scheme["scheme_null"] as Boolean)] = it
            }
        }

        return resolved
    }

    /*
     * Returns the mapped rows including their internally generated item and identifier primary keys.
     * These are used by the downstream RA lookup which technically breaks encapsulation but it's the pragmatic choice
     * and it's a private function so no real harm done!
     */
    private fun ensureIdentifiersIn(
        mappedRows: List<Map<String, *>>,
        hostMapping: Map<Pair<String, Boolean>, Long>,
        schemeMapping: Map<Pair<String, Boolean>, Long>
    ): List<Map<String, *>> {
        val distinctRows = mappedRows.distinct()

        val zippedRows = mutableListOf<Map<String, *>>()

        distinctRows.chunked(100) { chunk ->
            if (chunk.isNotEmpty()) {
                val newItemPks = npTemplate.query(
                    "SELECT nextval('item_pk_seq') as pk FROM generate_series(1, :num)",
                    mapOf("num" to chunk.count())
                ) { rs: ResultSet, _: Int -> rs.getLong("pk") }

                npTemplate.batchUpdate(
                    "INSERT INTO item (pk, created_at) VALUES (:val, NOW())",
                    newItemPks.map { pk -> mapOf("val" to pk) }.toTypedArray()
                )

                val newIdentifierPks = npTemplate.query(
                    "SELECT nextval('item_identifier_pk_seq') as pk FROM generate_series(1, :num)",
                    mapOf("num" to chunk.count())
                ) { rs: ResultSet, _: Int -> rs.getLong("pk") }

                val rows = chunk
                    // Insert in stable order for contention.
                    .sortedBy {
                        listOf(
                            it["scheme_specific_part"],
                            it["host_pk"],
                            it["scheme_pk"],
                            it["port"]
                        ).toString()
                    }
                    // Associate with generated new item pks.
                    .zip(newItemPks)
                    // Associate with generated new identifier pks
                    .zip(newIdentifierPks)
                    .map { (row, identifierPk) ->
                        row.first +
                                ("identifier_pk" to identifierPk) +
                                ("item_pk" to row.second) +
                                ("scheme_pk" to schemeMapping[row.first["scheme"] to row.first["scheme_null"]]) +
                                ("host_pk" to hostMapping[row.first["host"] to row.first["host_null"]])
                    }

                npTemplate.batchUpdate(
                    """INSERT INTO item_identifier (
                        pk, scheme_pk, port, host_pk, scheme_specific_part, item_pk, type_id
                    ) SELECT :identifier_pk, :scheme_pk, :port, :host_pk, :scheme_specific_part, :item_pk, :type_id
                    ON CONFLICT (scheme_specific_part, host_pk, scheme_pk, port) 
                    DO NOTHING""".trimIndent(),
                    rows.toTypedArray()
                )

                zippedRows.addAll(rows)
            }
        }

        return zippedRows
    }
}
