package org.crossref.manifold.itemgraph

import org.crossref.manifold.identifiers.IdentifierParser
import org.crossref.manifold.itemgraph.Configuration.ENABLED
import org.crossref.manifold.itemgraph.Configuration.ITEM_TREE_UPDATE
import org.crossref.manifold.itemtree.CanonicalItemTreeDao
import org.crossref.manifold.itemtree.Canonicalizer
import org.crossref.manifold.itemtree.ItemInfo
import org.crossref.manifold.itemtree.ItemInfoDao
import org.crossref.manifold.modules.consts.Items
import org.crossref.manifold.registries.RelationshipTypeRegistry
import org.crossref.manifold.relationship.RelationshipSyncDao
import org.crossref.manifold.rendering.RenderStatusDao
import org.crossref.manifold.util.Constants.BATCH_SIZE
import org.crossref.manifold.util.Constants.FIXED_DELAY
import org.crossref.manifold.util.Constants.INITIAL_DELAY
import org.crossref.manifold.util.Constants.PRODUCE_ITEM_TREE_JSON
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Service
import java.util.concurrent.TimeUnit

@Service
@ConditionalOnProperty(prefix = ITEM_TREE_UPDATE, name = [ENABLED])
class ItemTreeUpdater(
    @Value("\${$ITEM_TREE_UPDATE.$BATCH_SIZE:100}") private val batchSize: Int,
    @Value("\${$ITEM_TREE_UPDATE.$PRODUCE_ITEM_TREE_JSON:false}") private val produceJson: Boolean,

    val queueDao: ItemTreeUpdateQueueDao,
    private val renderStatusDao: RenderStatusDao,
    private val itemTreeAssertionDao: ItemTreeAssertionDao,
    private val relationshipMatchDao: RelationshipMatchDao,
    private val canonicalizer: Canonicalizer,
    private val canonicalItemTreeDao: CanonicalItemTreeDao,
    private val itemInfoDao: ItemInfoDao,
    private val relationshipSyncDao: RelationshipSyncDao,
    private val itemDao: ItemDao,
    private val relationshipDao: RelationshipDao,
    private val relationshipTypeRegistry: RelationshipTypeRegistry
) {
    private var logger: Logger = LoggerFactory.getLogger(this::class.java)

    @Scheduled(
        initialDelayString = "\${$ITEM_TREE_UPDATE.$INITIAL_DELAY:0}",
        fixedDelayString = "\${$ITEM_TREE_UPDATE.$FIXED_DELAY:1}",
        timeUnit = TimeUnit.SECONDS
    )
    fun run() {
        logger.debug("Checking for stale items that need to be updated")

        if (batchSize > 200) {
            logger.error("ITEM_TREE_UPDATE_BATCH_SIZE too large could cause 'out of range integer' SQL errors.")
        }

        try {
            val crossrefItemPk =
                itemDao.findItemAndIdentifierPk(IdentifierParser.parse(Items.CROSSREF_AUTHORITY))?.itemPk
            val stewardRelTypePk = relationshipTypeRegistry.resolve("steward")

            var lastBatchCount: Int? = null
            while (lastBatchCount == null || lastBatchCount > 0) {

                // Make sure we terminate the loop if there's a failure polling.
                lastBatchCount = 0

                queueDao.getFromQueue(batchSize) { itemPksToRender ->

                    // Set the count, so we know whether to loop next time.
                    lastBatchCount = itemPksToRender.count()

                    logger.debug("Got ${itemPksToRender.count()} from tree updater queue. Of which ${itemPksToRender.count()} distinct.")

                    val trees = itemTreeAssertionDao.getAllCurrentForRoot(itemPksToRender)
                    val relationships = relationshipMatchDao.getAllCurrentForRoot(itemPksToRender)

                    // These are a mix of tree assertions for a range of root node ids, made by a range of parties.
                    val treesPerRootNode = trees.groupBy { it.root.pk }
                    val relationshipMatchesPerRootNode = relationships.groupBy { it.rootItemPk }

                    val canonicalTrees = treesPerRootNode.mapNotNull { (rootPk, treesForRoot) ->
                        rootPk?.let {
                            val relationshipsForRoot = relationshipMatchesPerRootNode.getOrDefault(it, emptyList())
                            canonicalizer.canonicalize(treesForRoot, relationshipsForRoot)
                        }
                    }

                    val itemInfos = canonicalTrees.map { itemTreeEntry ->
                        val typePk = itemTreeEntry.type?.let {
                            itemInfoDao.addOrGetItemType(it)
                        }
                        val subtypePk = itemTreeEntry.subtype?.let {
                            itemInfoDao.addOrGetItemSubtype(it)
                        }
                        val stewardPk = crossrefItemPk?.let { cipk ->
                            stewardRelTypePk?.let { srtpk ->
                                relationshipDao.fetchSimpleRelationships(
                                    subjectItemPk = itemTreeEntry.rootItemPk,
                                    relationshipTypePk = srtpk,
                                    assertedByPk = cipk
                                ).firstOrNull()?.objectItemPk
                            }
                        }
                        ItemInfo(itemTreeEntry.rootItemPk, typePk, subtypePk, stewardPk)
                    }

                    val syncId = relationshipSyncDao.newRelationshipSync()
                    itemInfoDao.saveItemInfo(itemInfos, syncId)
                    relationshipSyncDao.relationshipSyncReady(syncId)

                    if (produceJson) {
                        canonicalItemTreeDao.saveJson(canonicalTrees)
                    }

                    // Once saved, enqueue to be rendered.
                    renderStatusDao.enqueue(itemPksToRender)
                }
            }
        } catch (e: Exception) {
            logger.error("Error rendering: $e", e)
        }
    }
}
