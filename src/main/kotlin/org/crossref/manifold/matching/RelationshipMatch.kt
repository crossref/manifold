package org.crossref.manifold.matching

import java.time.OffsetDateTime

/**
 * The assertion of a relationship as found in an Item Tree. Each relationship is made with respect to a specific
 * Item Tree assertion. By including all these fields, it's possible to then use these to decorate an Item Tree.
 *
 * The @param[nodePosition] records the subject item for the relationship.
 * For simple relationships (subj-item, relationship-type, obj-item) this would be the subject of the relationship.
 * For reified relationships (subj-item, relationship-type, reified-item, "object", obj-item) this would be the index
 * of the reified item. This enables the relationship matches to be attached to those reified relationships in the
 * item tree.
 *
 *
 * @param[itemTreeAssertingParty] The asserting party for the Item Tree matched against.
 * @param[rootItemPk] The root node of the referenced Item Tree.
 * @param[assertion_pk] The PK of the item tree assertion that this matches. Can be found in current or historical item tree assertion table.
 * @param[nodePosition] The position in the tree of the subject of the relationship.
 * @param[matchingParty] The party asserting this relationship match.
 * @param[scope] An integer, supplied by the matching party. Allows for them to run multiple matchers alongside.
 * @param[subjectPk] The subject of the relationship.
 * @param[relationshipType] The type of the relationship.
 * @param[objectPk] The object of this relationship.
 * @param[state] Whether this relationship is said to be true or false (e.g. retracting a past match).
 */
data class RelationshipMatch(
    val itemTreeAssertingParty: Long,
    val itemTreeAssertedAt: OffsetDateTime,
    val rootItemPk: Long,
    val assertionPk: Long,
    val nodePosition: Int,
    val matchingParty: Long,
    val scope: Int,
    val subjectPk: Long,
    val relationshipType: Int,
    val objectPk: Long,
    val state: Boolean,
)
