package org.crossref.manifold.matching

import org.crossref.manifold.itemgraph.Resolver
import org.crossref.manifold.itemtree.ItemTree
import org.crossref.manifold.matching.MatcherContext.Companion.CROSSREF_ID
import org.springframework.stereotype.Service
import java.time.OffsetDateTime

@Service
class Matcher(
    val strategies: Collection<MatchingStrategy>,
    val resolver: Resolver,
) {

    /**
     * For an [ItemTree] extract the relationship matches based on strategies that match the given partition.
     *
     * Pass through some params about the original assertion.
     * It is assumed that when matches are made against an item tree by a party, they will assert *all* matches for that tree.
     * Return groups of relationship match, each of which should be about a single Party asserting against a single Root PK, in a single scope.
     */
    fun extractAllMatches(
        partition: Configuration.QueuePartition,
        tree: ItemTree,
        assertionPk: Long,
        assertedAt: OffsetDateTime,
        assertingPartyPk: Long,
        context: MatcherContext,
    ): Collection<RelationshipMatch> {
        return strategies.filter{ it.partition() == partition}.flatMap { it.match(tree, assertionPk, assertedAt, assertingPartyPk, context) }.toSet()
    }

    /**
     * Build a context object of data that's useful across matchers. Must be built each run, so the data is live.
     */
    fun buildContext(): MatcherContext? {
        val crossrefPk = resolver.resolveRO(CROSSREF_ID).pk ?: resolver.resolveRW(CROSSREF_ID)?.pk

        return if (crossrefPk != null) {
            MatcherContext(crossrefPk)
        } else null
    }
}