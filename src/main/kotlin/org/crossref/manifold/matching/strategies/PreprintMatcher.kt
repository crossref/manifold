package org.crossref.manifold.matching.strategies

import org.crossref.manifold.identifiers.IdentifierParser
import org.crossref.manifold.itemgraph.RelationshipTypeDao
import org.crossref.manifold.itemgraph.Resolver
import org.crossref.manifold.itemtree.ItemTree
import org.crossref.manifold.matching.Configuration
import org.crossref.manifold.matching.Configuration.MATCHING
import org.crossref.manifold.matching.Configuration.PREPRINT_API
import org.crossref.manifold.matching.MatcherContext
import org.crossref.manifold.matching.MatchingStrategy
import org.crossref.manifold.matching.RelationshipMatch
import org.crossref.manifold.modules.preprints.*
import org.crossref.manifold.util.BasicJsonMapper
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty
import org.springframework.retry.annotation.Backoff
import org.springframework.retry.annotation.Retryable
import org.springframework.stereotype.Component
import java.net.URI
import java.net.http.HttpClient
import java.net.http.HttpRequest
import java.net.http.HttpResponse
import java.time.OffsetDateTime

/**
 * Return matches to preprints for this article.
 * Matches are made in matching scope 4.
 */
@Component
@ConditionalOnProperty(prefix = MATCHING, name = [PREPRINT_API])
class PreprintMatcher(
    val relationshipTypeDao: RelationshipTypeDao,
    val itemResolver: Resolver,
    @Value("\${$MATCHING.$PREPRINT_API}") val apiUrl: String
) : MatchingStrategy {
    companion object {
        const val SCOPE = 4
        fun marpleQueryStringFrom(itemTree: ItemTree): String = MarpleInput(
            getTitles(itemTree),
            Issued(listOf(getEarliestDate(itemTree).second)),
            getAuthors(itemTree)
        ).let { BasicJsonMapper.mapper.writeValueAsString(it) }
    }

    private val logger: Logger = LoggerFactory.getLogger(this::class.java)

    private val client = HttpClient.newBuilder().build()


    @Retryable(retryFor = [Exception::class], maxAttempts = 3, backoff = Backoff(delay = 100))
    fun getMatches(request: HttpRequest): HttpResponse<String> =
        client.send(request, HttpResponse.BodyHandlers.ofString())

    fun queryMarple(itemTree: ItemTree): MarpleResponse? {
        val query = marpleQueryStringFrom(itemTree)

        val request = HttpRequest.newBuilder()
            .uri(URI.create("$apiUrl/match?task=preprint-matching&strategy=preprint-sbmv-es"))
            .POST(HttpRequest.BodyPublishers.ofString(query))
            .build()

        return try {
            val response = getMatches(request)
            if (response.statusCode() == 200) {
                BasicJsonMapper.mapper.readValue(response.body(), MarpleResponse::class.java)
            } else {
                logger.error("Failed to receive response for item: ${itemTree.root.pk}")
                logger.error("Preprint API responded with status: ${response.statusCode()}, body: ${response.body()}")
                null
            }
        } catch (e: Exception) {
            logger.error("Failed to receive response for item: ${itemTree.root.pk}")
            e.printStackTrace()
            null
        }
    }

    override fun partition(): Configuration.QueuePartition = Configuration.QueuePartition.PREPRINT

    override fun match(
        itemTree: ItemTree,
        assertionPk: Long,
        assertedAt: OffsetDateTime,
        assertingPartyPk: Long,
        context: MatcherContext,
    ): List<RelationshipMatch> {

        if (itemTree.getType() != "work" ||
            itemTree.getSubType() != "journal-article"
        ) {
            return emptyList()
        }

        val relType = relationshipTypeDao.resolveRelationshipTypeRO("has-preprint")

        if (relType == null) {
            logger.error("Can't resolve 'has-preprint' relationship.")
            return emptyList()
        }

        val rootPosition = itemTree.getPosition(itemTree.root)

        if (itemTree.root.pk == null || rootPosition == null) {
            return emptyList()
        }

        val marpleResponse = queryMarple(itemTree)

        return if (
            marpleResponse == null ||
            marpleResponse.status != "ok" ||
            marpleResponse.message.items.isEmpty()
        ) {
            emptyList()
        } else {
            marpleResponse.message.items.mapNotNull {

                val objectPk = itemResolver.resolveRW(IdentifierParser.parse(it.id))?.pk

                if (objectPk == null) {
                    null
                } else {
                    RelationshipMatch(
                        context.crossrefPk,
                        OffsetDateTime.now(),
                        itemTree.root.pk,
                        assertionPk,
                        rootPosition,
                        context.crossrefPk,
                        SCOPE,
                        itemTree.root.pk,
                        relType,
                        objectPk,
                        true
                    )
                }
            }
        }
    }

}