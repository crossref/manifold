package org.crossref.manifold.api.relationship

import org.crossref.manifold.api.ApiMessage
import org.crossref.manifold.api.RelationshipView

class RelationshipsMessage(
    nextCursor: String?,
    itemsPerPage: Int,
    val relationships: List<RelationshipView>,
) : ApiMessage(nextCursor, itemsPerPage) {
    constructor() : this(null, 0, emptyList())
}
