package org.crossref.manifold.itemtree

import com.fasterxml.jackson.core.JsonParser
import com.fasterxml.jackson.core.JsonProcessingException
import com.fasterxml.jackson.core.ObjectCodec
import com.fasterxml.jackson.databind.DeserializationContext
import com.fasterxml.jackson.databind.JsonNode
import com.fasterxml.jackson.databind.deser.std.StdDeserializer
import java.io.IOException

/**
 * Standard deserializer for ItemTree.
 * This is needed because there's an alternative [ItemTreeCompactDeserializer].
 * Simple, but allows us to use the constructor which does important things, such as constructing the lokup table.
 */
class ItemTreeDeserializer : StdDeserializer<ItemTree>(IdentifierDeserializer::class.java) {
    @Throws(IOException::class, JsonProcessingException::class)
    override fun deserialize(p: JsonParser, ctxt: DeserializationContext?): ItemTree {
        val oc: ObjectCodec = p.codec
        val node: JsonNode = oc.readTree(p)
        val root = node.get("root")
        val item = oc.treeToValue<Item>(root, Item::class.java)
        return ItemTree(item)
    }
}