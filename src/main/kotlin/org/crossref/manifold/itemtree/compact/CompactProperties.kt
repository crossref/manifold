package org.crossref.manifold.itemtree.compact

import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.databind.node.ObjectNode
import java.time.OffsetDateTime

/** Set of properties for the Item asserted by a party.
 */
data class CompactProperties(
    @JsonProperty("V")
    @get:JsonProperty("V")
    val values: ObjectNode,

    @JsonProperty("Y")
    @get:JsonProperty("Y")
    val assertedBy: CompactItem? = null,

    @JsonProperty("D")
    @get:JsonProperty("D")
    val assertedAt: OffsetDateTime? = null,
) {
    companion object
}
