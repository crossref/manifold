package org.crossref.manifold.itemtree.compact

import com.fasterxml.jackson.annotation.JsonProperty
import java.time.OffsetDateTime

data class CompactRelationship(
    /**
     * The Item object that is the object of this Relationship.
     * This is the point of recursion in the tree.
     */
    @JsonProperty("O")
    @get:JsonProperty("O")
    val obj: CompactItem,

    /**
     * Multiple parties making this assertion.
     * Allow this to be constructed with a default asserted by no-one.
     */
    @JsonProperty("Y")
    @get:JsonProperty("Y")
    val assertedBy: CompactItem? = null,

    @JsonProperty("T")
    @get:JsonProperty("T")
    val relTypePk: Int,

    @JsonProperty("D")
    @get:JsonProperty("D")
    val assertedAt: OffsetDateTime? = null,
) {
    companion object
}
