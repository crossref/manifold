package org.crossref.manifold.itemtree

import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.jdbc.core.JdbcTemplate
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate
import org.springframework.stereotype.Repository
import org.springframework.transaction.annotation.Transactional

@Repository
class ItemInfoDao(
    private val jdbcTemplate: JdbcTemplate,
) {
    private val npTemplate: NamedParameterJdbcTemplate = NamedParameterJdbcTemplate(jdbcTemplate)
    private var logger: Logger = LoggerFactory.getLogger(this::class.java)

    @Transactional
    fun saveItemInfo(itemInfos: List<ItemInfo>, syncId: Long) {
        val distinct = itemInfos.distinctBy { it.rootItemPk }

        logger.debug("Save ${itemInfos.count()} of which ${distinct.count()} distinct")

        // Prevent any deadlocks by inserting in order.
        val ordered = itemInfos.sortedBy { it.rootItemPk }

        npTemplate.batchUpdate(
            """
            INSERT INTO item_info (root_item_pk, type_pk, subtype_pk, steward_pk, sync_id)
            SELECT :rootItemPk, :typePk, :subtypePk, :stewardPk, :syncId
            WHERE NOT EXISTS(
                SELECT root_item_pk
                FROM item_info
                WHERE root_item_pk = :rootItemPk
                AND COALESCE(type_pk, -1) = COALESCE(:typePk, -1)
                AND COALESCE(subtype_pk, -1) = COALESCE(:subtypePk, -1)
                AND COALESCE(steward_pk, -1) = COALESCE(:stewardPk, -1))
            ON CONFLICT (root_item_pk)
            DO UPDATE SET type_pk = excluded.type_pk,
                subtype_pk = excluded.subtype_pk,
                steward_pk = excluded.steward_pk,
                sync_id = excluded.sync_id
            """.trimIndent(), ordered.map {
                mapOf(
                    "rootItemPk" to it.rootItemPk,
                    "typePk" to it.typePk,
                    "subtypePk" to it.subtypePk,
                    "stewardPk" to it.stewardPk,
                    "syncId" to syncId
                )
            }.toTypedArray()
        )
    }

    fun addOrGetItemType(itemType: String): Int =
        npTemplate.queryForObject(
            """
            WITH new_item_type AS (
            INSERT INTO item_type (name)
            SELECT :itemType
            WHERE NOT EXISTS (SELECT pk FROM item_type WHERE name = :itemType)
            ON CONFLICT (name) DO NOTHING RETURNING pk)
            SELECT COALESCE((SELECT pk FROM new_item_type), (SELECT pk FROM item_type WHERE name = :itemType))
            """.trimIndent(), mapOf("itemType" to itemType), Int::class.java
        )!!

    fun getItemTypePkFromName(typeName: String): Int? =
        jdbcTemplate.queryForList("SELECT pk FROM item_type WHERE name = ?", Int::class.java, typeName).singleOrNull()

    fun getItemTypes(): Map<Int, String> =
        jdbcTemplate.query("SELECT pk, name FROM item_type") { rs, _ ->
            rs.getInt("pk") to rs.getString("name")
        }.toMap()

    fun addOrGetItemSubtype(itemSubtype: String): Int =
        npTemplate.queryForObject(
            """
            WITH new_item_subtype AS (
            INSERT INTO item_subtype (name)
            SELECT :itemSubtype
            WHERE NOT EXISTS (SELECT pk FROM item_subtype WHERE name = :itemSubtype)
            ON CONFLICT (name) DO NOTHING RETURNING pk)
            SELECT COALESCE((SELECT pk FROM new_item_subtype), (SELECT pk FROM item_subtype WHERE name = :itemSubtype))
            """.trimIndent(), mapOf("itemSubtype" to itemSubtype), Int::class.java
        )!!

    fun getItemSubtypePkFromName(subtypeName: String): Int? =
        jdbcTemplate.queryForList("SELECT pk FROM item_subtype WHERE name = ?", Int::class.java, subtypeName)
            .singleOrNull()

    fun getItemSubtypes(): Map<Int, String> =
        jdbcTemplate.query("SELECT pk, name FROM item_subtype") { rs, _ ->
            rs.getInt("pk") to rs.getString("name")
        }.toMap()
}
