package org.crossref.manifold.itemtree

import com.fasterxml.jackson.databind.ObjectMapper
import org.crossref.manifold.itemtree.compact.CompactItemTree
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.jdbc.core.JdbcTemplate
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate
import org.springframework.stereotype.Repository
import org.springframework.transaction.annotation.Transactional

/**
 * DAO for the canonical representation of Item Trees, along with extra fields that are useful to record.
 */
@Repository
class CanonicalItemTreeDao(
    jdbcTemplate: JdbcTemplate,
    private val mapper: ObjectMapper
) {
    private val npTemplate: NamedParameterJdbcTemplate = NamedParameterJdbcTemplate(jdbcTemplate)
    private var logger: Logger = LoggerFactory.getLogger(this::class.java)

    @Transactional
    fun saveJson(entries: Collection<CanonicalItemTreeEntry>) {
        val distinct = entries.distinctBy { it.rootItemPk }

        logger.debug("Save ${entries.count()} of which ${distinct.count()} distinct")

        // Prevent any deadlocks by inserting in order.
        val ordered = entries.sortedBy { it.rootItemPk }

        npTemplate.batchUpdate(
            """
            INSERT INTO item_tree (
                item_tree,
                root_item_pk,
                update_count)
            VALUES (
                :itemTree ::jsonb,
                :rootItemPk,
                1
              )
              ON CONFLICT (root_item_pk) DO UPDATE SET
              rendered_at = excluded.rendered_at,
              item_tree = excluded.item_tree,
              update_count = excluded.update_count + 1
              """.trimIndent(), ordered.map {
                mapOf(
                    "itemTree" to mapper.writer().writeValueAsString(it.tree),
                    "rootItemPk" to it.rootItemPk,
                )
            }.toTypedArray()
        )
    }

    /**
     * For a set of itemPks, retrieve all current assertions by any party, where they exist.
     */
    fun get(itemPks: Collection<Long>): Collection<CanonicalItemTreeEntry> =
        if (itemPks.isEmpty()) {
            emptyList()
        } else {
            val itemTreeEntries = mutableListOf<CanonicalItemTreeEntry>()
            itemPks.chunked(1000) { chunk ->
                itemTreeEntries.addAll(
                    npTemplate.query(
                        """
                        SELECT
                            item_tree,
                            ii.root_item_pk,
                            itty.name AS type,
                            itsu.name AS subtype
                        FROM item_tree it
                        JOIN item_info ii ON it.root_item_pk = ii.root_item_pk
                        LEFT JOIN item_type itty ON ii.type_pk = itty.pk
                        LEFT JOIN item_subtype itsu ON ii.subtype_pk = itsu.pk
                        WHERE it.root_item_pk IN (:rootItemPks)
                        """.trimIndent(), mapOf("rootItemPks" to chunk)
                    ) { rs, _ ->
                        val tree = mapper.readValue(rs.getString("item_tree"), CompactItemTree::class.java)
                        val rootItemPk = rs.getLong("root_item_pk")
                        val type = rs.getString("type")
                        val subtype = rs.getString("subtype")
                        tree?.let {
                            CanonicalItemTreeEntry(it, rootItemPk, type, subtype)
                        }
                    }.filterNotNull()
                )
            }
            itemTreeEntries
        }

    fun get(itemPk: Long): CanonicalItemTreeEntry? = get(listOf(itemPk)).firstOrNull()
}
