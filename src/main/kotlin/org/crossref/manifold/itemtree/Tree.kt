/**
 * Immutable tree of Items, properties and relationships.
 * The tree is updated by taking an immutable copy via the copy constructors.
 * Every Item has a PK (primary key ID), which is blank until it's resolved.
 * An Item Tree is constructed without these PKs. Resolution is the process of mapping these into a Manifold Graph.
 * Each type's PK applies only within that type (i.e. an Item PK is different to a Relation PK).
 *
 * Empty companion objects are present to allow for static extension functions to be attached from other namespaces.
 *
 * Every node has a set of methods called `withX` which return a copy of the object with the given field set. These allow
 * for easy recursive, functional and immutable transformations to be made to the tree.
 */
package org.crossref.manifold.itemtree

fun unresolvedIdentifiers(itemTree: ItemTree): Set<Identifier> {
    val result = mutableSetOf<Identifier>()

    fun recurse(item: Item) {
        result.addAll(item.identifiers.filter { it.pk == null })

        item.rels.forEach { it -> recurse(it.obj) }
    }

    recurse(itemTree.root)

    return result
}
//
///**
// * Convert the tree to the collection of [RelationshipAssertionStatement]s that describe the relationships encoded in the tree.
// */
//fun toRelationshipStatements(
//    resolvedItemTree: ItemTree,
//    partyPk: Long,
//    assertedAt: OffsetDateTime
//): Set<RelationshipAssertionStatement> {
//    val result = mutableSetOf<RelationshipAssertionStatement>()
//
//    fun recurse(item: Item) {
//        result.addAll(item.rels.mapNotNull {
//            if (item.pk != null && it.obj.pk !== null && it.relTyp != null) {
//                RelationshipAssertionStatement(item.pk, it.relTyp, it.obj.pk, partyPk, true, assertedAt)
//            } else null
//        })
//        item.rels.forEach { recurse(it.obj) }
//    }
//
//    recurse(resolvedItemTree.root)
//
//    return result
//}
//
//fun toPropertyStatements(resolvedItemTree: ItemTree, partyPk: Long, assertedAt: OffsetDateTime): Set<PropertyAssertionStatement> {
//    val results = mutableSetOf<PropertyAssertionStatement>()
//
//    fun recurse(item: Item) {
//        if (item.pk != null) {
//            results.addAll(item.properties.map {
//                PropertyAssertionStatement(item.pk, it.values, partyPk, assertedAt)
//            })
//        }
//        item.rels.forEach { recurse(it.obj) }
//    }
//
//    recurse(resolvedItemTree.root)
//
//    return results
//}

/**
 * Return the set of all relationship types.
 */
fun getAllRelTypes(itemTree: ItemTree): Set<String> {
    fun recurse(item: Item): Set<String> = item.rels.fold(emptySet()) { acc, rel ->
        acc union item.rels.mapNotNull(Relationship::relTyp) union recurse(rel.obj)
    }

    return recurse(itemTree.root)
}

/**
 * Recursively count the number of Relationships.
 */
fun countRelationships(item: Item): Int = item.rels.count() + item.rels.map { countRelationships(it.obj) }.sum()

/**
 * Recursively count the number of Items.
 */
fun countItems(item: Item): Int = 1 + item.rels.map { countItems(it.obj) }.sum()


/**
 * Retrieve the set of Items whose Identifiers which are used unambiguously.
 * This means all Items which use exactly one Identifier.
 */
fun getUnambiguousUnresolvedItems(itemTree: ItemTree): Set<Item> {
    val result = mutableSetOf<Item>()

    fun recurse(item: Item) {
        if (item.pk == null && item.identifiers.count() == 1) {
            result.add(item)
        }

        item.rels.forEach { recurse(it.obj) }
    }

    recurse(itemTree.root)

    return result
}


fun hasAmbiguousIdentifiers(itemTree: ItemTree): Boolean {
    fun recurse(item: Item): Boolean {
        if (item.identifiers.count() > 1) {
            return true
        }
        return item.rels.any {
            recurse(it.obj)
        }
    }

    return recurse(itemTree.root)
}


/**
 * Recursively remove all Items that are ambiguous, including the root node.
 */
fun removeAmbiguousItems(item: Item): Item? =
    if (item.isAmbiguous()) {
        logger.debug("Removed ambiguous ${item.identifiers}")
        null
    } else {
        item.withRelationships(item.rels.mapNotNull {
            val objItem = removeAmbiguousItems(it.obj)
            if (objItem != null) {
                it.withItem(objItem)
            } else null
        })
    }
