package org.crossref.manifold.identifiers.parsers

import org.crossref.manifold.identifiers.IdentifierParseInput
import org.crossref.manifold.identifiers.IdentifierType
import org.crossref.manifold.identifiers.parsers.Util.HTTPS_SCHEME
import org.crossref.manifold.itemtree.Identifier
import java.net.URI
import java.net.URLEncoder
import java.nio.charset.StandardCharsets

/**
 * Last resort for representing an identifier.
 * It's rare that a string won't parse as a URI, but if it won't wrap it up in the path.
 * Bundle up in a special URI format to allow us to store it.
 */
object Unknown : IdentifierTypeParser {
    private const val CANONICAL_HOST = "id.crossref.org"

    override fun tryParse(input: IdentifierParseInput): Identifier? {

        val escapedURL = URLEncoder.encode(input.raw, StandardCharsets.UTF_8).replace("\\+", "%20")

        return Identifier(URI(HTTPS_SCHEME, null, CANONICAL_HOST, -1, "/unknown/${escapedURL}", null, null), IdentifierType.UNKNOWN)
    }


}
