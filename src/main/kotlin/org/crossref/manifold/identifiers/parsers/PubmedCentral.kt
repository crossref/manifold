package org.crossref.manifold.identifiers.parsers

import org.crossref.manifold.identifiers.IdentifierParseInput
import org.crossref.manifold.identifiers.IdentifierType
import org.crossref.manifold.itemtree.Identifier
import java.net.URI

object PubmedCentral : IdentifierTypeParser {
    const val CANONICAL_HOST = "europepmc.org"

    private val PATH_LOWER_RE = """^/articles/pmc(\d+)/?$""".toRegex()

    override fun tryParse(input: IdentifierParseInput): Identifier? {
        return if (input.hostIs(CANONICAL_HOST)) {
            getInternalIdFromPath(input)?.let {
                construct(it)
            }
        } else null
    }

    /**
     * Extract the Wikidata entity, or null.
     */
    private fun getInternalIdFromPath(input: IdentifierParseInput): String? {
        return PATH_LOWER_RE.find(input.lowercasePath())?.groups?.get(1)?.value

    }

    fun construct(pubMedCentralId: String): Identifier =
        Identifier(URI(Util.HTTPS_SCHEME, CANONICAL_HOST, "/articles/PMC$pubMedCentralId", null), IdentifierType.PUB_MED_CENTRAL)

}