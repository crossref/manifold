package org.crossref.manifold.identifiers.parsers

import org.crossref.manifold.identifiers.IdentifierParseInput
import org.crossref.manifold.identifiers.IdentifierType
import org.crossref.manifold.itemtree.Identifier
import java.net.URI

/**
 * An ARK identifier. Normalizes to Compact ARK format.
 * Doesn't attempt to implement the whole specification. Just enough to recognise and store.
 */
object Ark : IdentifierTypeParser {

    private val ARK_RE = """ark:(.*)$""".toRegex()

    private const val CANONICAL_SCHEME = "ark"
    override fun tryParse(input: IdentifierParseInput): Identifier? =
        ARK_RE.find(input.raw)?.groups?.get(1)?.let {
            // Hyphens are not significant and should be removed.
            val ark = it.value.replace("-", "")


            Identifier(URI(Util.percentEncode((CANONICAL_SCHEME + ":" + ark))), IdentifierType.ARK)
        }
}
