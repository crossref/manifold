package org.crossref.manifold.identifiers.parsers

import org.crossref.manifold.identifiers.IdentifierParseInput
import org.crossref.manifold.identifiers.IdentifierType
import org.crossref.manifold.identifiers.parsers.Util.HTTPS_SCHEME
import org.crossref.manifold.itemtree.Identifier
import java.net.URI


object Ror : IdentifierTypeParser {
    const val CANONICAL_HOST = "ror.org"

    override fun tryParse(input: IdentifierParseInput): Identifier? = try {
        val cleanedInput = input.lowercasePathNoSlash()
        if (input.hostIs(CANONICAL_HOST) &&
            validateCheckDigit(cleanedInput)
        ) {
            Identifier(URI(HTTPS_SCHEME, CANONICAL_HOST, cleanedInput, null, null), IdentifierType.ROR)
        } else null
    } catch (exception: Exception) {
        null
    }

    // Second group is checksum.
    private val ROR_PATH_RE = """^/(0[a-hj-km-np-tv-z|0-9]{6})([0-9]{2})$""".toRegex()

    /**
     * Lookup table of character to value. Per Douglas Crockford Base32 implementation.
     * https://www.crockford.com/base32.html
     */
    private val ENCODING =
        "0123456789abcdefghjkmnpqrstvwxyz".mapIndexed { value, character -> Pair(character, value.toLong()) }.toMap()

    /**
     * A ROR id is a Base32 encoded Long. Parse that long for checksum validation.
     */
    private fun parseValue(path: String): Long = path.fold(0L) { x, c ->
        x * 32L + ENCODING.getOrDefault(c, 0)
    }

    private fun validateCheckDigit(path: String) =
        ROR_PATH_RE.find(path)?.groupValues?.let { groups ->
            // The regex will guard this being an unparseable integer.
            val checkDigit = groups.get(2).toLong()
            val numbers = groups[1]
            val parsed = parseValue(numbers)

            val expectedCheckDigit = 98 - ((parsed * 100) % 97)
            checkDigit == expectedCheckDigit
        } ?: false

}
