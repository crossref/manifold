package org.crossref.manifold.identifiers.parsers

import org.crossref.manifold.identifiers.IdentifierParseInput
import org.crossref.manifold.identifiers.IdentifierType
import org.crossref.manifold.itemtree.Identifier
import java.net.URI

object CrossrefAccession : IdentifierTypeParser {

    const val CANONICAL_HOST = "id.crossref.org"
    private val PATH_REGEX = """/accession/.*""".toRegex()

    override fun tryParse(input: IdentifierParseInput): Identifier? {
        return if (input.hostIs(CANONICAL_HOST) &&
            PATH_REGEX.matches(input.lowercasePath())
        ) {
            Identifier(URI(Util.HTTPS_SCHEME, CANONICAL_HOST, input.uri?.path, null), IdentifierType.CROSSREF_ACCESSION)
        } else null
    }
}