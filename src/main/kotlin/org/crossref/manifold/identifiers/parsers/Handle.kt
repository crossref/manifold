package org.crossref.manifold.identifiers.parsers

import org.crossref.manifold.identifiers.IdentifierParseInput
import org.crossref.manifold.identifiers.IdentifierType
import org.crossref.manifold.identifiers.parsers.Util.HTTPS_SCHEME
import org.crossref.manifold.itemtree.Identifier
import java.net.URI

object Handle : IdentifierTypeParser {

    private const val CANONICAL_HOST = "hdl.handle.net"

    // There are basically no constraints on a DOI except that it has a prefix and suffix separated by '/'.
    private val PATH_MATCH = """/.*/.*""".toRegex()

    override fun tryParse(input: IdentifierParseInput): Identifier? =
        if (input.hostIs(CANONICAL_HOST) &&
            PATH_MATCH.matches(input.uri?.path ?: "")
        ) {
            Identifier(URI(HTTPS_SCHEME, CANONICAL_HOST, input.uri?.path, null, null), IdentifierType.HANDLE)
        } else null
}
