package org.crossref.manifold.identifiers.parsers

import org.crossref.manifold.identifiers.IdentifierParseInput
import org.crossref.manifold.identifiers.IdentifierType
import org.crossref.manifold.identifiers.parsers.Util.HTTPS_SCHEME
import org.crossref.manifold.itemtree.Identifier
import java.net.URI

/**
 * Crossref Owner Prefix. Not the same as a DOI prefix.
 */
object CrossrefPrefix : IdentifierTypeParser {
    private const val CANONICAL_HOST = "id.crossref.org"

    private val PREFIX_REGEX = """10.\d*$""".toRegex()

    override fun tryParse(input: IdentifierParseInput): Identifier? =
        if (input.lowercaseUri != null) {
            if (input.hostIs(CANONICAL_HOST) && input.lowercasePathStartsWith("/prefix/")) {
                Identifier(URI(HTTPS_SCHEME, CANONICAL_HOST, input.lowercasePathNoSlash(), null), IdentifierType.CROSSREF_PREFIX)
            } else null
        } else null


    /**
     * Get the DOI prefix. e.g. "10.5555"
     */
    fun getDoiPrefix(identifier: Identifier): String? =
        PREFIX_REGEX.find(identifier.uri.path)?.value

}
