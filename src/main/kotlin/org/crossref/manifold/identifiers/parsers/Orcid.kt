package org.crossref.manifold.identifiers.parsers

import org.crossref.manifold.identifiers.IdentifierParseInput
import org.crossref.manifold.identifiers.IdentifierType
import org.crossref.manifold.identifiers.parsers.Util.HTTPS_SCHEME
import org.crossref.manifold.identifiers.parsers.Util.generateCheckDigitIso27729
import org.crossref.manifold.itemtree.Identifier
import java.net.URI


object Orcid : IdentifierTypeParser {
    private const val CANONICAL_HOST = "orcid.org"

    override fun tryParse(input: IdentifierParseInput): Identifier? =
        try {
            val cleanedInput = input.uppercasePathNoSlash()
            if (input.hostIs(CANONICAL_HOST) &&
                validateCheckDigit(cleanedInput)
            ) {
                Identifier(URI(HTTPS_SCHEME, CANONICAL_HOST, cleanedInput, null, null), IdentifierType.ORCID)
            } else null
        } catch (exception: Exception) {
            null
        }


    /**
     * Find digits from a path. Last digit is checksum, broken out separately.
     */
    private val PATH_NUMBERS_RE = """^/(\d{4})-(\d{4})-(\d{4})-(\d{3})([\dX])$""".toRegex()

    private fun validateCheckDigit(path: String) : Boolean =
        PATH_NUMBERS_RE.find(path)?.groupValues?.let { groups ->
            val checkDigit = groups.get(5).uppercase()
            val numbers = groups[1] + groups[2] + groups[3] + groups[4]
            val expectedCheckDigit = generateCheckDigitIso27729(numbers)

            checkDigit == expectedCheckDigit
        } ?: false
}