package org.crossref.manifold.identifiers

import org.crossref.manifold.identifiers.parsers.*
import org.crossref.manifold.itemtree.Identifier
import java.net.URI

/**
 * The [org.crossref.manifold.identifiers] package normalizes identifier strings into [Identifier] objects. We treat all
 * identifiers as URIs, not as polymorphic types. However, each identifier has its own rules for normalization.
 *
 * Behaviour is modularized into one Object per identifier type. Because there are interdependencies between identifiers
 * (for example DOI is a subset of Handle is a subset of URI), they should not be called in isolation. This object is
 * responsible for calling them in the right sequence.
 *
 * This also means that testing of each identifier type should be done via this object. It will verify not only the
 * behaviour of that type, but any interdependencies.
 */
object IdentifierParser {
    private val orderedParsers = listOf(
        // DOI first, as they are most important. There's some extra-tolerant parsing for badly encoded URI paths.
        Doi,

        // These can run in any order.
        CrossrefOrg,
        CrossrefPrefix,
        Orcid,
        CrossrefContainer,
        CrossrefAccession,

        Grid,
        Isni,
        Ror,

        Purl,
        Handle,

        Isbn,
        Issn,

        Wikidata,

        Arxiv,

        PubmedCentral,
        PubMed,
        Ecli,

        // Ark is more permissive, as it does a text match. Don't let it match identifiers that would have been
        // picked up by other parsers.
        Ark,

        // This is the penultimate fallback. Will always accept a valid URI, or something that can be parsed as URL and
        // represented as a URI.
        Uri,

        // Final fallback. Nothing worked, stick this in special string.
        // We know that this always succeeds.
        Unknown
    )

    /**
     * Construct an Identifier of the appropriate class.
     * Used when reconstructing data from the database, so relies on a correct integer being returned.
     * If the value is unrecognised, return null.
     *
     * We should never get an unrecognised identifier type from the database. But if we do, fall back to plain Uri type.
     */
    fun fromUri(uri: URI, typeId: Int): Identifier =
        Identifier(uri, IdentifierType.fromId(typeId))

    /**
     * Parse an input string, which is probably a URI of some kind, into an Identifier.
     * Try various strategies to recognise and normalize identifier types.
     * The Unknown type must always succeed, so this will always return.
     *
     */
    fun parse(input: String): Identifier {
        val parsedInput = IdentifierParseInput.build(input)
        val (normalizedUri, type) = orderedParsers.firstNotNullOf {
             it.tryParse(parsedInput)
        }

        return Identifier(normalizedUri, type)
    }
}