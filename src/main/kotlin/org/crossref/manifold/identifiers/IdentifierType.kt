package org.crossref.manifold.identifiers

enum class IdentifierType(val id: Int) {
    ARK(1),
    ARXIV(2),
    CROSSREF_ACCESSION(3),
    CROSSREF_CONTAINER(5),
    CROSSREF_ORG(6),
    CROSSREF_PREFIX(7),
    DOI(8),
    ECLI(9),
    GRID(10),
    HANDLE(11),
    ISBN(12),
    ISNI(13),
    ISSN(14),
    ORCID(15),
    PUB_MED(16),
    PUB_MED_CENTRAL(17),
    PURL(18),
    ROR(19),
    UNKNOWN(20),
    URI(21),
    WIKIDATA(22);

    companion object {
        /**
         * Build type from the integer ID.
         * This is used for reconstructing from the database, so it's reasonable to use URI as the fallback.
         * The UNKNOWN type is not suitable, as it wraps an arbitrary string in a wrapper. Constructing another URI from
         * would produce confusing results.
         */
        fun fromId(id: Int) = IdentifierType.values().firstOrNull { it.id == id } ?: URI
    }

    /**
     * Name for showing in API responses.
     */
    fun publicName(): String =
        when (this) {
            ARK -> "ark"
            ARXIV -> "arxiv"
            CROSSREF_ACCESSION -> "accession"
            CROSSREF_CONTAINER -> "container"
            CROSSREF_ORG -> "org"
            CROSSREF_PREFIX -> "prefix"
            DOI -> "DOI"
            ECLI -> "ECLI"
            GRID -> "GRID"
            HANDLE -> "handle"
            ISBN -> "ISBN"
            ISNI -> "ISNI"
            ISSN -> "ISSN"
            ORCID -> "ORCID"
            PUB_MED -> "pub-med"
            PUB_MED_CENTRAL -> "pub-med-central"
            PURL -> "PURL"
            ROR -> "ROR"
            UNKNOWN -> "unknown"
            URI -> "URI"
            WIKIDATA -> "wikidata"
        }

}