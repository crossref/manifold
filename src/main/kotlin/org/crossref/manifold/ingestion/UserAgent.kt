package org.crossref.manifold.ingestion

data class UserAgent(
    val product: String,
    val version: String? = null,
    val pk: Int? = null
) {

    fun withPK(pk: Int) = this.copy(pk = pk)

    fun value() = if (version != null) {
        "$product/$version"
    } else {
        product
    }
}

object UserAgentParser {
    /**
     * Parse the user agent string and return a [UserAgent] object.
     * For strings with multiple identifiers only a single UserAgent will be returned, matching the first identifier.
     */
    fun parse(value: String): UserAgent {

        //
        // Based on https://www.rfc-editor.org/rfc/rfc9110#section-10.1.5.
        //
        // A user agent string may consist of many identifiers in the form of
        // product/version pairs separated by whitespace.
        //
        // By convention the identifiers are ordered "in decreasing order of their significance
        // for identifying the user agent software".
        //
        // Therefore, to keep this implementation simple, we will keep only the first identifier.
        val parts = value.split('/', limit = 2)

        val product = parts[0].trim()

        // We need to have a meaningful product value
        check(product.isNotBlank()) { "User agent cannot be blank" }

        // For the version discard anything past the first whitespace if there is one
        val version = if (parts.size == 2) {
            parts[1].trim().substringBefore(' ').ifBlank { "unknown" }
        } else {
            "unknown"
        }

        return UserAgent(product, version)
    }
}