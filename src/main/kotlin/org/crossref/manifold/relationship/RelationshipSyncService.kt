package org.crossref.manifold.relationship

import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Service

@Service
class RelationshipSyncService(
    private val relationshipSyncDao: RelationshipSyncDao
) {
    companion object {
        const val SYNC_RELATIONSHIPS_CRON = "sync-relationships-cron"
    }

    private val logger: Logger = LoggerFactory.getLogger(this::class.java)

    @Scheduled(cron = "\${$SYNC_RELATIONSHIPS_CRON:-}")
    fun syncRelationships() {
        logger.info("Relationship sync starting")
        relationshipSyncDao.prepare()

        logger.info("Relationship sync started")
        relationshipSyncDao.syncRelationships()

        logger.info("Relationship sync finishing")
        relationshipSyncDao.cleanup()

        logger.info("Relationship sync finished")
    }
}
