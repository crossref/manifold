package org.crossref.manifold.relationship

import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Value
import org.springframework.jdbc.core.JdbcTemplate
import org.springframework.stereotype.Repository
import org.springframework.transaction.PlatformTransactionManager
import org.springframework.transaction.TransactionDefinition
import org.springframework.transaction.support.TransactionTemplate

@Repository
class RelationshipSyncDao(
    private val jdbcTemplate: JdbcTemplate,
    txManager: PlatformTransactionManager,
    @Value("\${$REL_SYNC_BATCH_SIZE:10000}") private val relSyncBatchSize: Int
) {
    companion object {
        private const val REL_SYNC_BATCH_SIZE = "rel-sync-batch-size"
    }

    private val logger: Logger = LoggerFactory.getLogger(this::class.java)
    private val txTemplate = TransactionTemplate(txManager).apply {
        propagationBehavior = TransactionDefinition.PROPAGATION_REQUIRES_NEW
    }

    fun newRelationshipSync(): Long = jdbcTemplate.queryForObject(
        "INSERT INTO relationship_sync (ready_to_sync) VALUES (FALSE) RETURNING sync_id",
        Long::class.java
    )!!

    fun relationshipSyncReady(syncId: Long) {
        jdbcTemplate.update("UPDATE relationship_sync SET ready_to_sync = TRUE WHERE sync_id = ?", syncId)
    }

    fun syncRelationships() {
        do {
            txTemplate.executeWithoutResult {
                jdbcTemplate.update(
                    """
                    CREATE TEMPORARY TABLE sync_ids ON COMMIT DROP AS
                        SELECT sync_id 
                        FROM relationship_sync 
                        WHERE ready_to_sync
                        ORDER BY sync_id
                        LIMIT $relSyncBatchSize
                    """.trimIndent()
                )

                jdbcTemplate.update("CREATE INDEX ON sync_ids (sync_id)")

                jdbcTemplate.update("ANALYZE sync_ids")

                val deleted = jdbcTemplate.update(
                    """
                    DELETE FROM relationship_api rapi
                    USING relationship_history rmh
                    JOIN sync_ids USING (sync_id)
                    WHERE rapi.pk = rmh.pk
                    """.trimIndent()
                )
                logger.info("Deleted $deleted relationships")

                val updated = jdbcTemplate.queryForObject(
                    """
                    WITH updated_subject_item_infos AS (
                        UPDATE relationship_api
                        SET subject_type_pk = type_pk,
                            subject_subtype_pk = subtype_pk,
                            subject_steward_pk = steward_pk
                        FROM item_info
                        JOIN sync_ids USING (sync_id)
                        WHERE subject_item_pk = root_item_pk
                        RETURNING pk
                    ), updated_object_item_infos AS (
                        UPDATE relationship_api
                        SET object_type_pk = type_pk,
                            object_subtype_pk = subtype_pk,
                            object_steward_pk = steward_pk
                        FROM item_info
                        JOIN sync_ids USING (sync_id)
                        WHERE object_item_pk = root_item_pk
                        RETURNING pk
                    ), updated_subject_ras AS (
                        UPDATE relationship_api rapi
                        SET subject_ra_pk = registration_agency_pk
                        FROM item_identifier ii
                        JOIN identifier_ra_lookup iral
                        ON ii.pk = iral.identifier_pk
                        JOIN sync_ids USING (sync_id)
                        JOIN ra_lookup ral
                        ON iral.ra_lookup_pk = ral.pk
                        WHERE subject_item_pk = item_pk
                        RETURNING rapi.pk
                    ), updated_object_ras AS (
                        UPDATE relationship_api rapi
                        SET object_ra_pk = registration_agency_pk
                        FROM item_identifier ii
                        JOIN identifier_ra_lookup iral
                        ON ii.pk = iral.identifier_pk
                        JOIN sync_ids USING (sync_id)
                        JOIN ra_lookup ral
                        ON iral.ra_lookup_pk = ral.pk
                        WHERE object_item_pk = item_pk
                        RETURNING rapi.pk
                    )
                    SELECT COUNT(*) FROM (
                        SELECT pk
                        FROM updated_subject_item_infos
                        UNION
                        SELECT pk
                        FROM updated_object_item_infos
                        UNION
                        SELECT pk
                        FROM updated_subject_ras
                        UNION
                        SELECT pk
                        FROM updated_object_ras
                    ) updated_count
                    """.trimIndent(), Int::class.java
                )!!
                logger.info("Updated $updated relationships")

                val created = jdbcTemplate.update(
                    """
                    INSERT INTO relationship_api (
                        pk,
                        asserted_at,
                        item_tree_asserting_party_pk,
                        matching_party_pk,
                        subject_item_pk,
                        subject_type_pk,
                        subject_subtype_pk,
                        subject_steward_pk,
                        subject_ra_pk,
                        relationship_type_pk,
                        object_item_pk,
                        object_type_pk,
                        object_subtype_pk,
                        object_steward_pk,
                        object_ra_pk
                    )
                    SELECT
                        rmc.pk,
                        asserted_at,
                        item_tree_asserting_party_pk,
                        matching_party_pk,
                        subject_item_pk,
                        sitin.type_pk,
                        sitin.subtype_pk,
                        sitin.steward_pk,
                        sral.registration_agency_pk,
                        relationship_type_pk,
                        object_item_pk,
                        oitin.type_pk,
                        oitin.subtype_pk,
                        oitin.steward_pk,
                        oral.registration_agency_pk
                    FROM relationship_current rmc
                    JOIN sync_ids USING (sync_id)
                    LEFT JOIN item_info sitin
                        ON rmc.subject_item_pk = sitin.root_item_pk
                    LEFT JOIN item_info oitin
                        ON rmc.object_item_pk = oitin.root_item_pk
                    LEFT JOIN item_identifier sii
                        ON rmc.subject_item_pk = sii.item_pk
                    LEFT JOIN identifier_ra_lookup siral
                        ON sii.pk = siral.identifier_pk
                    LEFT JOIN ra_lookup sral
                        ON siral.ra_lookup_pk = sral.pk
                    LEFT JOIN item_identifier oii
                        ON rmc.object_item_pk = oii.item_pk
                    LEFT JOIN identifier_ra_lookup oiral
                        ON oii.pk = oiral.identifier_pk
                    LEFT JOIN ra_lookup oral
                        ON oiral.ra_lookup_pk = oral.pk
                    """.trimIndent()
                )
                logger.info("Created $created relationships")

                jdbcTemplate.update("DELETE FROM relationship_sync rs USING sync_ids si WHERE rs.sync_id = si.sync_id")
            }
            val remainingSyncIdCount = jdbcTemplate.queryForObject(
                "SELECT COUNT(sync_id) FROM relationship_sync WHERE ready_to_sync",
                Int::class.java
            )!!
            logger.info("$remainingSyncIdCount remaining sync operations")
            if (remainingSyncIdCount >= relSyncBatchSize) {
                logger.info("Relationship sync rerunning")
            }
        } while (remainingSyncIdCount >= relSyncBatchSize)
    }

    fun prepare() {
        jdbcTemplate.update("VACUUM ANALYZE relationship_current")
        jdbcTemplate.update("VACUUM ANALYZE relationship_history")
        jdbcTemplate.update("VACUUM ANALYZE item_info")
        jdbcTemplate.update("VACUUM ANALYZE relationship_sync")
    }

    fun cleanup() {
        jdbcTemplate.update("VACUUM ANALYZE relationship_api")
    }
}
