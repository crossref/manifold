package org.crossref.manifold.modules.citeprocjson

import org.crossref.cayenne
import org.crossref.manifold.identifiers.IdentifierParser
import org.crossref.manifold.itemgraph.Resolver
import org.crossref.manifold.itemtree.ItemTreeService
import org.crossref.manifold.rendering.Configuration.RENDERING
import org.crossref.manifold.rendering.Configuration.WORKS_API
import org.crossref.manifold.rendering.ContentType
import org.crossref.manifold.rendering.ItemTreeRenderer
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.server.ResponseStatusException

/**
 * REST APIs for the Citeproc-JSON 'works' view.
 */
@RestController
@ConditionalOnProperty(prefix = RENDERING, name = [WORKS_API])
class WorksController(
    val itemTreeService: ItemTreeService,
    val itemResolver: Resolver,
    val itemTreeRenderer: ItemTreeRenderer,
) {
    init {
        // Configure Cayenne's identifier types.
        // Required because the identifier setup is part of Cayenne's dynamic 'core' configuration system.
        cayenne.boot()
    }

    @GetMapping("/beta/works/{*identifier}", produces = [MediaType.APPLICATION_JSON_VALUE])
    fun getWork(@PathVariable identifier: String): String = work(identifier)

    /**
     * Retrieve the Citeproc-json render of the Item Tree for the given Identifier.
     */
    private fun work(identifierStr: String): String {
        // Need to trim the leading slash.
        val identifier = itemResolver.resolveRO(IdentifierParser.parse(identifierStr.substring(1)))
        if (identifier.pk == null) {
            throw ResponseStatusException(HttpStatus.NOT_FOUND, "Couldn't find that item.")
        }

        val itemTree = itemTreeService.getCanonicalItemTree(identifier.pk)
            ?: throw ResponseStatusException(HttpStatus.NOT_FOUND, "Couldn't retrieve data for that item.")

        val renderedItemTree = itemTreeRenderer.getRenderedItemTree(itemTree.second, ContentType.CITEPROC_JSON)
            ?: throw ResponseStatusException(HttpStatus.NOT_FOUND, "Couldn't find that item.")

        return renderedItemTree.content
            ?: throw ResponseStatusException(HttpStatus.NOT_FOUND, "Couldn't find that item.")
    }
}
