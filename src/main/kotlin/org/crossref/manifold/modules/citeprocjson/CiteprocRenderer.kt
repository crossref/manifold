package org.crossref.manifold.modules.citeprocjson

import clojure.lang.Keyword
import clojure.lang.RT
import org.crossref.cayenne
import org.crossref.manifold.identifiers.IdentifierType
import org.crossref.manifold.identifiers.parsers.CrossrefOrg
import org.crossref.manifold.itemtree.Item
import org.crossref.manifold.itemtree.ItemTree
import org.crossref.manifold.modules.consts.RelationshipTypes
import org.crossref.manifold.rendering.ContentType
import org.crossref.manifold.rendering.ContentTypeRenderer
import org.crossref.manifold.rendering.RenderingContext
import org.crossref.manifold.util.clojure.toClj
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Service

@Service
class CiteprocRenderer() : ContentTypeRenderer {
    private val logger: Logger = LoggerFactory.getLogger(this::class.java)

    init {
        cayenne.boot()
    }

    override fun internalContentType(): ContentType = ContentType.CITEPROC_JSON
    override fun publicContentType(): ContentType = ContentType.APPLICATION_JSON

    override fun offer(type: String?, subtype: String?, itemTree: ItemTree?, context: RenderingContext?): Boolean = type == "work"

    /**
     * Internally, manifold has the concept of an organization but typically things
     * that we consider as such are represented as members, before we can call out to
     * cayenne we need to turn organization identifiers into member identifiers
     */
    fun organizationIdToMemberId(item: Item): Item {
        val newItem = item
            .withIdentifiers(
                item.identifiers.mapNotNull { identifier ->
                    if (identifier.type == IdentifierType.CROSSREF_ORG) {
                        CrossrefOrg.toLegacyMemberId(identifier)
                    } else {
                        identifier
                    }
                },
            )
            .withRelationships(
                item.rels.map { relationship ->
                    relationship.withItem(organizationIdToMemberId(relationship.obj))
                },
            )
        return newItem
    }


    override fun render(itemTree: ItemTree, context: RenderingContext?): String {
        logger.debug("Rendering ${itemTree.root.pk} using ${internalContentType()}")

        // Turn organization ids back into member ids for cayenne
        val patchedItem = organizationIdToMemberId(itemTree.root)

        val fieldsToMakeValuesKeywords = setOf("type", "subtype", "kind")

        val converted = patchedItem.toClj(fieldsToMakeValuesKeywords)
        val esDoc = cayenne.itemToEsDoc(converted)
        val cleanedEsDoc = RT.dissoc(esDoc, Keyword.intern("indexed"))

        val steward = patchedItem.rels.firstOrNull { it.relTyp == RelationshipTypes.STEWARD }?.obj

        var citeproc = cayenne.esDocToCiteproc(cleanedEsDoc)
            ?: throw Exception("Couldn't find that item.")

        if (steward != null) {
            val property = steward.properties.firstOrNull()
            val name = property?.values?.get("name")?.textValue()
            citeproc = RT.assoc(citeproc, "publisher", name)
        }

        return cayenne.writeJsonString(citeproc)
    }
}
