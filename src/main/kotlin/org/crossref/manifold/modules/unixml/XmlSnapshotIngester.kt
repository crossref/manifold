package org.crossref.manifold.modules.unixml

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.cancel
import org.crossref.manifold.itemgraph.ItemGraph
import org.crossref.manifold.modules.unixml.support.insertFromTarGz
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import java.io.File

/**
 * If the user supplies UNIXML_INGEST_XML_SNAPSHOT with a .tgz file path in the environment, load that file into the database.
 */
object XmlSnapshotIngester {
    private var logger: Logger = LoggerFactory.getLogger(this::class.java)
    private var scope = CoroutineScope(Dispatchers.IO)

    fun ingestXmlSnapshot(
        itemGraph: ItemGraph,
        snapshotFile: File
    ) {
        try {
            logger.info("XML ingestion starting in background...")
            logger.info("Ingest snapshot handler file: $snapshotFile.name")
            insertFromTarGz(
                snapshotFile, itemGraph
            )
            logger.info("XML ingestion continuing in background!")
        } catch (e: Exception) {
            logger.warn("Error! $e")
            e.printStackTrace()
            throw (e)
        }
    }

    fun destroy() {
        scope.cancel()
    }
}
