package org.crossref.manifold.modules.unixml

import org.crossref.manifold.modules.unixml.Module.Companion.UNIXML
import org.crossref.messaging.aws.autoconfig.S3AutoConfig.Companion.S3_NOTIFICATION_QUEUE
import org.crossref.messaging.aws.s3.S3EventNotification
import org.crossref.messaging.aws.sqs.SqsListener
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import software.amazon.awssdk.services.s3.S3Client
import software.amazon.awssdk.services.s3.model.GetObjectRequest

class XmlS3Ingester(
    private val s3Client: S3Client,
    private val ingester: XmlSingleIngester
) {
    private val logger: Logger = LoggerFactory.getLogger(this::class.java)

    /**
     * Process XML files retrieved from S3.
     * The method will attempt to fetch the file, transform it and then ingest it as an EnvelopeBatch.
     */
    @SqsListener("$UNIXML.$S3_NOTIFICATION_QUEUE")
    fun onMessageReceived(s3EventNotification: S3EventNotification) {
        val wantedFiles = s3EventNotification.records.filter { it.objectKey.endsWith(".xml") }
        val keys = wantedFiles.map { it.objectKey }
        logger.info("Reading XML files: ${keys.joinToString(", ")} ...")
        val s3Files = wantedFiles.map { record ->
            s3Client.getObject(
                    GetObjectRequest.builder()
                            .bucket(record.bucketName)
                            .key(record.objectKey)
                            .build()
            ).use {
                Pair(record.objectKey, it.buffered().reader().readText())
            }
        }
        ingester.ingestMany(s3Files)
    }
}
