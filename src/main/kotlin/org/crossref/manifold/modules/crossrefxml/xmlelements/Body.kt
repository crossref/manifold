package org.crossref.manifold.modules.crossrefxml.xmlelements

data class Body (
    val query : Query
)
