package org.crossref.manifold.modules.crossrefxml.elementRenderers

import org.crossref.manifold.itemtree.Item
import org.crossref.manifold.itemtree.ItemTree
import org.crossref.manifold.modules.crossrefxml.ItemTreeConverter
import org.crossref.manifold.modules.crossrefxml.JournalElement
import org.crossref.manifold.modules.crossrefxml.xmlelements.*


/**
 * Journal and Journal Article rendering.
 */
class JournalRenderer {
    companion object : ItemTreeConverter {
        override fun toXmlElement(itemTree: ItemTree): JournalElement {
            val journal = journalArticle(itemTree.root)
            return JournalElement(value = journal, doi = journal.journalArticle?.doiData?.doi)
        }

        /**
         * Render a Journal tag, which includes a JournalArticle.
         */
        private fun journalArticle(item: Item) = Journal(
            journalMetadata = getJournalMetadata(item),
            journalIssue = getJournalIssueMetadata(item),
            journalArticle = JournalArticle(
                titles = listOf(Util.getFullTitle(item)),
                pages = Util.getPageNumbers(item),
                contributors = Util.getContributors(item),
                abstract = Util.getAbstract(item),
                publicationDate = Util.getPublicationDates(item),
                publisherItem = Util.getPublisherItems(item),
                citationList = Util.getCitations(item),
                crossmark = null,
                archiveLocations = Util.getArchiveLocations(item),
                doiData = Util.getDoiData(item),
            ),
        )

        private fun getJournalMetadata(journalArticle: Item): JournalMetadata? {
            val journal = Util.getAncestor(journalArticle, "journal")
            return if (journal != null) {
                JournalMetadata(
                    language = journal.getPropertyText("language"),
                    fullTitle = Util.getFullTitle(journal),
                    abbrevTitle = Util.getAbbreviatedTitle(journal),
                    issn = Util.getIssn(journal),
                    archiveLocations = Util.getArchiveLocations(journal),
                    doiData = Util.getDoiData(journal)
                )
            } else null

        }

        private fun getJournalIssueMetadata(journalArticle: Item): JournalIssue? {
            val issue = Util.getAncestor(journalArticle, "journal-issue")
            return if (issue != null) {
                val volumeItem = Util.getAncestor(journalArticle, "journal-volume")
                JournalIssue(
                    publicationDate = Util.getPublicationDates(issue),
                    journalVolume = volumeItem?.let { JournalVolume(volume = it.getPropertyText("volume")) },
                    issue = issue.getPropertyText("issue")
                )
            } else null
        }
    }
}