package org.crossref.manifold.modules.crossrefxml.xmlelements

import com.fasterxml.jackson.annotation.JsonPropertyOrder
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty

@JsonPropertyOrder("assertion")
data class FundrefProgram(
    @field:JacksonXmlProperty(isAttribute = true, localName = "xmlns")
    val xmlns: String = "http://www.crossref.org/fundref.xsd",

    @field:JacksonXmlProperty(isAttribute = true, localName = "xsi:schemaLocation")
    val schemaLocation: String = "http://www.crossref.org/schemas/fundref.xsd",

    @field:JacksonXmlProperty(localName = "assertion")
    @field:JacksonXmlElementWrapper(useWrapping = false)
    val assertion: List<Assertion>? = null,

    ) : Program
