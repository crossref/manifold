package org.crossref.manifold.modules.crossrefxml.xmlelements

/**
 * Marker interface to allow us to have multiple <program> tags next to each other.
 */
interface Program
