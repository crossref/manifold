package org.crossref.manifold.modules.crossrefxml.xmlelements

import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.annotation.JsonValue
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty


data class Noisbn(
    @field:JacksonXmlProperty(isAttribute = true, localName = "reason")
    @field:JsonInclude(JsonInclude.Include.NON_NULL)
    val reason: NoisbnReason?
)

enum class NoisbnReason(@get:JsonValue val value: String) {
    ARCHIVE_VOLUME("archive_volume"),
    MONOGRAPH("monograph"),
    SIMPLE_SERIES("simple_series");
}
