package org.crossref.manifold.modules.crossrefxml.xmlelements

import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.annotation.JsonPropertyOrder
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty

@JsonPropertyOrder(*["publication_date", "journal_volume", "issue"])
data class JournalIssue(
    @field:JacksonXmlProperty(localName = "publication_date")
    @field:JacksonXmlElementWrapper(useWrapping = false)
    @field:JsonInclude(JsonInclude.Include.NON_EMPTY)
    val publicationDate: List<PublicationDate>? = null,

    @field:JacksonXmlProperty(localName = "journal_volume")
    val journalVolume: JournalVolume? = null,

    @field:JsonInclude(JsonInclude.Include.NON_NULL)
    val issue: String? = null,
)
