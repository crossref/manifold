package org.crossref.manifold.modules.crossrefxml.elementRenderers

import com.fasterxml.jackson.databind.JsonNode
import org.crossref.manifold.identifiers.IdentifierParseInput
import org.crossref.manifold.identifiers.IdentifierType
import org.crossref.manifold.identifiers.parsers.Doi
import org.crossref.manifold.itemtree.Item
import org.crossref.manifold.modules.crossrefxml.xmlelements.*
import org.crossref.manifold.identifiers.parsers.Isbn as IsbnParser
import org.crossref.manifold.identifiers.parsers.Issn as IssnParser

/**
 * A generic function to convert an XML attribute string value into corresponding Enum value.
 * Works for any Enum.
 * */
inline fun <reified E : Enum<E>, V> ((E) -> V).fromXmlValue(value: V): E {
    return value.let {
        enumValues<E>().firstOrNull { this(it) == value } ?: throw NoSuchElementException("Enum has no member `$value`")
    }
}

/**
 * Important note about JsonNode objects:
 * if a JSON field is *missing*, node["field"] will return Java's `null`. But if the field exists and has the explicit
 * `null` value (node = {"field": null}), then node["field"] will return NullNode, and `result == null` will be *false*
 * For this reason we should use `result.isNull` for these checks instead.
 * */
object Util {
    private val mediaTypes = listOf("online", "print")

    fun getAncestor(item: Item, subtype: String): Item? {
        val ancestor = item.rels.filter { it.relTyp == "ancestor" }
        return ancestor.map { it.obj }.firstOrNull { it.getPropertyText("subtype") == subtype }
    }

    fun getFullTitle(item: Item?) = item?.getPropertyArrayFirstValue("title-long")

    fun getSubtitle(item: Item?) = item?.getPropertyArrayFirstValue("title-secondary")

    fun getOriginalTitle(item: Item?) = item?.getPropertyArrayFirstValue("title-original")

    fun getAbbreviatedTitle(item: Item?) = item?.getPropertyArrayFirstValue("title-short")

    /**
     * Collect 'date' objects from an Item and convert them to typed PublicationDates.
     */
    fun getPublicationDates(item: Item): List<PublicationDate> {
        val dates = mutableListOf<PublicationDate>()
        for (mediaType in mediaTypes) {
            item.getProperty("published-${mediaType}")?.let { jsonDate ->
                toYMD(jsonDate)?.let { (year, month, day) ->
                    dates.add(PublicationDate(mediaType, year, month, day))
                }
            }
        }
        return dates.ifEmpty {
            item.getProperty("published")?.let { jsonDate ->
                toYMD(jsonDate)?.let { (year, month, day) ->
                    dates.add(PublicationDate("print", year, month, day))
                }
            }
            dates
        }
    }

    fun getAcceptanceDate(item: Item): AcceptanceDate? {
        val jsonDate = item.getProperty("accepted")
        return if (jsonDate != null) {
            toYMD(jsonDate)?.let { (year, month, day) ->
                AcceptanceDate("print", year, month, day)
            }
        } else null
    }

    /**
     * Read a date node to year, month day strings, formatted with leading zeroes.
     */
    private fun toYMD(node: JsonNode): Triple<String?, String?, String?>? = node["date-parts"]?.let { dateNode ->
        val year = dateNode[0]?.intValue()?.let { "%04d".format(it) }
        val month = dateNode[1]?.intValue()?.let { "%02d".format(it) }
        val day = dateNode[2]?.intValue()?.let { "%02d".format(it) }

        Triple(year, month, day)
    }

    fun getDoiData(item: Item): DoiData? {
        val resourceResolution = item.rels.filter { it.relTyp == "resource-resolution" }.map { it.obj }

        val primaryResourceUrl =
            resourceResolution.firstOrNull { it.getPropertyText("subtype") == "primary" }?.childrenWithRel("object")
                ?.firstOrNull()?.identifiers?.firstOrNull()?.uri?.toString()

        // Present in "10.5555/12345678" format.
        val itemDoi = item.identifiersOfType(IdentifierType.DOI).firstOrNull()?.let { Doi.withoutResolver(it) }

        return if (primaryResourceUrl != null && itemDoi != null) {
            DoiData(itemDoi, primaryResourceUrl)
        } else null
    }

    fun getComponentNumber(item: Item) = item.getPropertyText("component-number")

    fun getContributors(item: Item): Contributors {
        val authors = item.childrenWithRel("author").map { author ->

            // Author's ORCID is located in different parts of ItemTree depending on provenance
            // See https://crossref.atlassian.net/browse/CR-1853

            var orcid = author.childrenWithRel("object").map { obj ->
                obj.identifiers.filter { it.type == IdentifierType.ORCID }.map {
                    it.uri.toString()
                }.firstOrNull()
            }.firstOrNull()

            if (orcid.isNullOrEmpty()) {
                orcid = author.identifiers.filter { it.type == IdentifierType.ORCID }.map {
                    it.uri.toString()
                }.firstOrNull()
            }

            val affiliation = author.childrenWithRel("affiliation").firstNotNullOfOrNull {
                it.getPropertyText("name")
            }

            // Role is derived from the relationship (see `cayenne.item-tree/contributor-rels` in Cayenne).
            PersonName(
                sequence = author.getPropertyText("sequence"),
                contributorRole = "author",
                givenName = author.getPropertyText("first-name"),
                surname = author.getPropertyText("last-name"),
                suffix = author.getPropertyText("suffix"),
                affiliation = affiliation,
                orcid = orcid,
            )
        }

        val organizations = item.childrenWithRel("chair")
            .filter {
                it.getPropertyText("type") == "contributor" && it.getPropertyText("subtype") == "org"
            }.map {
                Organization(
                    sequence = it.getPropertyText("sequence"),
                    contributorRole = "chair",
                    value = it.getPropertyText("name")
                )
            }

        return Contributors(authors, organizations)
    }

    fun getAbstract(item: Item): List<Abstract>? = item.getPropertyArray("abstract")?.map {
        Abstract(value = it["value"]?.textValue())
    }

    /**
     * Schema requires either an <isbn> with the correct value, or <noisbn> with "reason"
     */
    fun getIsbn(item: Item): Pair<List<Isbn>?, Noisbn?> {
        val isbnList = item.rels.filter { rel -> rel.relTyp == "isbn" }.map { rel ->
            Isbn(
                value = rel.obj.getPropertyText("original"),
                mediaType = rel.obj.getPropertyText("kind")?.let {
                    IsbnMediaType::value.fromXmlValue(it)
                }
            )
        }
        return if (isbnList.isEmpty()) {
            return Pair(null, Noisbn(NoisbnReason.MONOGRAPH))
        } else {
            Pair(isbnList, null)
        }
    }

    fun getIssn(item: Item): List<Issn> {
        return item.rels.filter { rel -> rel.relTyp == "issn" }.map { rel ->
            Issn(
                value = rel.obj.getPropertyText("original"),
                mediaType = rel.obj.getPropertyText("kind")?.let {
                    IssnMediaType::value.fromXmlValue(it)
                }
            )
        }
    }

    fun getPageNumbers(item: Item): Pages? {
        val firstPage = item.getPropertyText("first-page")
        val lastPage = item.getPropertyText("last-page")
        val otherPages = item.getPropertyText("other-pages")

        return if (firstPage != null || lastPage != null) {
            Pages(firstPage, lastPage, otherPages)
        } else null
    }

    fun getArchiveLocations(item: Item): ArchiveLocations? {
        val archiveLocations = item.childrenWithRel("archived-with")

        return if (archiveLocations.isNotEmpty()) {
            ArchiveLocations(archiveLocations.mapNotNull { location ->
                location.getPropertyText("name")?.let {
                    Archive(name = it)
                }
            })
        } else null
    }

    private fun issnFromString(issnString: String): String? {
        val identifier = IdentifierParseInput.build(issnString)
        val issn = IssnParser.tryParse(identifier)
        return IssnParser.withoutResolver(issn)
    }

    private fun isbnFromString(isbnString: String): String? {
        val identifier = IdentifierParseInput.build(isbnString)
        val isbn = IsbnParser.tryParse(identifier)
        return IsbnParser.withoutResolver(isbn)
    }

    fun getCitations(item: Item): List<Citation> =
        item.childrenWithRel("cites").map { c ->
            val issn = c.getPropertyText("issn")?.let {
                Issn(
                    value = issnFromString(it),
                    mediaType = c.getPropertyText("issn-type")?.let { t ->
                        IssnMediaType::value.fromXmlValue(t)
                    },
                )
            }

            val isbn = c.getPropertyText("isbn")?.let {
                Isbn(
                    value = isbnFromString(it),
                    mediaType = c.getPropertyText("isbn-type")?.let { t ->
                        IsbnMediaType::value.fromXmlValue(t)
                    },
                )
            }

            // XML schema only injects this when it was matched.
            val provider = when (c.getPropertyText("doi-asserted-by")) {
                "crossref" -> "crossref"
                else -> null
            }

            Citation(
                key = c.getPropertyText("key"),
                doi = c.getPropertyText("doi"),
                provider = provider,
                issn = issn,
                isbn = isbn,
                cyear = c.getPropertyText("year"),
                issue = c.getPropertyText("issue"),
                author = c.getPropertyText("author"),
                volume = c.getPropertyText("volume"),
                editionNumber = c.getPropertyText("edition"),
                seriesTitle = c.getPropertyText("series-title"),
                unstructuredCitation = c.getPropertyText("unstructured"),
                volumeTitle = c.getPropertyText("volume-title"),
                articleTitle = c.getPropertyText("article-title"),
                journalTitle = c.getPropertyText("journal-title"),
                standardsBody = c.getPropertyText("standards-body"),
                stdDesignator = c.getPropertyText("standard-designator")
            )

        }

    fun getPublisher(item: Item): Publisher? {
        val node = item.getProperty("publisher")
        return if (node != null) {
            val locations = node["location"]
            Publisher(
                publisherName = node["name"]?.textValue(),
                publisherPlace = if (!locations.isNull) locations.elements().next()?.get("value")?.textValue() else null
            )
        } else null
    }

    fun getPublisherItems(item: Item): PublisherItem? {
        val itemNumber = item.getPropertyArray("number")?.map {
            ItemNumber(it["kind"].textValue(), it["value"].textValue())
        }
        return if (itemNumber.isNullOrEmpty()) null
        else PublisherItem(itemNumber = itemNumber)
    }
}