package org.crossref.manifold.modules.crossrefxml.xmlelements

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty

data class DoiRecord(
    @field:JacksonXmlProperty(localName = "crossref")
    val crossref: Crossref
)
