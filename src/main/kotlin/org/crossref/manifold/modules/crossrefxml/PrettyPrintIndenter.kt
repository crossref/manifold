package org.crossref.manifold.modules.crossrefxml

import com.fasterxml.jackson.core.JsonGenerator
import com.fasterxml.jackson.core.util.DefaultIndenter
import com.fasterxml.jackson.dataformat.xml.util.DefaultXmlPrettyPrinter
import org.codehaus.stax2.XMLStreamWriter2
import java.io.Serializable

/**
 * jackson's DefaultXmlPrettyPrinter indents with 2 spaces, but we want 4 instead
 * */
class PrettyPrintIndenter : DefaultXmlPrettyPrinter.Indenter, Serializable {
    private val indent = "    "
    private val lineEnd = DefaultIndenter.SYS_LF

    override fun writeIndentation(jsonGen: JsonGenerator, indentLevel: Int) {
        jsonGen.writeRaw(lineEnd)
        repeat(indentLevel) { jsonGen.writeRaw(indent) }
    }

    override fun writeIndentation(streamWriter: XMLStreamWriter2, indentLevel: Int) {
        streamWriter.writeRaw(lineEnd)
        repeat(indentLevel) { streamWriter.writeRaw(indent) }
    }

    override fun isInline() = false
}