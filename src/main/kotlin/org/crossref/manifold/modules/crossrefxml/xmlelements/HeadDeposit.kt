package org.crossref.manifold.modules.crossrefxml.xmlelements

import com.fasterxml.jackson.annotation.JsonPropertyOrder
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement

@JacksonXmlRootElement(localName = "head")
@JsonPropertyOrder("doi_batch_id", "timestamp", "depositor", "registrant")
data class HeadDeposit(
    @field:JacksonXmlProperty(localName = "doi_batch_id")
    val doiBatchId: String,

    val timestamp: Long,

    val depositor: Depositor,

    val registrant: String,
)
