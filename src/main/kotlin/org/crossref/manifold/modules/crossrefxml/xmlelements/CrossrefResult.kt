package org.crossref.manifold.modules.crossrefxml.xmlelements

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement

@JacksonXmlRootElement(localName = "crossref_result")
data class CrossrefResult(
    @field:JacksonXmlProperty(isAttribute = true)
    val version: String? = "3.0",

    @field:JacksonXmlProperty(isAttribute = true, localName = "xsi:schemaLocation")
    val schemaLocation : String = "http://www.crossref.org/qrschema/3.0 http://www.crossref.org/schemas/crossref_query_output3.0.xsd",

    @field:JacksonXmlProperty(isAttribute = true, localName = "xmlns:xsi")
    val xmlNsXsi : String = "http://www.w3.org/2001/XMLSchema-instance",

    @field:JacksonXmlProperty(isAttribute = true, localName = "xmlns")
    val xmlNs : String = "http://www.crossref.org/qrschema/3.0",

    @field:JacksonXmlProperty(localName = "query_result")
    val queryResult : QueryResult
)