package org.crossref.manifold.modules.crossrefxml.xmlelements

import com.fasterxml.jackson.annotation.JsonPropertyOrder
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty

@JsonPropertyOrder(*["name"])
data class Archive (
    @field:JacksonXmlProperty(isAttribute = true)
    val name: String
)
