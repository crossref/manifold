package org.crossref.manifold.modules.crossrefxml.xmlelements

import com.fasterxml.jackson.annotation.JsonInclude

data class BodyDeposit(
    @field:JsonInclude(JsonInclude.Include.NON_NULL)
    val journal: Journal? = null,

    @field:JsonInclude(JsonInclude.Include.NON_NULL)
    val book: Book? = null,
)
