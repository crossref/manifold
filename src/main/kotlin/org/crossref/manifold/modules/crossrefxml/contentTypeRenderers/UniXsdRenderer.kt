package org.crossref.manifold.modules.crossrefxml.contentTypeRenderers

import org.crossref.manifold.modules.crossrefxml.BookElement
import org.crossref.manifold.modules.crossrefxml.JournalElement
import org.crossref.manifold.modules.crossrefxml.XmlElement
import org.crossref.manifold.modules.crossrefxml.XmlRenderer
import org.crossref.manifold.modules.crossrefxml.xmlelements.*
import org.crossref.manifold.rendering.ContentType
import org.crossref.manifold.rendering.RenderingContext
import org.springframework.stereotype.Component


/**
 * Renders ItemTree into <crossref_result> XML.
 */
@Component
class UniXsdRenderer : XmlRenderer() {
    override fun internalContentType(): ContentType = ContentType.UNIXSD

    override fun validateXmlOutput(xmlString: String) = xmlValidator.validateUniXsdXml(xmlString)

    override fun finalizeToString(xmlElement: XmlElement, context: RenderingContext?): String {
        val crossrefXml = when (xmlElement) {
            is BookElement -> Crossref(book = xmlElement.value)
            is JournalElement -> Crossref(journal = xmlElement.value)
        }

        return xmlMapper.writeValueAsString(
            CrossrefResult(
                queryResult = QueryResult(
                    head = Head(),
                    body = Body(
                        query = Query(
                            doiRecord = DoiRecord(
                                crossref = crossrefXml,
                            ),
                            doi = Doi(
                                value = xmlElement.doi
                            )
                        )
                    )
                )
            )
        )
    }
}