package org.crossref.manifold.modules.crossrefxml.xmlelements

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlText

data class ClinicalTrialNumber(
    @field:JacksonXmlProperty(isAttribute = true)
    val registry: String?,

    @field:JacksonXmlText
    val value: String?
)
