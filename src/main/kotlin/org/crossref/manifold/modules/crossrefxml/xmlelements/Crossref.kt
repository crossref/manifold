package org.crossref.manifold.modules.crossrefxml.xmlelements
import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty

data class Crossref(
    @field:JacksonXmlProperty(isAttribute = true, localName = "xmlns")
    val xmlns : String = "http://www.crossref.org/xschema/1.1",

    @field:JacksonXmlProperty(isAttribute = true, localName = "xsi:schemaLocation")
    val schemaLocation : String = "http://www.crossref.org/xschema/1.1 http://doi.crossref.org/schemas/unixref1.1.xsd",

    @field:JacksonXmlProperty(isAttribute = true, localName = "xmlns:jats")
    val jatsNs : String = "http://www.ncbi.nlm.nih.gov/JATS1",

    @field:JsonInclude(JsonInclude.Include.NON_NULL)
    @field:JacksonXmlProperty(localName = "journal")
    val journal: Journal? = null,

    @field:JsonInclude(JsonInclude.Include.NON_NULL)
    @field:JacksonXmlProperty(localName = "book")
    val book: Book? = null,
)
