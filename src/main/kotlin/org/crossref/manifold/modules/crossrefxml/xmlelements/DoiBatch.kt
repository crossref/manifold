package org.crossref.manifold.modules.crossrefxml.xmlelements

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement


@JacksonXmlRootElement(localName = "doi_batch")
data class DoiBatch(
    @field:JacksonXmlProperty(isAttribute = true)
    val version: String? = "5.3.1",

    @field:JacksonXmlProperty(isAttribute = true, localName = "xmlns:xsi")
    val xmlNsXsi: String = "http://www.w3.org/2001/XMLSchema-instance",

    @field:JacksonXmlProperty(isAttribute = true, localName = "xmlns")
    val xmlNs: String = "http://www.crossref.org/schema/5.3.1",

    @field:JacksonXmlProperty(isAttribute = true, localName = "xsi:schemaLocation")
    val schemaLocation: String = "http://www.crossref.org/schema/5.3.1 https://www.crossref.org/schemas/crossref5.3.1.xsd",

    @field:JacksonXmlProperty(isAttribute = true, localName = "xmlns:jats")
    val xmlNsJats: String = "http://www.ncbi.nlm.nih.gov/JATS1",

    @field:JacksonXmlProperty(isAttribute = true, localName = "xmlns:fr")
    val xmlNsFr: String = "http://www.crossref.org/fundref.xsd",

    @field:JacksonXmlProperty(isAttribute = true, localName = "xmlns:mml")
    val xmlNsMml: String = "http://www.w3.org/1998/Math/MathML",

    val head: HeadDeposit,

    val body: BodyDeposit,
)