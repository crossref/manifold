package org.crossref.manifold.modules.crossrefxml.xmlelements

import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.annotation.JsonPropertyOrder
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty


@JsonPropertyOrder(*["license_ref"])
data class AccessIndicatorsProgram (
    @field:JacksonXmlProperty(isAttribute = true)
    val name: String = "AccessIndicators",

    @field:JacksonXmlProperty(isAttribute = true)
    val xmlns: String = "http://www.crossref.org/AccessIndicators.xsd",

    @field:JacksonXmlElementWrapper(useWrapping = false)
    @field:JacksonXmlProperty(localName = "license_ref")
    @field:JsonInclude(JsonInclude.Include.NON_EMPTY)
    val licenseRef: List<LicenseRef>
) : Program
