package org.crossref.manifold.modules.crossrefxml.xmlelements

import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.annotation.JsonPropertyOrder
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty

@JsonPropertyOrder("full_title", "abbrev_title", "issn", "archive_locations", "doi_data")
data class JournalMetadata(
    @field:JacksonXmlProperty(isAttribute = true)
    @field:JsonInclude(JsonInclude.Include.NON_NULL)
    val language: String? = null,

    @field:JacksonXmlProperty(localName = "full_title")
    @field:JsonInclude(JsonInclude.Include.NON_NULL)
    val fullTitle: String? = null,

    @field:JacksonXmlProperty(localName = "abbrev_title")
    @field:JsonInclude(JsonInclude.Include.NON_NULL)
    val abbrevTitle: String? = null,

    @field:JsonInclude(JsonInclude.Include.NON_EMPTY)
    @field:JacksonXmlElementWrapper(useWrapping = false)
    val issn: List<Issn>? = null,

    @field:JacksonXmlProperty(localName = "archive_locations")
    @field:JsonInclude(JsonInclude.Include.NON_NULL)
    val archiveLocations: ArchiveLocations?,

    @field:JacksonXmlProperty(localName = "doi_data")
    @field:JsonInclude(JsonInclude.Include.NON_NULL)
    val doiData: DoiData?,
)
