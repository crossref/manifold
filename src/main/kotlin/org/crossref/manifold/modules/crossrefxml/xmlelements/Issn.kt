package org.crossref.manifold.modules.crossrefxml.xmlelements

import com.fasterxml.jackson.annotation.JsonValue
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlText

data class Issn(
    @field:JacksonXmlProperty(isAttribute = true, localName = "media_type")
    val mediaType: IssnMediaType? = null,

    @field:JacksonXmlText
    val value: String? = null,
)

enum class IssnMediaType(@get:JsonValue val value: String) {
    PRINT("print"),
    ELECTRONIC("electronic");
}
