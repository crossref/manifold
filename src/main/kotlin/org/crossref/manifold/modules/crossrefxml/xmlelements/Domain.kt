package org.crossref.manifold.modules.crossrefxml.xmlelements

data class Domain (
    val domain: String
)
