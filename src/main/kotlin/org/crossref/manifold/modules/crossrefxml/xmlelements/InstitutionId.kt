package org.crossref.manifold.modules.crossrefxml.xmlelements

import com.fasterxml.jackson.annotation.JsonValue
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlText

data class InstitutionId(
    @field:JacksonXmlProperty(isAttribute = true)
    val type: InstitutionIdType? = null,

    @field:JacksonXmlText
    val value: String? = null
)

enum class InstitutionIdType(@get:JsonValue val value: String) {
    ROR("ror"),
    ISNI("isni"),
    WIKIDATA("wikidata"),
}