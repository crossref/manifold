package org.crossref.manifold.modules.crossrefxml.xmlelements

import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.annotation.JsonPropertyOrder
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty

@JsonPropertyOrder(*["journal_metadata", "journal_issue", "journal_article"])
data class Journal(
    @field:JacksonXmlProperty(localName = "journal_metadata")
    @field:JsonInclude(JsonInclude.Include.NON_NULL)
    val journalMetadata: JournalMetadata? = null,

    @field:JacksonXmlProperty(localName = "journal_issue")
    @field:JsonInclude(JsonInclude.Include.NON_NULL)
    val journalIssue: JournalIssue? = null,

    @field:JacksonXmlProperty(localName = "journal_article")
    @field:JsonInclude(JsonInclude.Include.NON_NULL)
    val journalArticle: JournalArticle? = null,
)
