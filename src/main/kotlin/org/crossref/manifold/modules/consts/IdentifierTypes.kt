package org.crossref.manifold.modules.consts

/**
 * Collection of Identifier type constants. These correspond to the content of identifiers.schema.json and are included
 * to support Module authors in using a shared vocabulary.
 */
object IdentifierTypes {
    /**
     * Item applies to the whole Manifold kernel, so we bake it into the Identifier Type registry.
     */
    const val ITEM_URI = "http://id.crossref.org/item/"
    const val ITEM_PREFIX = "item"

    const val MEMBER_URI = "https://id.crossref.org/member/"
    const val MEMBER_PATTERN = "^https://id.crossref.org/member/"

    const val ORGANIZATION_URI = "https://id.crossref.org/org/"
    const val ORGANIZATION_PATTERN = "^https://id.crossref.org/org/"
}
