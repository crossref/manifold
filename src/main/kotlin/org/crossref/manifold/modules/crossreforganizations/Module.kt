package org.crossref.manifold.modules.crossreforganizations

import org.crossref.manifold.itemgraph.ItemGraph
import org.crossref.manifold.itemgraph.Resolver
import org.crossref.manifold.modules.Manifest
import org.crossref.manifold.modules.ModuleRegistrar
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty
import org.springframework.context.annotation.Configuration
import org.springframework.scheduling.annotation.Scheduled

@ConditionalOnProperty(prefix = Module.CROSSREF_ORGANIZATIONS, name = [Module.SUGAR_IMPORT_ENABLED])
@Configuration(Module.CROSSREF_ORGANIZATIONS)
class Module(
    moduleRegistrar: ModuleRegistrar,
    private val resolver: Resolver,
    private val itemGraph: ItemGraph,
    @Value("\${$CROSSREF_ORGANIZATIONS.$SUGAR_URL}") private val url: String,
    @Value("\${$CROSSREF_ORGANIZATIONS.$SUGAR_USERNAME}") private val username: String,
    @Value("\${$CROSSREF_ORGANIZATIONS.$SUGAR_PASSWORD}") private val password: String,
) {

    private val logger: Logger = LoggerFactory.getLogger(this::class.java)

    companion object {
        const val CROSSREF_ORGANIZATIONS = "crossref-organizations"
        const val SUGAR_IMPORT_ENABLED = "sugar-import-enabled"
        const val SUGAR_IMPORT_CRON = "sugar-import-cron"
        const val SUGAR_URL = "sugar-url"
        const val SUGAR_USERNAME = "sugar-username"
        const val SUGAR_PASSWORD = "sugar-password"
    }

    init {
        moduleRegistrar.register(
            Manifest(
                name = CROSSREF_ORGANIZATIONS,
                description = "Crossref Organizations",
            ),
        )
    }

    fun sugarIngestion(): SugarIngestion = SugarIngestion(SugarService(url), itemGraph)

    @Scheduled(cron = "\${$CROSSREF_ORGANIZATIONS.$SUGAR_IMPORT_CRON}")
    fun importFromSugar() {
        try {
            val ingestion = sugarIngestion()
            ingestion.ingest(username, password)
        } catch (exception: Exception) {
            logger.error("Scheduled Sugar Ingestion failed: ${exception.message}")
        }
    }
}
