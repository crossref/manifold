package org.crossref.manifold.modules.crossreforganizations

import org.crossref.manifold.identifiers.IdentifierParser
import org.crossref.manifold.ingestion.Envelope
import org.crossref.manifold.ingestion.EnvelopeBatch
import org.crossref.manifold.ingestion.EnvelopeBatchProvenance
import org.crossref.manifold.ingestion.UserAgentParser
import org.crossref.manifold.itemgraph.ItemGraph
import org.crossref.manifold.itemgraph.ItemTreeAssertion
import org.crossref.manifold.itemtree.*
import org.crossref.manifold.modules.consts.Items
import org.crossref.manifold.modules.consts.RelationshipTypes
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import java.time.OffsetDateTime

class SugarIngestion(
    private val sugarService: SugarService,
    private val itemGraph: ItemGraph,
) {
    private val logger: Logger = LoggerFactory.getLogger(this::class.java)

    /**
     * There are around 20k members, so we can hold this in memory.
     */
    private fun retrieveAll(username: String, password: String) : List<SugarOrganization> {
        logger.info("Starting organization ingestion")

        val token = sugarService.authenticate(username, password)


        val allRecords = mutableListOf<SugarOrganization>()

        var organizations = PagedSugarOrganizations(0, arrayOf())

        // The Sugar API returns nextOffset of -1 when there are no more items
        while (organizations.nextOffset != -1) {
            logger.info("Getting page from Sugar starting ${organizations.nextOffset}... ")
            organizations = sugarService.getOrganizations(token, organizations.nextOffset)

            allRecords.addAll(organizations.records)
            organizations.records

            // Sleep to limit the number of called per second to the Sugar API
            Thread.sleep(100)
        }
        logger.info("Finished organization ingestion")
        return allRecords
    }

    /**
     * Remove bad data. Duplicate Sugar IDs and Member IDs are reported and removed.
     */
    private fun dedupe(input: List<SugarOrganization>) : List<SugarOrganization> {
        val duplicateSugarIdEntries = input.groupBy { it.id }.filter { it.value.count() > 1 }
        val duplicateCrossrefIdEntries = input.groupBy { it.crossrefMemberId }.filter { it.value.count() > 1 }

        if (duplicateSugarIdEntries.isNotEmpty()) {
            logger.error("Found duplicate Sugar IDs: $duplicateSugarIdEntries")
        }

        if (duplicateCrossrefIdEntries.isNotEmpty()) {
            logger.error("Found duplicate Crossref Member IDs: $duplicateCrossrefIdEntries")
        }

        val duplicateSugarIds = duplicateSugarIdEntries.map { it.key }.toSet()
        val duplicateCrossrefIds = duplicateCrossrefIdEntries.map { it.key }.toSet()

        val filtered = input.filterNot { duplicateSugarIds.contains(it.id) || duplicateCrossrefIds.contains(it.crossrefMemberId)  }

        if (filtered.count() != input.count()) {
            logger.error("Got ${input.count()} entries from Sugar but ingesting only ${filtered.count()}")
        }

        return filtered
    }

    /**
     * Ingest organization data downloaded from Sugar into the Item Graph.
     *
     * @return the ingestion id, which can be used to track progress
     */
    fun ingest(username: String, password: String) {

        val entries = retrieveAll(username, password)

        val deduped = dedupe(entries)

        logUnrecognised(deduped)

        // Look up org by its Sugar ID.
        val crossrefMemberIds = deduped.associateBy { it.id }

        val trees = deduped.map {
            val memberRel = if ((!it.inactive) && it.isMember) {
                Relationship(
                    RelationshipTypes.MEMBER_OF,
                    Item().withIdentifier(IdentifierParser.parse(Items.CROSSREF_AUTHORITY)),
                    assertedAt = null,
                )
            } else null

            val sponsorshipRel = if (it.isSponsored && !it.sponsorId.isNullOrEmpty()) {
                val sponsorIdentifier = crossrefMemberIds[it.sponsorId]?.identifier
                if (sponsorIdentifier != null) {

                    Relationship(
                        RelationshipTypes.SPONSOR,
                        Item().withIdentifier(IdentifierParser.parse(sponsorIdentifier)),
                        assertedAt = null,
                    )
                } else null
            } else null

            val relationships = listOf(memberRel, sponsorshipRel).filterNotNull()


            ItemTree(Item()
                .withIdentifier(IdentifierParser.parse(it.identifier))
                .withProperties(
                    listOf(
                        Properties(
                            jsonObjectFromMap(
                                mapOf(
                                    "name" to it.name,
                                    "type" to "org",
                                ),
                            ),
                        ),
                    )
                )
                .withRelationships(relationships))
        }

        val envelopeBatch =
            EnvelopeBatch(
                listOf(Envelope(
                    trees,
                    ItemTreeAssertion(
                        OffsetDateTime.now(),
                        Item().withIdentifier(IdentifierParser.parse(Items.CROSSREF_AUTHORITY)),
                    ),
                )),
            EnvelopeBatchProvenance(UserAgentParser.parse("Crossref SugarCRM Import/1.0"), OffsetDateTime.now().toString()),
        )

        logger.info("Asserting organizations into the item graph")


        // As this is called regularly with rarely-changing data, do a simple de-duplication.
        // If the tree didn't change except for timestamp, then don't re-assert.
        itemGraph.ingest(listOf(envelopeBatch)) { old, new ->
            // If there are no prior insertions, always insert.
            if (old == null) {
                new
            } else {
                // If there are no differences except timestamp, don't re-insert.
                if (! old.root.equalsIgnoringTime(new.root)) {
                    new
                } else null
            }
        }

        logger.info("Asserted organizations into the item graph")
    }

    private fun logUnrecognised(organizations: List<SugarOrganization>) {
        val accountTypes = organizations.map { it.accountType }.groupingBy { it }.eachCount()
        for ((value, count) in accountTypes) {
            if (AccountTypes.values().none { it.value == value }) {
                logger.error("Didn't recognise account type '$value', but found $count of them.")
            }
        }
    }
}
