package org.crossref.manifold.modules.crossreforganizations

import com.fasterxml.jackson.databind.PropertyNamingStrategies
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import org.crossref.manifold.itemtree.ItemTree
import org.crossref.manifold.modules.consts.RelationshipTypes
import org.crossref.manifold.rendering.ContentType
import org.crossref.manifold.rendering.ContentTypeRenderer
import org.crossref.manifold.rendering.RenderingContext
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Service

@Service
class OrganizationRenderer() : ContentTypeRenderer {
    private val logger: Logger = LoggerFactory.getLogger(this::class.java)

    private val mapper = jacksonObjectMapper().setPropertyNamingStrategy(PropertyNamingStrategies.KEBAB_CASE)

    override fun internalContentType(): ContentType = ContentType.ORGANIZATION_JSON

    override fun publicContentType(): ContentType = ContentType.APPLICATION_JSON

    override fun offer(type: String?, subtype: String?, itemTree: ItemTree?, context: RenderingContext?): Boolean = type == "org"

    override fun render(itemTree: ItemTree, context: RenderingContext?): String? {
        logger.debug("Rendering ${itemTree.root.pk} using ${internalContentType()}")

        val property = itemTree.root.properties.firstOrNull()
        if (property == null) {
            logger.error("Tried to render ${itemTree.root.pk} as organisation JSON but the expected property assertions cannot be found")
            return null
        }

        val primaryName = property.values.get("name").textValue()
        val type = property.values.get("type").textValue()
        val sponsoredBy = itemTree.root.rels.firstOrNull { it.relTyp == RelationshipTypes.SPONSOR }?.obj

        val member = Organization(
            type = type,
            primaryName = primaryName,
            identifiers = itemTree.root.identifiers.map { OrganizationIdentifier(it.uri) },
            relationships = OrganizationRelationship(
                sponsoredBy = sponsoredBy?.let {
                    NamedOrganizationIdentifiers(
                        sponsoredBy.identifiers.map { OrganizationIdentifier(it.uri) },
                        sponsoredBy.properties.firstOrNull()?.values?.get("name")?.textValue(),
                    )
                },
            ),
        )

        return mapper.writeValueAsString(member)
    }
}
