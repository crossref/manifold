package org.crossref.manifold.modules.eventdata.support

import org.crossref.manifold.itemtree.Item
import org.crossref.manifold.itemtree.ItemTree
import org.crossref.manifold.itemtree.Relationship
import org.crossref.manifold.lookup.RaLookUp
import org.slf4j.Logger
import org.slf4j.LoggerFactory

object Datacite {

    private var logger: Logger = LoggerFactory.getLogger(this::class.java)

    private fun Event.toLegacyDataciteEvent() = LegacyDataciteEvent(
        id = id,
        subjId = subjId,
        objId = objId,
        relationTypeId = relationTypeId,
        occurredAt = occurredAt,
        subj = LegacyDataciteEventSubj(workTypeId = subj?.workTypeId)
    )

    /**
     * Overload fn for compatibility with live ingestion
     */
    suspend fun itemTreeFrom(event: Event, raLookUp: RaLookUp): ItemTree? =
        itemTreeFrom(event.toLegacyDataciteEvent(), raLookUp)

    /**
     * Convert a Datacite event to an ItemTree.
     * The RA is only asserted for the subject, and it is always Datacite.
     */
    suspend fun itemTreeFrom(event: LegacyDataciteEvent, raLookUp: RaLookUp): ItemTree? = event.run {

        // We want to ensure that we will only process events where
        // the subject is registered by Datacite and the object
        // is registered by Crossref. Everything else should be discarded.
        val subjectRa = raLookUp.getRaFor(subjId)

        if (subjectRa == null) {
            logger.warn("Failed to retrieve subject registration agency for prefix $subjId. Will not convert: $id")
            return null
        } else if (subjectRa != "DataCite") {
            return null
        }

        val objectRa = raLookUp.getRaFor(objId)

        if (objectRa == null) {
            logger.warn("Failed to retrieve object registration agency for prefix $objId. Will not convert: $id")
            return null
        } else if (objectRa != "Crossref") {
            return null
        }

        val objItem = Item(identifiers = listOf(objId))

        // The two different attributes for the work type
        // belong to two different eras of submissions.
        // The change occurred on 2019-03-23 and there should be no records containing both attributes.
        // If any ever exist though, choose the workTypeId first.
        val subtype = subj?.let {
            listOfNotNull(
                it.workTypeId,
                it.atType
            ).firstOrNull()
        }

        val properties = if (subtype.isNullOrBlank()) {
            emptyMap()
        } else {
            mapOf("type" to "work", "subtype" to subtype)
        }

        return ItemTree(
            Item(
                identifiers = listOf(subjId),
                rels = listOf(
                    Relationship(relationTypeId, objItem, assertedAt = null)
                ),
            ).withPropertiesFromMap(properties)
        )
    }
}
