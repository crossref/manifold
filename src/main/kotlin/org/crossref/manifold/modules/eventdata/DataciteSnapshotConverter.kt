package org.crossref.manifold.modules.eventdata

import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.SerializationFeature
import com.fasterxml.jackson.databind.util.StdDateFormat
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule
import com.fasterxml.jackson.module.kotlin.KotlinModule
import com.fasterxml.jackson.module.kotlin.readValue
import kotlinx.coroutines.*
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.consumeAsFlow
import kotlinx.coroutines.flow.toSet
import org.crossref.manifold.identifiers.IdentifierParser
import org.crossref.manifold.identifiers.IdentifierType
import org.crossref.manifold.identifiers.parsers.Doi
import org.crossref.manifold.ingestion.*
import org.crossref.manifold.itemgraph.ItemTreeAssertion
import org.crossref.manifold.itemtree.Item
import org.crossref.manifold.lookup.InMemoryRaLookUp
import org.crossref.manifold.lookup.RaLookUp
import org.crossref.manifold.modules.consts.Items
import org.crossref.manifold.modules.eventdata.support.Datacite
import org.crossref.manifold.modules.eventdata.support.LegacyDataciteEvent
import org.crossref.manifold.modules.unixml.support.PARSE_PARALLELISM
import org.crossref.manifold.modules.unixml.support.sendTarGzTo
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import java.io.File
import java.time.OffsetDateTime
import kotlin.io.path.Path
import kotlin.io.path.absolutePathString
import kotlin.io.path.isDirectory
import kotlin.io.path.name


object DataciteSnapshotConverter {
    private var logger: Logger = LoggerFactory.getLogger(this::class.java)

    private val mapper: ObjectMapper = ObjectMapper()
        .registerModule(JavaTimeModule())
        .disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES)
        .disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS)
        .setDateFormat(StdDateFormat().withColonInTimeZone(true))
        .setSerializationInclusion(JsonInclude.Include.NON_EMPTY)
        .registerModule(KotlinModule())

    private val dataciteAuthority = Item(listOf(IdentifierParser.parse(Items.DATACITE_AUTHORITY)))

    fun run(src: String, dst: String, aRaLookUp: RaLookUp? = null) {

        val raLookUp = aRaLookUp ?: InMemoryRaLookUp()

        val srcPath = Path(src).toAbsolutePath()
        val dstPath = Path(dst).toAbsolutePath()

        if (srcPath.isDirectory()) {
            srcPath.toFile().listFiles()
                ?.filter { it.isFile && it.extension == "gz" }
                ?.forEach {
                    val file = Path(
                        dstPath.absolutePathString(),
                        "${it.name.removeSuffix(".tar.gz")}.tree.tar.gz"
                    ).toFile()

                    convertSingleFile(it, file, raLookUp)
                }
        } else {
            val file = if (dstPath.isDirectory()) {
                Path(
                    dstPath.absolutePathString(),
                    "${srcPath.name.removeSuffix(".tar.gz")}.tree.tar.gz"
                ).toFile()
            } else {
                Path(dstPath.absolutePathString()).toFile()
            }

            convertSingleFile(srcPath.toFile(), file, raLookUp)
        }
    }

    private fun warmUpRACache(srcFile: File, raLookUp: RaLookUp) {
        val contentFilenameChannel = Channel<Pair<String, String>>(PARSE_PARALLELISM)
        val doiChannel = Channel<String>(PARSE_PARALLELISM)

        runBlocking {

            val parseTasks = (0..PARSE_PARALLELISM).map {
                launch(CoroutineName("parse-event-record-$it") + Dispatchers.IO) {
                    logger.info("Start prefix cache warm-up on ${Thread.currentThread().name}")

                    for ((itemFilename, content) in contentFilenameChannel) {
                        try {
                            mapper.readValue<LegacyDataciteEvent>(content).objId.let { objId ->
                                if (objId.type == IdentifierType.DOI) {
                                    Doi.doiPrefix(objId)?.let { prefix -> doiChannel.send(prefix) }
                                }
                            }
                        } catch (exception: Exception) {
                            logger.error("Failed file $itemFilename")
                            exception.printStackTrace()
                        }
                    }
                }
            }

            val cacheTask = launch(CoroutineName("prefix-cache") + Dispatchers.IO) {
                try {
                    val prefixes = doiChannel.consumeAsFlow().toSet()

                    prefixes.chunked(100).forEach {
                        logger.info("Retrieving RA for batch of ${it.size} dois")
                        raLookUp.fetchRaFor(it.toSet())
                    }
                } catch (e: Exception) {
                    logger.error("Prefix caching: ${e.message}")
                }
            }

            sendTarGzTo(srcFile, contentFilenameChannel)

            logger.info("Parsing all DOIs...")
            parseTasks.joinAll()
            doiChannel.close()

            logger.info("Caching responses...")
            cacheTask.join()

        }
    }

    private fun convertSingleFile(srcFile: File, dstFile: File, raLookUp: RaLookUp) {

        if (raLookUp.cacheEnabled()) {
            warmUpRACache(srcFile, raLookUp)
        }

        val archive = EnvelopeBatchArchiveWriter(dstFile)

        val contentFilenameChannel = Channel<Pair<String, String>>(PARSE_PARALLELISM)
        val envelopeBatchChannel = Channel<Pair<String, EnvelopeBatch>>(PARSE_PARALLELISM)

        runBlocking {

            val parseTasks = (0..PARSE_PARALLELISM).map {
                launch(CoroutineName("parse-event-record-$it") + Dispatchers.IO) {
                    logger.info("Start Event record converter on ${Thread.currentThread().name}")

                    var count = 0
                    var unparsable = 0

                    for ((itemFilename, content) in contentFilenameChannel) {
                        try {

                            val event = mapper.readValue<LegacyDataciteEvent>(content)

                            // convert event to item tree
                            val item = Datacite.itemTreeFrom(event, raLookUp)

                            if (item != null) {
                                val envelope = Envelope(
                                    listOf(item),
                                    ItemTreeAssertion(
                                        OffsetDateTime.parse(event.occurredAt),
                                        dataciteAuthority
                                    )
                                )

                                val envelopeBatch =
                                    EnvelopeBatch(
                                        listOf(envelope),
                                        EnvelopeBatchProvenance(UserAgentParser.parse("Datacite agent/1.0"), event.id)
                                    )

                                envelopeBatchChannel.send(event.id to envelopeBatch)

                            } else {
                                unparsable++
                            }

                        } catch (exception: Exception) {
                            logger.error("Failed file $itemFilename")
                            exception.printStackTrace()
                        }
                        count++
                    }
                    logger.info("$unparsable out of $count files failed to parse. Successful: ${count - unparsable}")
                }
            }

            val writeTask = launch(CoroutineName("write") + Dispatchers.IO) {
                try {
                    archive.use {
                        for ((filename, envelopeBatch) in envelopeBatchChannel) {

                            logger.info("Archiving envelope batch $filename containing ${envelopeBatch.envelopes.sumOf { env -> env.itemTrees.size }} item trees")
                            archive.add(filename, listOf(envelopeBatch))
                        }
                    }
                } catch (e: Exception) {
                    logger.error("Archive writing: ${e.message}")
                }
            }


            // Read the Zip file sequentially in this thread. Others will continue in the background.
            logger.info("Read TGZ...")

            sendTarGzTo(srcFile, contentFilenameChannel)

            logger.info("Wait for parsing")
            parseTasks.joinAll()
            envelopeBatchChannel.close()

            logger.info("Wait for writing...")
            writeTask.join()
            logger.info("Finished writing...")

            logger.info("Prefix cache: ${raLookUp.cacheSize()}")
        }

    }
}