package org.crossref.manifold.modules.eventdata.support

import io.micrometer.core.instrument.Counter
import io.micrometer.core.instrument.MeterRegistry


class TaggedCounter(
    private val name: String,
    private val tagName: String,
    private val registry: MeterRegistry
) {
    private val counters: MutableMap<String, Counter?> = HashMap<String, Counter?>()

    fun increment(tagValue: String) {
        var counter: Counter? = counters[tagValue]
        if (counter == null) {
            counter = Counter.builder(name).tags(tagName, tagValue).register(registry)
            counters[tagValue] = counter
        }
        counter.increment()
    }
}
