package org.crossref.manifold.kernel

import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController

@RestController
class HeartbeatController {
    /**
     * Returns a simple OK / HTTP 200 response.
     */
    @GetMapping("/heartbeat")
    fun heartbeat(): String = "OK"
}