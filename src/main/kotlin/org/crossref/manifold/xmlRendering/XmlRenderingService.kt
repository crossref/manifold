package org.crossref.manifold.xmlRendering

import org.crossref.manifold.itemtree.ItemTree
import org.crossref.manifold.modules.crossrefxml.contentTypeRenderers.DepositContext
import org.crossref.manifold.modules.crossrefxml.contentTypeRenderers.DepositXmlRenderer
import org.crossref.manifold.util.JsonItemTreeMapper
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Service


class UnsupportedFormatException(message: String) : RuntimeException(message)

@Service
class XmlRenderingService {
    private val logger: Logger = LoggerFactory.getLogger(this::class.java)
    private val depositXmlRenderer = DepositXmlRenderer()

    fun renderDepositXml(content: String, context: DepositContext): String {
        return try {
            val metadataTree = JsonItemTreeMapper.mapper.readValue(content, ItemTree::class.java)
            depositXmlRenderer.render(metadataTree, context)
        } catch (e: Exception) {
            logger.error(e.stackTraceToString())
            throw UnsupportedFormatException("Failed to parse ItemTree JSON")
        }
    }
}
