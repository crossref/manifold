package org.crossref.manifold.xmlRendering

import com.fasterxml.jackson.databind.JsonNode
import com.fasterxml.jackson.databind.ObjectMapper
import io.swagger.v3.oas.annotations.Operation
import io.swagger.v3.oas.annotations.media.Content
import io.swagger.v3.oas.annotations.media.Schema
import io.swagger.v3.oas.annotations.responses.ApiResponse
import io.swagger.v3.oas.annotations.tags.Tag
import org.crossref.manifold.modules.crossrefxml.contentTypeRenderers.DepositContext
import org.crossref.manifold.util.JsonValidator
import org.crossref.manifold.util.MimeType
import org.joda.time.DateTime
import org.joda.time.DateTimeZone
import org.joda.time.format.DateTimeFormat
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.CrossOrigin
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RestController

@Tag(name = "Metadata Format Converter")
@RestController
class XmlRenderingController(private val xmlRenderingService: XmlRenderingService) {
    private val objectMapper = ObjectMapper()

    // Pattern symbols: yyyy-MM-dd'T'HH:mm:ss.SSSZZ
    private val dateTimeFmt = DateTimeFormat.forPattern("yyyyMMddHHmmssSSS")

    // we need to explicitly set response charset to UTF-8, otherwise unicode characters in XML will be corrupted
    private val responseHeaders = HttpHeaders().apply {
        set("Content-Type", "${MimeType.DEPOSIT_XML};charset=UTF-8")
    }

    @PostMapping(
        value = ["/beta/convert"],
        consumes = [MediaType.APPLICATION_JSON_VALUE],
    )
    @Operation(
        summary = "Crossref Metadata Format Converter",
        description = "Beta: converts Crossref metadata ItemTree in JSON format into the Deposit XML format. This endpoint is under active development, we may change it without notice.",
        responses = [
            ApiResponse(
                responseCode = "200",
                description = "Metadata rendering successful",
                content = [
                    Content(
                        mediaType = MimeType.DEPOSIT_XML,
                        schema = Schema(type = "string")
                    )
                ]
            ),
            ApiResponse(responseCode = "400", description = "Invalid input format"),
            ApiResponse(responseCode = "500", description = "Internal Server Error")
        ]
    )
    @CrossOrigin

    private fun renderXml(@RequestBody body: String): ResponseEntity<String> {
        val requestJson = objectMapper.readTree(body)
        val output = JsonValidator.validateConvertRequest(requestJson)
        if (!output.isValid) {
            return ResponseEntity
                .status(HttpStatus.BAD_REQUEST)
                .headers(responseHeaders)
                .body(output.toString())
        }

        val userInfo = requestJson.path("userInfo")
        val depositContext = DepositContext(
            doiBatchId = userInfo?.path("doiBatchId")?.getNonEmptyValue() ?: "none",
            depositorName = userInfo?.path("depositorName")?.getNonEmptyValue() ?: "Replace this with your name",
            depositorEmail = userInfo?.path("depositorEmail")?.getNonEmptyValue() ?: "Replace this with your email",
            registrant = userInfo?.path("registrant")?.getNonEmptyValue() ?: "Replace this with the Registrant",
            timestamp = dateTimeFmt.print(DateTime(DateTimeZone.forID("GMT"))).toLong(),
        )

        return try {
            val xml = xmlRenderingService.renderDepositXml(
                content = requestJson.path("itemTree").toString(),
                context = depositContext
            )
            ResponseEntity
                .ok()
                .headers(responseHeaders)
                .body(xml)
        } catch (e: UnsupportedFormatException) {
            ResponseEntity
                .status(HttpStatus.BAD_REQUEST)
                .headers(responseHeaders)
                .body(e.message)
        }
    }

    private fun JsonNode.getNonEmptyValue(): String? {
        return this.asText()?.ifBlank { null }
    }
}
