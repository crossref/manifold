package org.crossref.messaging.aws.s3

import org.springframework.core.MethodParameter
import org.springframework.messaging.Message
import org.springframework.messaging.converter.MappingJackson2MessageConverter
import org.springframework.messaging.converter.MessageConverter
import org.springframework.messaging.handler.invocation.HandlerMethodArgumentResolver

/**
 * Handler method argument resolver that converts a [org.springframework.messaging.Message] payload to a
 * [S3EventNotification].
 *
 * If the resolver's [org.springframework.messaging.converter.MessageConverter] is a
 * [org.springframework.messaging.converter.MappingJackson2MessageConverter] then its
 * [com.fasterxml.jackson.databind.ObjectMapper] is used to directly parse the JSON in the message payload. Otherwise
 * the converter's [org.springframework.messaging.converter.MessageConverter.fromMessage] function is called.
 *
 * @param messageConverter used for converting the message payload
 */
class S3EventResolver(private val messageConverter: MessageConverter) : HandlerMethodArgumentResolver {
    companion object {
        private const val MESSAGE = "Message"
        private const val RECORDS = "Records"
        private const val S3 = "s3"
        private const val BUCKET = "bucket"
        private const val NAME = "name"
        private const val OBJECT = "object"
        private const val KEY = "key"
    }

    override fun supportsParameter(parameter: MethodParameter): Boolean =
        parameter.parameterType.isAssignableFrom(S3EventNotification::class.java)

    override fun resolveArgument(parameter: MethodParameter, message: Message<*>): Any? {
        return if (messageConverter is MappingJackson2MessageConverter) {
            val payloadNode = messageConverter.objectMapper.readTree(message.payload.toString())
            val recordsNode = messageConverter.objectMapper.readTree(payloadNode[MESSAGE].asText())[RECORDS]
            S3EventNotification(recordsNode.map {
                S3EventNotification.Record(
                    it.get(S3)
                        .get(OBJECT)
                        .get(KEY)
                        .textValue(),
                    it.get(S3)
                        .get(BUCKET)
                        .get(NAME)
                        .textValue()
                )
            })
        } else {
            messageConverter.fromMessage(message, parameter.parameterType)
        }
    }
}
