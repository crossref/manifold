package org.crossref.manifold.common

import io.cucumber.java.en.Then
import io.cucumber.java.en.When
import org.crossref.manifold.identifiers.IdentifierParser
import org.crossref.manifold.ingestion.Envelope
import org.crossref.manifold.ingestion.EnvelopeBatch
import org.crossref.manifold.ingestion.EnvelopeBatchProvenance
import org.crossref.manifold.ingestion.UserAgentParser
import org.crossref.manifold.itemgraph.*
import org.crossref.manifold.itemtree.Item
import org.crossref.manifold.itemtree.ItemTree
import org.crossref.manifold.itemtree.ItemTreeService
import org.crossref.manifold.itemtree.Relationship
import org.junit.Before
import org.junit.jupiter.api.Assertions.*
import java.time.OffsetDateTime

class ItemGraphSD(
    private val itemGraph: ItemGraph,
    private val itemGraphSetup: ItemGraphSetup,
    private val resolver: Resolver,
    private val itemTreeService: ItemTreeService
) {
    @Before
    fun truncateItemGraphTables() {
        itemGraphSetup.truncateItemGraphTables()
    }

    @When("{string} asserts relationship {string} {string} {string} in the Item Graph")
    fun asserts_relationship(assertedBy: String, subj: String, relationship: String, obj: String) {
        val envelope = Envelope(
            listOf(
                ItemTree(Item().withIdentifier(IdentifierParser.parse(subj))
                    .withRelationship(
                        Relationship(
                            relationship, Item()
                                .withIdentifier(IdentifierParser.parse(obj)), assertedAt = null
                        )
                    )
            )
            ),
            ItemTreeAssertion(
                OffsetDateTime.now(),
                Item().withIdentifier(IdentifierParser.parse(assertedBy))
            )
        )

        val envelopeBatch = EnvelopeBatch(
            listOf(envelope),
            EnvelopeBatchProvenance(UserAgentParser.parse("Crossref Test/1.0.0"), "1234"),
        )

        itemGraph.ingest(listOf(envelopeBatch))
    }

    @When("{string} asserts property {string} {string}: {string} in the Item Graph")
    fun asserts_property(assertedBy: String, subj: String, property: String, value: String) {
        val envelope = Envelope(
            listOf(
                ItemTree(Item().withIdentifier(IdentifierParser.parse(subj))
                    .withPropertiesFromMap(mapOf(property to value))
            )),
            ItemTreeAssertion(
                OffsetDateTime.now(),
                Item().withIdentifier(IdentifierParser.parse(assertedBy))
            )
        )

        val envelopeBatch = EnvelopeBatch(
            listOf(envelope),
            EnvelopeBatchProvenance(UserAgentParser.parse("Crossref Test/1.0.0"), "1234"),
        )

        itemGraph.ingest(listOf(envelopeBatch))
    }

    @Then("the Item Graph contains Item {string}")
    fun the_item_graph_contains_item(identifier: String) {
        val result = resolver.resolveRO(IdentifierParser.parse(identifier))

        assertNotNull(result.pk, "Expected to  find Item $identifier in the Item Graph")
    }

    @Then("the Item Graph contains members")
    fun the_item_graph_contains_members(members: List<Statements.Member>) {

            members.forEach { m ->


            val result = resolver.resolveRO(IdentifierParser.parse(m.identifier))
            assertNotNull(result.pk, "Expected to find Item ${m.identifier} in the Item Graph")

            val tree = itemTreeService.getCanonicalItemTree(result.pk!!)

            assertEquals(m.name,
                tree?.second?.root?.properties?.first()?.values?.get("name")?.textValue(),
                "Expected to find name property with value '${m.name}' for '${m.identifier}' in tree: $tree")
        }
    }

    @Then("the Item Graph does not contain members")
    fun the_item_graph_does_not_contain_members(members: List<Statements.Member>) {
        members.forEach { m ->
            val result = resolver.resolveRO(IdentifierParser.parse(m.identifier))
            assertNull(result.pk, "Expected not to find Item ${m.identifier} in the Item Graph")
        }
    }
}
