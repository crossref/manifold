package org.crossref.manifold.common

import org.springframework.stereotype.Component
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.MvcResult
import org.springframework.test.web.servlet.RequestBuilder
import org.springframework.test.web.servlet.ResultActions
import org.springframework.test.web.servlet.ResultHandler
import org.springframework.test.web.servlet.ResultMatcher

@Component
class ApiResponseContext(private val mockMvc: MockMvc) : ResultActions {
    private lateinit var currentResponse: ResultActions

    override fun andExpect(matcher: ResultMatcher): ResultActions = currentResponse.andExpect(matcher)

    override fun andDo(handler: ResultHandler): ResultActions = currentResponse.andDo(handler)

    override fun andReturn(): MvcResult = currentResponse.andReturn()

    fun perform(requestBuilder: RequestBuilder): ResultActions {
        currentResponse = mockMvc.perform(requestBuilder)
        return currentResponse
    }
}
