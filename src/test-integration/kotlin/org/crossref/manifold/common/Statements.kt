package org.crossref.manifold.common

import io.cucumber.java.DataTableType
import org.crossref.manifold.modules.crossreforganizations.SugarOrganization
import org.crossref.manifold.rendering.ContentType
import java.time.LocalDateTime
import java.time.OffsetDateTime
import java.time.ZoneOffset

/**
 * Statements are used in a lot of places: ingestion, retrieval, archives, Given, Then, etc.
 * These classes can be used to parse data tables.
 */
object Statements {
    private const val SUBJECT_ID = "Subject ID"
    private const val SUBJECT_COLLECTION = "Subj Collection"
    private const val SUBJECT_TYPE = "Subj Type"

    // When this is a relationship, the object ID.
    private const val OBJECT_ID = "Object ID"
    private const val OBJECT_COLLECTION = "Obj Collection"
    private const val OBJECT_TYPE = "Obj Type"

    // When this is a relationship, the relationship type.
    private const val RELATIONSHIP_TYPE = "Relationship Type"

    // When this is a property, the property type.
    private const val PROPERTY = "Property"

    // When this is a property, the property value.
    private const val VALUE = "Value"

    private const val ASSERTED_AT = "Asserted At"
    private const val ASSERTED_BY = "Asserted By"
    private const val LINKED_BY = "Linked By"

    private const val INGESTED_AT = "Ingested At"
    private const val CREATED_TIME = "Created Time"
    private const val UPDATED_TIME = "Updated Time"

    private const val CURRENT = "Current"
    private const val STATE = "State"
    private const val COUNT = "Count"

    private const val IDENTIFIER = "Identifier"
    private const val STALE = "Stale?"
    private const val RENDERED = "Rendered?"
    private const val INTERNAL_CONTENT_TYPE = "Internal Content Type"
    private const val PUBLIC_CONTENT_TYPE = "Public Content Type"
    private const val TITLE = "Title"
    private const val NUMBER_OF_RENDERS = "Number of renders"
    private const val UPDATED_AT = "Updated At"

    private const val SUGAR_INTERNAL_ID = "Sugar Internal Id"
    private const val NAME = "Name"
    private const val ACCOUNT_TYPE = "Account Type"
    private const val CROSSREF_MEMBER_ID = "Crossref Member Id"
    private const val SPONSOR_ID = "Sponsor Id"
    private const val INACTIVE = "Inactive"

    /**
     * Get asserted at, or now.
     */
    fun assertedAt(statement: Map<String, String>): OffsetDateTime =
        statement[ASSERTED_AT]?.let {
            OffsetDateTime.parse(it)
        } ?: OffsetDateTime.now()

    /**
     * Get ingested at, or null.
     */
    private fun dateTimeOrNull(statement: Map<String, String>, dateTimeField: String): OffsetDateTime? =
        statement[dateTimeField]?.let {
            OffsetDateTime.parse(it)
        }

    /**
     * Get count or 1.
     */
    private fun count(statement: Map<String, String>) =
        statement[COUNT]?.toInt() ?: 1

    data class RelationshipAssertionStatement(
        val assertedBy: String,
        val linkedBy: String?,
        val subjectId: String,
        val relationshipType: String,
        val objectId: String,
        val assertedAt: OffsetDateTime,

        /**
         * Optional, the number of assertions expected.
         */
        val count: Int = 1,

        /**
         * Optional, whether the assertions are expected to be current.
         */
        val current: Boolean = true,

        /**
         * Optional, the current state of the assertion
         */
        val state: Boolean = true,
        val ingestedAt: OffsetDateTime?,
    )

    @DataTableType
    fun relationshipAssertionStatement(line: Map<String, String>): RelationshipAssertionStatement =
        RelationshipAssertionStatement(
            line[ASSERTED_BY]!!,
            line[LINKED_BY],
            line[SUBJECT_ID]!!,
            line[RELATIONSHIP_TYPE]!!,
            line[OBJECT_ID]!!,
            assertedAt(line),
            count(line),
            line[CURRENT] == "true",
            // We use != "false" here because state is already assumed to
            // be true when not specified
            line[STATE] != "false",
            dateTimeOrNull(line, INGESTED_AT),
        )

    data class RelationshipStatement(
        val subjectId: String,
        val relationshipType: String,
        val objectId: String,
        val createdTime: OffsetDateTime?,
        val updatedTime: OffsetDateTime?,
        val subjectCollection: String?,
        val subjectType: String?,
        val objectCollection: String?,
        val objectType: String?,
        val assertedBy: String?,
        val linkedBy: String?
    )

    @DataTableType
    fun relationshipStatement(line: Map<String, String>): RelationshipStatement = RelationshipStatement(
        line[SUBJECT_ID]!!,
        line[RELATIONSHIP_TYPE]!!,
        line[OBJECT_ID]!!,
        dateTimeOrNull(line, CREATED_TIME),
        dateTimeOrNull(line, UPDATED_TIME),
        line[SUBJECT_COLLECTION],
        line[SUBJECT_TYPE],
        line[OBJECT_COLLECTION],
        line[OBJECT_TYPE],
        line[ASSERTED_BY],
        line[LINKED_BY]
    )

    data class PropertyAssertionStatement(
        val assertedBy: String,
        val subjectId: String,
        val property: String,
        val value: String,
        val assertedAt: OffsetDateTime,

        /**
         * Optional, the number of assertions expected.
         */
        val count: Int = 1,

        /**
         * Optional, whether the assertions are expected to be current.
         */
        val current: Boolean = true,
    )

    @DataTableType
    fun propertyAssertionStatement(line: Map<String, String>): PropertyAssertionStatement = PropertyAssertionStatement(
        line[ASSERTED_BY]!!,
        line[SUBJECT_ID]!!,
        line[PROPERTY]!!,
        line[VALUE]!!,
        assertedAt(line),
        count(line),
        line[CURRENT] == "true",
    )

    data class Item(
        val identifier: String,
        val stale: Boolean?,
        val internalContentType: ContentType?,
        val publicContentType: ContentType?,
        val title: String?,
        val rendered: Boolean?,
        val numRenders: Int?,
        val updatedAt: OffsetDateTime?,
    )

    data class Member(
        val identifier: String,
        val name: String?,
    )

    private fun getBool(row: Map<String, String>, key: String): Boolean? =
        row[key]?.let {
            row[key]?.toBoolean()
        }

    private fun getInt(row: Map<String, String>, key: String): Int? =
        row[key]?.let {
            row[key]?.toInt()
        }

    @DataTableType
    fun itemTransformer(row: Map<String, String>): Item = Item(
        identifier = row[IDENTIFIER]!!,
        stale = getBool(row, STALE),
        internalContentType = row[INTERNAL_CONTENT_TYPE]?.let {
            ContentType.fromMimeType(row[INTERNAL_CONTENT_TYPE].toString())
        },
        publicContentType = row[PUBLIC_CONTENT_TYPE]?.let {
        ContentType.fromMimeType(row[PUBLIC_CONTENT_TYPE].toString())
        },
        title = row[TITLE],
        rendered = getBool(row, RENDERED),
        numRenders = getInt(row, NUMBER_OF_RENDERS),
        updatedAt = row[UPDATED_AT]?.let {
            LocalDateTime.parse(row[UPDATED_AT])
                .atZone(ZoneOffset.UTC)
                .toOffsetDateTime()
        },
    )

    @DataTableType
    fun sugarOrganizationTransformer(row: Map<String, String>): SugarOrganization = SugarOrganization(
        id = row[SUGAR_INTERNAL_ID]!!,
        name = row[NAME]!!,
        accountType = row[ACCOUNT_TYPE]!!,
        crossrefMemberId = getInt(row, CROSSREF_MEMBER_ID),
        sponsorId = row[SPONSOR_ID],
        inactive = getBool(row, INACTIVE) ?: true,
    )

    @DataTableType
    fun memberTransformer(row: Map<String, String>): Member = Member(
        name = row[NAME],
        identifier = row[IDENTIFIER]!!,
    )
}
