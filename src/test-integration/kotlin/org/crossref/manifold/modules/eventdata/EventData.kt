package org.crossref.manifold.modules.eventdata

import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.SerializationFeature
import com.fasterxml.jackson.databind.util.StdDateFormat
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule
import com.fasterxml.jackson.module.kotlin.KotlinModule
import io.cucumber.java.en.Given
import org.apache.commons.io.IOUtils
import org.crossref.manifold.bulk.BulkIngestionService
import org.crossref.manifold.ingestion.TempFile
import org.crossref.manifold.itemgraph.ItemGraph
import org.crossref.manifold.modules.eventdata.support.EvidenceRecord
import org.crossref.manifold.modules.eventdata.support.convertEvidenceRecordToEnvelopeBatch
import org.crossref.manifold.util.TarGzWriter
import org.junit.jupiter.api.Assertions
import org.springframework.beans.factory.annotation.Autowired

/**
 * The Given step definitions here refer to an existing file in the test resources directory, which we expect to be
 * ready-made for the test. They don't construct the XML file, but they do verify the content to be sure that it says
 * what we expected it to say.
 */
class EventData(
    @Autowired
    var archiveIngester: BulkIngestionService,
    private val tempFile: TempFile,
    val itemGraph: ItemGraph
    ) {

    private val mapper: ObjectMapper = ObjectMapper().registerModule(JavaTimeModule()).disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES)
        .setSerializationInclusion(JsonInclude.Include.NON_EMPTY)
        .disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS)
        .setDateFormat(StdDateFormat().withColonInTimeZone(true))
        .registerModule(KotlinModule())

    fun resource(filename: String) = Thread.currentThread().contextClassLoader
        .getResourceAsStream("json/$filename")!!

    @Given("an Event Data Snapshot file named {string} containing JSON file {string}")
    fun an_event_data_snapshot_file_named_containing(snapshotFilename: String, jsonFilename: String) {
        // Just create a tar file with a single entry.
        TarGzWriter(tempFile[snapshotFilename]).use {
            it.add(jsonFilename) { outputStream ->
                IOUtils.copy(resource(jsonFilename), outputStream)
            }
        }
    }

    @Given("the Event Data snapshot file {string} has been converted and ingested")
    fun the_datacite_snapshot_file_is_ingested(
        snapshotFilename: String
    ) {
        val bulkFilename = "datacite-bulk.tar.gz"
        DataciteSnapshotConverter.run(
            tempFile[snapshotFilename].absolutePath,
            tempFile[bulkFilename].absolutePath)
        archiveIngester.ingestFiles(listOf(tempFile[bulkFilename]))
    }

    @Given( "the evidence record {string} contains {int} matches to {string}")
    fun evidence_records_contains_matches(evFilename: String, nMatches: Int, doi: String) {
        val evidenceRecord = resource(evFilename).let {
            mapper.readValue(it, EvidenceRecord::class.java)
        }

        val count = evidenceRecord.pages
            .flatMap { it.actions }
            .flatMap { it.matches }
            .filter { it.match.isNotBlank() }
            .count{ it.match == doi }

        Assertions.assertEquals(nMatches, count)
    }

    @Given("the evidence record {string} has been converted and ingested")
    fun evidence_record_has_been_converted_and_ingested(evFilename: String){
        val evidenceRecord = resource(evFilename).let {
            mapper.readValue(it, EvidenceRecord::class.java)
        }

        val converted = convertEvidenceRecordToEnvelopeBatch(
            evFilename,
            evidenceRecord
        )

        if(converted.isNotEmpty()){
            converted.forEach {
                itemGraph.ingest(listOf(it))
            }
        }
    }
}
