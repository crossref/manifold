package org.crossref.manifold

import com.ninjasquad.springmockk.MockkBean
import io.cucumber.spring.CucumberContextConfiguration
import org.crossref.manifold.itemgraph.Configuration.ITEM_TREE_UPDATE
import org.crossref.manifold.itemgraph.Configuration.MAX_RETURNED_ROWS
import org.crossref.manifold.lookup.DataCiteTypeLookupService.Companion.DATACITE_TYPE_LOOKUP_CRON
import org.crossref.manifold.lookup.RALookupService.Companion.RA_LOOKUP_NEW_CRON
import org.crossref.manifold.matching.Configuration.MATCHING
import org.crossref.manifold.matching.Configuration.PARTITIONS
import org.crossref.manifold.modules.crossreforganizations.Module.Companion.CROSSREF_ORGANIZATIONS
import org.crossref.manifold.modules.crossreforganizations.Module.Companion.SUGAR_IMPORT_CRON
import org.crossref.manifold.modules.unixml.Module.Companion.UNIXML
import org.crossref.manifold.relationship.Constants.API
import org.crossref.manifold.relationship.Constants.RELATIONSHIPS
import org.crossref.manifold.rendering.Configuration.ITEMS_API
import org.crossref.manifold.rendering.Configuration.RENDERER_ENABLED
import org.crossref.manifold.rendering.Configuration.RENDERING
import org.crossref.manifold.rendering.Configuration.RENDER_QUEUE_PROCESSOR_ENABLED
import org.crossref.manifold.rendering.Configuration.RENDER_S3_BUCKET
import org.crossref.manifold.rendering.Configuration.WORKS_API
import org.crossref.manifold.util.Constants.ENABLED
import org.crossref.manifold.util.Constants.INITIAL_DELAY
import org.crossref.manifold.util.Constants.PRODUCE_ITEM_TREE_JSON
import org.crossref.messaging.aws.autoconfig.S3AutoConfig.Companion.S3_ENABLED
import org.crossref.messaging.aws.autoconfig.S3AutoConfig.Companion.S3_NOTIFICATION_QUEUE
import org.crossref.messaging.aws.autoconfig.SqsAutoConfig.Companion.SQS_ENABLED
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.TestPropertySource
import java.time.InstantSource

@CucumberContextConfiguration
@SpringBootTest
@AutoConfigureMockMvc
@TestPropertySource(
    properties = [
        "$MAX_RETURNED_ROWS=5",
        "$UNIXML.$S3_NOTIFICATION_QUEUE=manifold-mainq",
        "$ITEM_TREE_UPDATE.$ENABLED=true",
        "$ITEM_TREE_UPDATE.$INITIAL_DELAY=${Long.MAX_VALUE}",
        "$ITEM_TREE_UPDATE.$PRODUCE_ITEM_TREE_JSON=true",
        "$MATCHING.$ENABLED=true",
        "$MATCHING.$PARTITIONS=basic,ror",
        "$MATCHING.$INITIAL_DELAY=${Long.MAX_VALUE}",
        "$RENDERING.$RENDER_QUEUE_PROCESSOR_ENABLED=true",
        "$RENDERING.$INITIAL_DELAY=${Long.MAX_VALUE}",
        "$RENDERING.$RENDERER_ENABLED=true",
        "$RENDERING.$RENDER_S3_BUCKET=render-bucket",
        "$S3_ENABLED=true",
        "$SQS_ENABLED=true",
        "$RELATIONSHIPS.$API=true",
        "$RENDERING.$WORKS_API=true",
        "$RENDERING.$ITEMS_API=true",
        "$RA_LOOKUP_NEW_CRON=-",
        "$DATACITE_TYPE_LOOKUP_CRON=-",
        "$CROSSREF_ORGANIZATIONS.$SUGAR_IMPORT_CRON=-"
    ]
)
class CucumberTestContextConfiguration {
    // Mock the InstantSource bean, so we can inject preset ingestion times.
    @MockkBean
    private lateinit var instantSource: InstantSource
}
