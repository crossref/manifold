Feature: Rendered items feed

  Scenario: Content type and links should be correct
    When the feed is retrieved from the "/beta/items/feed.xml" endpoint
    Then the Content-Type header should be "application/xml"
    And the feed has a "self" link href prefixed with the feed endpoint "/beta/items/feed.xml"
    And the feed has a "first" link href prefixed with the feed endpoint "/beta/items/feed.xml"

  Scenario: Feeds returns recent items in expected formats

    Given UniXSD file "journal-article-10.5555-12345678.xml" registers Article DOI "10.5555/12345678"
    And UniXSD file "journal-article-10.5555-12345678.xml" has been ingested
    And the item tree match queue has emptied
    And the item tree update queue has emptied
    And the render queue has emptied

    And the render for "https://doi.org/10.5555/12345678" in "application/citeproc+json" has become available
    And the render for "https://doi.org/10.5555/12345678" in "application/vnd.crossref.unixsd+xml" has become available
    And UniXSD file "journal-article-10.5555-87654321.xml" registers Article DOI "10.5555/87654321"
    And UniXSD file "journal-article-10.5555-87654321.xml" has been ingested
    And the item tree match queue has emptied
    And the item tree update queue has emptied
    And the render queue has emptied

    And the render for "https://doi.org/10.5555/87654321" in "application/citeproc+json" has become available
    And the render for "https://doi.org/10.5555/87654321" in "application/vnd.crossref.unixsd+xml" has become available

    When all feed items are retrieved from the "/beta/items/feed.xml" endpoint, following cursors in pages of 2
    Then an entry has alternate link to the un-versioned item endpoint for "https://doi.org/10.5555/87654321" with type "application/citeproc+json"
    And an entry has alternate link to the un-versioned item endpoint for "https://doi.org/10.5555/87654321" with type "application/vnd.crossref.unixsd+xml"
    And an entry has alternate link to the un-versioned item endpoint for "https://doi.org/10.5555/12345678" with type "application/citeproc+json"
    And an entry has alternate link to the un-versioned item endpoint for "https://doi.org/10.5555/12345678" with type "application/vnd.crossref.unixsd+xml"

    And an entry has related link to the versioned, typed item endpoint "https://doi.org/10.5555/87654321" with type "application/citeproc+json"
    And an entry has related link to the versioned, typed item endpoint "https://doi.org/10.5555/87654321" with type "application/vnd.crossref.unixsd+xml"
    And an entry has related link to the versioned, typed item endpoint "https://doi.org/10.5555/12345678" with type "application/citeproc+json"
    And an entry has related link to the versioned, typed item endpoint "https://doi.org/10.5555/12345678" with type "application/vnd.crossref.unixsd+xml"

    And an entry has enclosure link to the s3 path for "https://doi.org/10.5555/87654321" with type "application/citeproc+json"
    And an entry has enclosure link to the s3 path for "https://doi.org/10.5555/87654321" with type "application/vnd.crossref.unixsd+xml"
    And an entry has enclosure link to the s3 path for "https://doi.org/10.5555/12345678" with type "application/citeproc+json"
    And an entry has enclosure link to the s3 path for "https://doi.org/10.5555/12345678" with type "application/vnd.crossref.unixsd+xml"
