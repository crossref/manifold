Feature: Registration agency filters in Relationship endpoint

  We want to be able to filter results based on the registration agency of the subject and the object of a relationship.

  The filters will be:

  - subject.registration-agency
  - object.registration-agency

  If none of the above filters is provided then no registration agency filtering should be applied.

  Scenario: Without any registration agency filters all results are returned

    Given UniXSD file "relationships/article-cites-everything.xml" has been ingested
    Given UniXSD file "relationships/cited-proceedings.xml" has been ingested
    Given UniXSD file "relationships/book-with-chapters.xml" has been ingested
    Given UniXSD file "relationships/grants.xml" has been ingested
    Given UniXSD file "relationships/sa-component.xml" has been ingested
    And the registration agency lookup has run
    And the item tree match queue has emptied
    And the item tree update queue has emptied
    And the relationships have been synced

    When all relationships are requested
    Then the following relationships are returned from the item graph
      | Subject ID                       | Relationship Type | Object ID                                              | Asserted By                      | Linked By                        |
      | https://doi.org/10.5555/12345678 | cites             | https://doi.org/10.1109/elinsl.1996.549297             | https://id.crossref.org/org/7822 | https://id.crossref.org/org/7822 |
      | https://doi.org/10.5555/12345678 | cites             | https://doi.org/10.50505/book                          | https://id.crossref.org/org/7822 | https://id.crossref.org/org/7822 |
      | https://doi.org/10.5555/12345678 | cites             | https://doi.org/10.46936/cpcy.proj.2019.50733/60006578 | https://id.crossref.org/org/7822 | https://id.crossref.org/org/7822 |
      | https://doi.org/10.5555/12345678 | cites             | https://doi.org/10.5555/demo_1.1                       | https://id.crossref.org/org/7822 | https://id.crossref.org/org/7822 |
      | https://doi.org/10.5555/12345678 | steward           | https://id.crossref.org/org/7822                       | https://ror.org/02twcfp32        | https://ror.org/02twcfp32       |

  Scenario: Filter "subject.registration-agency" returns results
    Given "Crossref" is the RA for DOI prefix "10.5555"
    Given UniXSD file "relationships/article-cites-everything.xml" has been ingested
    Given UniXSD file "relationships/cited-proceedings.xml" has been ingested
    Given UniXSD file "relationships/book-with-chapters.xml" has been ingested
    Given UniXSD file "relationships/grants.xml" has been ingested
    Given UniXSD file "relationships/sa-component.xml" has been ingested
    And the registration agency lookup has run
    And the item tree match queue has emptied
    And the item tree update queue has emptied
    And the relationships have been synced
    When relationships for subject registration agency "Crossref" are requested
    Then the following relationships are returned from the item graph
      | Subject ID                       | Relationship Type | Object ID                                              | Asserted By                      | Linked By                        |
      | https://doi.org/10.5555/12345678 | cites             | https://doi.org/10.1109/elinsl.1996.549297             | https://id.crossref.org/org/7822 | https://id.crossref.org/org/7822 |
      | https://doi.org/10.5555/12345678 | cites             | https://doi.org/10.50505/book                          | https://id.crossref.org/org/7822 | https://id.crossref.org/org/7822 |
      | https://doi.org/10.5555/12345678 | cites             | https://doi.org/10.46936/cpcy.proj.2019.50733/60006578 | https://id.crossref.org/org/7822 | https://id.crossref.org/org/7822 |
      | https://doi.org/10.5555/12345678 | cites             | https://doi.org/10.5555/demo_1.1                       | https://id.crossref.org/org/7822 | https://id.crossref.org/org/7822 |
      | https://doi.org/10.5555/12345678 | steward           | https://id.crossref.org/org/7822                       | https://ror.org/02twcfp32        | https://ror.org/02twcfp32       |

  Scenario: Filter "subject.registration-agency" does not return any results
    Given UniXSD file "relationships/article-cites-everything.xml" has been ingested
    Given UniXSD file "relationships/cited-proceedings.xml" has been ingested
    Given UniXSD file "relationships/book-with-chapters.xml" has been ingested
    Given UniXSD file "relationships/grants.xml" has been ingested
    Given UniXSD file "relationships/sa-component.xml" has been ingested
    And the registration agency lookup has run
    And the item tree match queue has emptied
    And the item tree update queue has emptied
    And the relationships have been synced
    When relationships for subject registration agency "DataCite" are requested
    Then 0 relationships are returned from the item graph

  Scenario: Filter "object.registration-agency" returns results
    Given "Crossref" is the RA for DOI prefix "10.1109"
    Given "Crossref" is the RA for DOI prefix "10.50505"
    Given "Crossref" is the RA for DOI prefix "10.46936"
    Given "Crossref" is the RA for DOI prefix "10.5555"
    Given UniXSD file "relationships/article-cites-everything.xml" has been ingested
    Given UniXSD file "relationships/cited-proceedings.xml" has been ingested
    Given UniXSD file "relationships/book-with-chapters.xml" has been ingested
    Given UniXSD file "relationships/grants.xml" has been ingested
    Given UniXSD file "relationships/sa-component.xml" has been ingested
    And the registration agency lookup has run
    And the item tree match queue has emptied
    And the item tree update queue has emptied
    And the relationships have been synced
    When relationships for object registration agency "Crossref" are requested
    Then the following relationships are returned from the item graph
      | Subject ID                       | Relationship Type | Object ID                                              | Asserted By                      | Linked By                        |
      | https://doi.org/10.5555/12345678 | cites             | https://doi.org/10.1109/elinsl.1996.549297             | https://id.crossref.org/org/7822 | https://id.crossref.org/org/7822 |
      | https://doi.org/10.5555/12345678 | cites             | https://doi.org/10.50505/book                          | https://id.crossref.org/org/7822 | https://id.crossref.org/org/7822 |
      | https://doi.org/10.5555/12345678 | cites             | https://doi.org/10.46936/cpcy.proj.2019.50733/60006578 | https://id.crossref.org/org/7822 | https://id.crossref.org/org/7822 |
      | https://doi.org/10.5555/12345678 | cites             | https://doi.org/10.5555/demo_1.1                       | https://id.crossref.org/org/7822 | https://id.crossref.org/org/7822 |

  Scenario: Filter "object.registration-agency" does not return any results
    Given UniXSD file "relationships/article-cites-everything.xml" has been ingested
    Given UniXSD file "relationships/cited-proceedings.xml" has been ingested
    Given UniXSD file "relationships/book-with-chapters.xml" has been ingested
    Given UniXSD file "relationships/grants.xml" has been ingested
    Given UniXSD file "relationships/sa-component.xml" has been ingested
    And the registration agency lookup has run
    And the item tree match queue has emptied
    And the item tree update queue has emptied
    And the relationships have been synced
    When relationships for object registration agency "DataCite" are requested
    Then 0 relationships are returned from the item graph