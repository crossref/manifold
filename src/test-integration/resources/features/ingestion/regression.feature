Feature: Identifier regression cases

  Scenario: DOI with special characters

  This file failed processing because it has a weird sequence "%P" in it. This test checks that it was ingested without
  and error at least one relationship ('steward') was inserted.

  The identifier we look for is the DOI correctly encoded in a URI (compare "6%P" vs "6%25p").

    Given an empty Item Graph database
    And UniXSD file "ingestion/0ed2aaa3c085a699b658e7d8bd117157960e5e51.xml" registers Component DOI "10.1107/S0108768105034014/bk501910.6%Psup3.hkl"
    When UniXSD file "ingestion/0ed2aaa3c085a699b658e7d8bd117157960e5e51.xml" is ingested
    And the item tree match queue has emptied
    And the relationships have been synced
    Then the following relationship statements are found in the item graph
      | Count | Asserted By               | Subject ID                                                       | Relationship Type | Object ID                       |
      # Query using correct URI encoding.
      | 1     | https://ror.org/02twcfp32 | https://doi.org/10.1107/s0108768105034014/bk501910.6%25psup3.hkl | steward           | https://id.crossref.org/org/329 |


  Scenario: DOI without suffix

  The file failed processing because the submitted value '10.1120/' is not a valid DOI
  and does not produce an identifier (when we expect one). This test checks that
  that the conversion does not fail when this happens but instead ignores the missing identifier.

    The three expected relationships returned are 'resource-resolution', 'steward', 'container', and 'prefix'.

    Given an empty Item Graph database
    And UniXSD file "conversion/f1f5fdfd846dacf1687416b2456bbf800a24e86d.xml" registers Journal DOI "10.1120/"
    And UniXSD file "conversion/f1f5fdfd846dacf1687416b2456bbf800a24e86d.xml" is ingested
    And the item tree match queue has emptied
    And the relationships have been synced
    When all relationships are requested
    Then 4 relationships are returned from the item graph