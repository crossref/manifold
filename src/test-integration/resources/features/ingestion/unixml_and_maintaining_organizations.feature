Feature: UniXML and Maintaining Organizations

  UniXML is the data format that Crossref Members use to send us metadata, and the format we expose in our "XML APIs". It is currently our main source of truth as asserted by our members. It is also converted by Cayenne parser into Item Trees for ingestion into the /works endpoint of the REST API.

  The UniXML is wrapped in UniXSD, which is a container that includes extra information from Content System. For the purpose of this feature, the only part of this UniXSD wrapper we're interested in is the "crm-item" element with the name "member-id". This "member-id" field indicates which Crossref Member we believe the given UniXSD record is associated with. As the "crm-items" data comes from a different source, and has different provenance to the UniXML it wraps, it can change independent of the UniXML. Note that a given UniXML record can register and represent metadata about multiple DOIs (e.g. journal titles, articles, components, etc).

  UniXML is ingested into the Item Graph in two contexts. Initially we do a bulk load of a snapshot of the whole corpus. After that, Content System sends XML files on a continual basis whenever then are updated. These two timescales involve different contexts:

  - The initial bulk load is done from a snapshot that represents what we know at a certain point in time, including the member ID associated with the DOIs.
  - The continual load is done thereafter. In this time-frame, the XML content can change independent of the member IDs associated with them.

  We can assume that the member crm-item information is correctly represented at the time of snapshot creation. We also know that, due to the "Work to Member Relationships" feature in Content System, the XML is always re-pushed whenever this connection changes.

  Scenario: Ingestion asserts Items and Maintainer

    Given UniXSD file "silly-string.xml" registers Journal DOI "10.5555/1234567890"
    And UniXSD file "silly-string.xml" registers Article DOI "10.5555/12345678"
    And UniXSD file "silly-string.xml" has crm-item with Member ID "7822"
    When UniXSD file "silly-string.xml" is ingested
    And the item tree match queue has emptied
    And the relationships have been synced
    Then the following relationship statements are found in the item graph
      | Count | Asserted By                      | Subject ID                         | Relationship Type | Object ID                        |
      | 1     | https://ror.org/02twcfp32        | https://doi.org/10.5555/12345678   | steward           | https://id.crossref.org/org/7822 |
      # Steward should not be asserted by the member.
      | 0     | https://id.crossref.org/org/7822 | https://doi.org/10.5555/12345678   | steward           | https://id.crossref.org/org/7822 |

  Scenario: Ownership can change

  If the member ID changes, the new stewardship is asserted by Crossref, and the old maintainer is marked as historical and no longer current.

  We check that the DOI has the new current stewardship, that it no longer has the old one. We also verify that the previous maintainership is still present as non-current in the historical record.

  This is applied to both DOIs registered in the XML file.

    Given the following relationship assertions in the item graph
      | Asserted By               | Subject ID                         | Relationship Type | Object ID                        |
      | https://ror.org/02twcfp32 | https://doi.org/10.5555/1234567890 | steward           | https://id.crossref.org/org/7822 |
      | https://ror.org/02twcfp32 | https://doi.org/10.5555/12345678   | steward           | https://id.crossref.org/org/7822 |
    And the item tree match queue has emptied
    Given UniXSD file "silly-string-moved.xml" registers Journal DOI "10.5555/1234567890"
    And UniXSD file "silly-string-moved.xml" registers Article DOI "10.5555/12345678"
    And UniXSD file "silly-string-moved.xml" has crm-item with Member ID "99999"
    When UniXSD file "silly-string-moved.xml" is ingested
    And the item tree match queue has emptied
    And the relationships have been synced
    Then the following relationship statements are found in the item graph
      | Count | Asserted By               | Subject ID                         | Relationship Type | Object ID                         |
      | 1     | https://ror.org/02twcfp32 | https://doi.org/10.5555/12345678   | steward           | https://id.crossref.org/org/99999 |
      | 0     | https://ror.org/02twcfp32 | https://doi.org/10.5555/12345678   | steward           | https://id.crossref.org/org/7822  |

    Scenario: Steward only for ingested DOI - Book Content

      The Crossref schema allows for many DOIs to be included in one XML file. For example, Books may have both a
      Book Title and a Book Chapter present in the XML. This is reflected in the Item Tree.

      Content System produces and sends a UniXSD file for each known DOI. The UniXSD format includes the <query><doi>
      tag to denote which DOI it's returning. Therefore, if there are multiple DOIs in one XML file, it will be sent
      multiple times. The file will be similar (or identical), except for the <query><doi> tag.

      We assert stewardship based on DOI registrations found in the item tree. If we simply look for DOIs in the Item
      Tree, we would end up re-asserting the stewardship of the Book DOI twice.

      So, given this book content DOI, we should see a stewardship assertion about the Chapter, 10.1002/9780470015902.a0028438
      We should not see a stewardship assertion about the Book 10.1002/047001590X

      Given an empty Item Graph database
      And UniXSD file "ingestion/book_content.xml" query result DOI is "10.1002/9780470015902.a0028438"
      When UniXSD file "ingestion/book_content.xml" is ingested
      And the item tree match queue has emptied
      And the relationships have been synced
      Then the following relationship statements are found in the item graph
        | Count | Asserted By               | Subject ID                                       | Relationship Type | Object ID                       |
        | 0     | https://ror.org/02twcfp32 | https://doi.org/10.1002/047001590x               | steward           | https://id.crossref.org/org/311 |
        | 1     | https://ror.org/02twcfp32 | https://doi.org/10.1002/9780470015902.a0028438   | steward           | https://id.crossref.org/org/311 |

    Scenario: Steward only for ingested DOI - Book Title

      Building on the previous scenario, when the book title is ingested, the stewardship should be asserted for that DOI.

      Given an empty Item Graph database
      # Note that the DOI may appear in any case in the <query> tag, but DOIs are lower-cased when ingested.
      And UniXSD file "ingestion/book_title.xml" query result DOI is "10.1002/047001590X"
      When UniXSD file "ingestion/book_title.xml" is ingested
      And the item tree match queue has emptied
      And the relationships have been synced
      Then the following relationship statements are found in the item graph
        | Count | Asserted By               | Subject ID                                       | Relationship Type | Object ID                       |
        | 1     | https://ror.org/02twcfp32 | https://doi.org/10.1002/047001590x               | steward           | https://id.crossref.org/org/311 |
