Feature: Organization information is retrieved from Sugar and put into the item graph

  Scenario: Organization information is accurately copied from Sugar to the item graph.
    Given an empty Item Graph database
    And the following organizations in Sugar
      | Sugar Internal Id | Name                                | Account Type          | Crossref Member Id | Sponsor Id | Inactive |
      | 1                 | Federal Reserve Bank of Dallas      | Sponsored Publisher   | 10241              |            | false   |
      | 2                 | Federal Reserve Bank of Kansas City | Sponsoring Publishers | 7902               |            | false   |
    When the Sugar ingestion is run
    And the item tree match queue has emptied
    And the item tree update queue has emptied
    Then the Item Graph contains members
      | Name                                | Identifier                        |
      | Federal Reserve Bank of Dallas      | https://id.crossref.org/org/10241 |
      | Federal Reserve Bank of Kansas City | https://id.crossref.org/org/7902  |

  Scenario: Organizations are accurately marked as Crossref members, or not
    Given an empty Item Graph database
    And the following organizations in Sugar
      | Name                                           | Crossref Member Id | Account Type          | Sugar Internal Id                    | Inactive |
      | Universidad Pedagogica Experimental Libertador | 35135              | Member Publisher      | 0a557dcc-ca34-11ec-b609-02c180ed988e | false    |
      | Sekolah Tinggi Ilmu Kesehatan Abdurahman       | 33704              | REPRESENTED_MEMBER    | 4c126d4a-6ef6-11ec-a143-06b3e72edb9c | false    |
      | Médecine et Hygiène                            | 17667              | Sponsoring Publishers | 8640a360-b40f-11e8-b397-02ef9e9f3eb9 | false    |
      | Relawan Jurnal Indonesia                       | 15117              | Sponsoring Entities   | d979586a-c487-11e7-84a5-06e14280fdab | false    |
      | Federal Reserve Bank of Dallas                 | 10241              | Sponsored Publisher   | 1ee92fee-f92f-11e6-bb87-0230eed5e4fb | false    |
      | Sapiens publications SDN BHD                   | 22574              | RA_AFFILIATE          | 8276234c-e783-11e9-a3d3-06b3e72edb9c | false    |
      | Janeway                                        | 38171              | AGENT                 | ba7c8ce0-b6ce-11ed-b784-06a99076beaf | false    |
      | ResearchFilter                                 | 21926              | Affiliates            | 435dc456-b6b1-11e9-948d-06b3e72edb9c | false    |
    When the Sugar ingestion is run
    And the item tree match queue has emptied
    And the item tree update queue has emptied
    Then the Item Graph contains members
      | Name                                           | Identifier                        |
      | Universidad Pedagogica Experimental Libertador | https://id.crossref.org/org/35135 |
      | Sekolah Tinggi Ilmu Kesehatan Abdurahman       | https://id.crossref.org/org/33704 |
      | Médecine et Hygiène                            | https://id.crossref.org/org/17667 |
    Then the following relationship statements are found in the item graph
      | Count | Current | Asserted By               | Subject ID                        | Relationship Type | Object ID                 |
      | 1     | true    | https://ror.org/02twcfp32 | https://id.crossref.org/org/35135 | member-of         | https://ror.org/02twcfp32 |
      | 1     | true    | https://ror.org/02twcfp32 | https://id.crossref.org/org/33704 | member-of         | https://ror.org/02twcfp32 |
      | 1     | true    | https://ror.org/02twcfp32 | https://id.crossref.org/org/17667 | member-of         | https://ror.org/02twcfp32 |
      | 0     | true    | https://ror.org/02twcfp32 | https://id.crossref.org/org/15117 | member-of         | https://ror.org/02twcfp32 |
      | 0     | true    | https://ror.org/02twcfp32 | https://id.crossref.org/org/10241 | member-of         | https://ror.org/02twcfp32 |
      | 0     | true    | https://ror.org/02twcfp32 | https://id.crossref.org/org/22574 | member-of         | https://ror.org/02twcfp32 |
      | 0     | true    | https://ror.org/02twcfp32 | https://id.crossref.org/org/38171 | member-of         | https://ror.org/02twcfp32 |
      | 0     | true    | https://ror.org/02twcfp32 | https://id.crossref.org/org/21926 | member-of         | https://ror.org/02twcfp32 |

  Scenario: Sponsoring Entities - REPRESENTED_MEMBER
    Given an empty Item Graph database
    And the following organizations in Sugar
      | Sugar Internal Id                     | Name                                     | Crossref Member Id | Account Type        | Sponsor Id                           | Inactive |
      | 4c126d4a-6ef6-11ec-a143-06b3e72edb9c  | Sekolah Tinggi Ilmu Kesehatan Abdurahman | 33704              | REPRESENTED_MEMBER  | d979586a-c487-11e7-84a5-06e14280fdab | false   |
      | d979586a-c487-11e7-84a5-06e14280fdab  | Relawan Jurnal Indonesia                 | 15117              | Sponsoring Entities |                                      | false   |
    When the Sugar ingestion is run
    And the item tree match queue has emptied
    Then the following relationship statements are found in the item graph
      | Count | Current | Asserted By               | Subject ID                                 | Relationship Type | Object ID                                  |
      | 1     | true    | https://ror.org/02twcfp32 | https://id.crossref.org/org/33704 | sponsor           | https://id.crossref.org/org/15117 |

  Scenario: Sponsoring Publisher - Sponsored Publisher
    Given an empty Item Graph database
    And the following organizations in Sugar
      | Sugar Internal Id                    | Name                                | Crossref Member Id | Account Type          | Sponsor Id                           | Inactive |
      | 1ee92fee-f92f-11e6-bb87-0230eed5e4fb | Federal Reserve Bank of Dallas      | 10241              | Sponsored Publisher   | ec7958d8-5299-150d-a27e-55ad26cd8d98 | false   |
      | ec7958d8-5299-150d-a27e-55ad26cd8d98 | Federal Reserve Bank of Kansas City | 7902               | Sponsoring Publishers |                                      | false   |
    When the Sugar ingestion is run
    And the item tree match queue has emptied
    Then the following relationship statements are found in the item graph
      | Count | Current | Asserted By               | Subject ID                                 | Relationship Type | Object ID                                 |
      | 1     | true    | https://ror.org/02twcfp32 | https://id.crossref.org/org/10241 | sponsor           | https://id.crossref.org/org/7902 |

  Scenario: Organization import can cope with empty Crossref Member Id
    Given an empty Item Graph database
    And the following organizations in Sugar
      | Sugar Internal Id | Name                                | Account Type          | Crossref Member Id | Sponsor Id | Inactive |
      | 1                 | Federal Reserve Bank of Dallas      | Sponsored Publisher   | 10241              |            | false   |
      | 2                 | Federal Reserve Bank of Kansas City | Sponsoring Publishers |                    |            | false   |
    When the Sugar ingestion is run
    And the item tree match queue has emptied
    And the item tree update queue has emptied

    Then the Item Graph contains members
      | Name                           | Identifier                                 |
      | Federal Reserve Bank of Dallas | https://id.crossref.org/org/10241 |

  Scenario: Duplicate Organizations IDs should not be ingested
    Given an empty Item Graph database
    And the following organizations in Sugar
      | Sugar Internal Id | Name                           | Account Type        | Crossref Member Id | Sponsor Id | Inactive |
      | 1                 | Federal Reserve Bank of Dllas  | Sponsored Publisher | 10241              |            | false   |
      | 2                 | Federal Reserve Bank of Dallas | Sponsored Publisher | 10241              |            | false   |
    When the Sugar ingestion is run
    Then the Item Graph does not contain members
      | Identifier                        |
      | https://id.crossref.org/org/10241 |

  Scenario: Duplicate Sugar IDs should not be ingested.
    Given an empty Item Graph database
    And the following organizations in Sugar
      | Name                                           | Crossref Member Id | Account Type          | Sugar Internal Id                    | Inactive |
      | Universidad Pedagogica Experimental Libertador | 35135              | Member Publisher      | 0a557dcc-ca34-11ec-b609-02c180ed988e | false   |
      | Sekolah Tinggi Ilmu Kesehatan Abdurahman       | 33704              | REPRESENTED_MEMBER    | 0a557dcc-ca34-11ec-b609-02c180ed988e | false   |
    When the Sugar ingestion is run
    Then the Item Graph does not contain members
      | Identifier                        |
      | https://id.crossref.org/org/35135 |
      | https://id.crossref.org/org/33704 |

  Scenario: Active and Inactive Organizations are accurately marked as Crossref members, or not
    Given an empty Item Graph database
    And the following organizations in Sugar
      | Name                                           | Crossref Member Id | Account Type          | Sugar Internal Id                    | Inactive |
      | Universidad Pedagogica Experimental Libertador | 35135              | Member Publisher      | 0a557dcc-ca34-11ec-b609-02c180ed988e | false    |
      | Sekolah Tinggi Ilmu Kesehatan Abdurahman       | 33704              | REPRESENTED_MEMBER    | 4c126d4a-6ef6-11ec-a143-06b3e72edb9c | true     |
    When the Sugar ingestion is run
    And the item tree match queue has emptied
    And the item tree update queue has emptied

    Then the Item Graph contains members
      | Name                                           | Identifier                        |
      | Universidad Pedagogica Experimental Libertador | https://id.crossref.org/org/35135 |
      | Sekolah Tinggi Ilmu Kesehatan Abdurahman       | https://id.crossref.org/org/33704 |
    Then the following relationship statements are found in the item graph
      | Count | Asserted By               | Subject ID                        | Relationship Type | Object ID                 |
      | 1     | https://ror.org/02twcfp32 | https://id.crossref.org/org/35135 | member-of         | https://ror.org/02twcfp32 |
      | 0     | https://ror.org/02twcfp32 | https://id.crossref.org/org/33704 | member-of         | https://ror.org/02twcfp32 |

  Scenario: Organizations that become inactive are not marked as Crossref members
    Given an empty Item Graph database
    And the following organizations in Sugar
      | Name                                           | Crossref Member Id | Account Type     | Sugar Internal Id                    |
      | Universidad Pedagogica Experimental Libertador | 35135              | Member Publisher | 0a557dcc-ca34-11ec-b609-02c180ed988e | 
    And the Sugar ingestion has run
    And the following organizations become inactive in Sugar
      | Name                                           | Crossref Member Id | Account Type     | Sugar Internal Id                    |
      | Universidad Pedagogica Experimental Libertador | 35135              | Member Publisher | 0a557dcc-ca34-11ec-b609-02c180ed988e | 
    When the Sugar ingestion is run
    Then the following relationship statements are found in the item graph
      | Count | Asserted By               | Subject ID                        | Relationship Type | Object ID                 |
      | 0     | https://ror.org/02twcfp32 | https://id.crossref.org/org/35135 | member-of         | https://ror.org/02twcfp32 |

  Scenario: Organizations are rendered in org JSON
    Given an empty Item Graph database
    And no Items have been rendered yet
    And the following organizations in Sugar
      | Sugar Internal Id | Name                                | Account Type          | Crossref Member Id | Sponsor Id | Inactive |
      | 1                 | Federal Reserve Bank of Dallas      | Sponsored Publisher   | 10241              |            | false   |
      | 2                 | Federal Reserve Bank of Kansas City | Sponsoring Publishers | 7902               |            | false   |
    When the Sugar ingestion is run
    And the item tree match queue has emptied
    And the item tree update queue has emptied
    And the render queue has emptied

    Then item "https://id.crossref.org/org/10241" is rendered in "application/vnd.crossref.org+json" format
    And item "https://id.crossref.org/org/7902" is rendered in "application/vnd.crossref.org+json" format

  Scenario: Member Organizations are rendered in member JSON
    Given an empty Item Graph database
    And no Items have been rendered yet
    And the following organizations in Sugar
      | Sugar Internal Id | Name                                | Account Type          | Crossref Member Id | Sponsor Id | Inactive |
      | 1                 | Federal Reserve Bank of Kansas City | Sponsoring Publishers | 7902               |            | false   |
    When the Sugar ingestion is run
    And the item tree match queue has emptied
    And the item tree update queue has emptied
    And the render queue has emptied
    Then item "https://id.crossref.org/org/7902" is rendered in "application/vnd.crossref.member+json" format

  # IGNORED due to citeproc-json rendering bug.
#  Scenario: Organizational name change is reflected in citeproc+json publisher
#    Given an empty Item Graph database
#    And UniXSD file "test-publisher-name-change-10.5555-12345678.xml" registers Journal DOI "10.5555/1234567890"
#    And UniXSD file "test-publisher-name-change-10.5555-12345678.xml" has crm-item with Member ID "1000"
#    And UniXSD file "test-publisher-name-change-10.5555-12345678.xml" has crm-item with Publisher Name "Old Name"
#    And UniXSD file "test-publisher-name-change-10.5555-12345678.xml" has been ingested
#    And the following organizations in Sugar
#      | Name     | Crossref Member Id | Account Type     | Sugar Internal Id                    |
#      | New Name | 1000               | Member Publisher | 0a557dcc-ca34-11ec-b609-02c180ed988e |
#    When the Sugar ingestion is run
#    And the item tree match queue has emptied
#    And the item tree update queue has emptied
#    And the render queue has emptied
#    Then "https://doi.org/10.5555/12345678" in "application/citeproc+json" should have member name "New Name"

