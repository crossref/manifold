#!/bin/bash

set -o errexit   # abort on nonzero exit status
set -o nounset   # abort on unbound variable
set -o pipefail  # don't hide errors within pipes

# Define default values for environment variables
DEFAULT_AWS_ACCESS_KEY_ID="test"
DEFAULT_AWS_SECRET_ACCESS_KEY="test"
DEFAULT_AWS_REGION="us-east-1"
DEFAULT_AWS_ENDPOINT="http://localhost:4566"
DEFAULT_DB_URI_RW="jdbc:postgresql://localhost:5432/manifold?user=manifold&password=manifold"
DEFAULT_DB_URI_RO="jdbc:postgresql://localhost:5432/manifold?user=manifold&password=manifold"

# Use existing environment variables or set to defaults
AWS_ACCESS_KEY_ID="${AWS_ACCESS_KEY_ID:-$DEFAULT_AWS_ACCESS_KEY_ID}"
AWS_SECRET_ACCESS_KEY="${AWS_SECRET_ACCESS_KEY:-$DEFAULT_AWS_SECRET_ACCESS_KEY}"
AWS_REGION="${AWS_REGION:-$DEFAULT_AWS_REGION}"
AWS_ENDPOINT_OVERRIDE="${AWS_ENDPOINT_OVERRIDE:-$DEFAULT_AWS_ENDPOINT}"
DB_URI_RW="${DB_URI_RW:-$DEFAULT_DB_URI_RW}"
DB_URI_RO="${DB_URI_RO:-$DEFAULT_DB_URI_RO}"

# Function to run AWS CLI command and handle output
run_aws_command() {
    # The last argument is the success message
    local success_message=${*: -1}
    # All but the last argument are the command and its arguments
    local command=("${@:1:$#-1}")

    echo "Executing: ${command[*]}"
    local result
    result=$( "${command[@]}" 2>&1 )  # Capture both stdout and stderr

    # shellcheck disable=SC2181
    if [ $? -eq 0 ]; then
        echo "${success_message}: ${result}"
    else
        echo "Command failed: ${result}"
        exit 1
    fi
}

# Function to check if PostgreSQL command line tool is available
is_psql_available() {
    if ! command -v psql > /dev/null; then
        echo "psql command not found. PostgreSQL setup cannot proceed."
        return 1
    fi
    return 0
}

# Function to parse DB_URI and extract credentials
parse_db_uri() {
    local db_uri="$1"
    local -r db_host=$(echo "$db_uri" | sed -e 's/jdbc:postgresql:\/\///' -e 's/\(.*\):[0-9]*\/.*/\1/')
    local -r db_port=$(echo "$db_uri" | sed -e 's/.*://' -e 's/\/.*//')
    local -r db_name=$(echo "$db_uri" | sed -e 's/.*\///' -e 's/[?].*//')
    local -r db_user=$(echo "$db_uri" | grep -o 'user=[^&]*' | cut -d '=' -f 2)
    local -r db_password=$(echo "$db_uri" | grep -o 'password=[^&]*' | cut -d '=' -f 2)

    # Echo the parsed values so they can be read in by a caller
    echo "$db_host $db_port $db_name $db_user $db_password"
}

# Function to check for non-default credentials
confirm_db_name_and_credentials() {
    if [[ "$DB_NAME" != "manifold" || "$DB_USER" != "manifold" || "$DB_PASSWORD" != "manifold" ]]; then
        if [ "$FORCE" = false ]; then
            echo "Non-default database settings detected."
            echo "Skipping database creation. Use --force to override."
            return 1
        fi
    fi
}

# Function to check if PostgreSQL server version is between 13.8 and 15
check_postgres_version() {
    if ! is_psql_available; then
        return 1
    fi

    local pg_version
    pg_version=$(psql -t -c "SELECT version();" | awk '/PostgreSQL/ {print $2}')

    if [[ -z "$pg_version" ]]; then
        echo "Unable to determine PostgreSQL server version."
        return 1
    fi

    echo "PostgreSQL server version found: $pg_version"

    # Extract major and minor version components
    local major_version minor_version
    IFS='.' read -r major_version minor_version <<< "$pg_version"

    # Check if version is within the specified range

    if (( major_version == 13 && minor_version >= 8 )) || (( major_version >= 14 && major_version <= 15 )); then
        echo "PostgreSQL server version is within the recommended range."
        PG_VERSION_CHECK_RESULT=true
        return 0
    else
        echo "PostgreSQL server version is outside the recommended range."
        PG_VERSION_CHECK_RESULT=false
        return 1
    fi
}

# Function to check if LocalStack is running
check_localstack() {
    local localstack_url=$1
    if curl --silent "$localstack_url/_localstack/health" > /dev/null; then
        echo "LocalStack is running at $localstack_url"
        return 0
    else
        echo "LocalStack is not running at $localstack_url."
        echo "Please ensure LocalStack is installed and running. Visit https://localstack.cloud for installation instructions."
        return 1
    fi
}

# Function to set up PostgreSQL Database
setup_postgres_database() {
    if ! is_psql_available; then
        return 1
    fi

    read -r DB_HOST DB_PORT DB_NAME DB_USER DB_PASSWORD <<< "$(parse_db_uri "$DB_URI_RW")"

    confirm_db_name_and_credentials || return 0

    # Check if the database exists
    if psql -lqt | cut -d \| -f 1 | grep -qw "$DB_NAME"; then
        echo "Database '$DB_NAME' already exists."
    else
        # Create the database
        psql -c "CREATE DATABASE $DB_NAME;"

        # Check if user exists, create if doesn't exist
        if psql -tAc "SELECT 1 FROM pg_roles WHERE rolname='$DB_USER'" | grep -q 1; then
            echo "User '$DB_USER' already exists."
        else
            psql -c "CREATE USER $DB_USER WITH PASSWORD '$DB_PASSWORD';"
        fi

        # Grant privileges
        psql -c "GRANT ALL PRIVILEGES ON DATABASE $DB_NAME TO $DB_USER;"
        psql -c "GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA public TO $DB_USER;"
        psql -c "GRANT ALL PRIVILEGES ON DATABASE $DB_NAME TO $DB_USER;"


        echo "Database '$DB_NAME' created and privileges granted to user '$DB_USER'."
    fi
}

# Function to check if PostgreSQL is running
check_postgres() {
    if ! is_psql_available; then
        echo "psql command not available."
        return 1
    fi

    # Parse the DB_URI_RW to get database credentials
    read -r DB_HOST DB_PORT DB_NAME DB_USER DB_PASSWORD <<< "$(parse_db_uri "$DB_URI_RW")"

    # Construct the standard PostgreSQL URI from the parsed credentials
    local db_uri_standard="postgresql://${DB_USER}:${DB_PASSWORD}@${DB_HOST}:${DB_PORT}/${DB_NAME}"

    # Test the PostgreSQL connection
    if psql "$db_uri_standard" -c '\q' 2> /dev/null; then
        echo "Successfully connected to PostgreSQL at ${DB_HOST}:${DB_PORT}/${DB_NAME}"
        return 0
    else
        echo "Failed to connect to PostgreSQL at ${DB_HOST}:${DB_PORT}/${DB_NAME}"
        echo "Please ensure PostgreSQL is installed and running. Visit https://postgresapp.com for macOS or https://www.postgresql.org/download/ for other platforms."
        return 1
    fi
}

# Function to check if AWS CLI is available
check_aws_cli_available() {
    if ! command -v aws > /dev/null; then
        echo "AWS CLI is not installed. Visit https://aws.amazon.com/cli/ for installation instructions."
        return 1
    fi
    return 0
}

# Function to create AWS SNS topic
create_sns_topic() {
    run_aws_command aws sns create-topic --name mdtopic "${aws_cli_common_args[@]}" "SNS Topic created"
}

# Function to subscribe SQS queue to SNS topic
subscribe_sqs_to_sns() {
    local -r sns_topic_arn=$(aws sns list-topics "${aws_cli_common_args[@]}" | grep "mdtopic" | awk '{print $2}' | tr -d '"')
    local -r sqs_queue_arn=$(aws sqs get-queue-attributes --queue-url "$(aws sqs get-queue-url --queue-name manifold-mainq "${aws_cli_common_args[@]}" --output text)" --attribute-names QueueArn "${aws_cli_common_args[@]}" --query 'Attributes.QueueArn' --output text)

    run_aws_command aws sns subscribe --topic-arn "$sns_topic_arn" --protocol sqs --notification-endpoint "$sqs_queue_arn" "${aws_cli_common_args[@]}" "SQS Queue subscribed to SNS Topic"
}

# Function to set up infrastructure
setup_infrastructure() {
    if ! check_aws_cli_available; then
        return 1
    fi

    local aws_cli_common_args=(--endpoint-url "$AWS_ENDPOINT_OVERRIDE" --region "$AWS_REGION")

    # Create SQS Queues and handle output
    run_aws_command aws sqs create-queue --queue-name manifold-mainq "${aws_cli_common_args[@]}" "SQS Queue created"
    run_aws_command aws sqs create-queue --queue-name manifold-dlq "${aws_cli_common_args[@]}" "DLQ Queue created"
    run_aws_command aws sqs create-queue --queue-name manifold-renderingq "${aws_cli_common_args[@]}" "Rendering Queue created"

    # Create S3 Bucket and handle output
    run_aws_command aws s3api create-bucket --bucket render-bucket "${aws_cli_common_args[@]}" "S3 Bucket created"

    # Create SNS Topic and subscribe SQS Queue
    create_sns_topic
    subscribe_sqs_to_sns
}

# Create IntelliJ run config from a template
generate_intellij_run_config() {
    local dist_file=".run/$1"
    local target_file=".run/$2"

    echo "dist_file $dist_file"
    echo "target_file $target_file"

    # Check if the distribution file exists
    if [ ! -f "$dist_file" ]; then
        echo "$dist_file does not exist. Unable to generate run configuration."
        return 1
    fi

    # Check if the target file already exists
    if [ -f "$target_file" ]; then
        echo "Run configuration $target_file already exists. Generation skipped."
        return 0
    fi

    # Copy the .dist file to create a new configuration file
    cp "$dist_file" "$target_file"

    # Escape special characters for sed and for XML
    # shellcheck disable=SC2001
    local -r escaped_db_rw_uri=$(echo "$DB_URI_RW" | sed 's|&|\\&amp;|g')
    # shellcheck disable=SC2001
    local -r escaped_db_ro_uri=$(echo "$DB_URI_RO" | sed 's|&|\\&amp;|g')

    # Replace placeholders with actual environment variable values
    sed -i '' "s|%AWS_ACCESS_KEY_ID%|$AWS_ACCESS_KEY_ID|g" "$target_file"
    sed -i '' "s|%AWS_ENDPOINT_OVERRIDE%|$AWS_ENDPOINT_OVERRIDE|g" "$target_file"
    sed -i '' "s|%AWS_REGION%|$AWS_REGION|g" "$target_file"
    sed -i '' "s|%AWS_SECRET_ACCESS_KEY%|$AWS_SECRET_ACCESS_KEY|g" "$target_file"
    sed -i '' "s|%DB_URI_RW%|$escaped_db_rw_uri|g" "$target_file"
    sed -i '' "s|%DB_URI_RO%|$escaped_db_ro_uri|g" "$target_file"

    echo "Run configuration generated: $target_file"
    echo "You may need to restart IntelliJ to load the new config."
}

# Function to display environment variables for IntelliJ
display_env_variables_for_intellij() {
    echo "Environment variables for IntelliJ's Run Configuration: "
    echo -n "AWS_ACCESS_KEY_ID=$AWS_ACCESS_KEY_ID; "
    echo -n "AWS_SECRET_ACCESS_KEY=$AWS_SECRET_ACCESS_KEY; "
    echo -n "AWS_REGION=$AWS_REGION; "
    echo -n "AWS_ENDPOINT_OVERRIDE=$AWS_ENDPOINT_OVERRIDE; "
    echo "DB_URI_RW=$DB_URI_RW;"
    echo "DB_URI_RO=$DB_URI_RO;"
}

# Main function
main() {
      FORCE=false
      GENERATE_CONFIG=true

      for arg in "$@"
      do
          case $arg in
            --force)
                FORCE=true
                shift # Remove --force from processing
                ;;
            --skip-generate-config)
                GENERATE_CONFIG=false
                shift # Remove --skip-generate-config from processing
                ;;
          esac
      done

    # Perform PostgreSQL version check
    check_postgres_version

    if ! $PG_VERSION_CHECK_RESULT && [ "$FORCE" = false ]; then
        echo "Skipping database setup - use --force to override."
    else
        if ! $PG_VERSION_CHECK_RESULT && [ "$FORCE" = true ]; then
            echo "--force specified, continuing with database setup."
        fi
        setup_postgres_database
        check_postgres "$DB_URI_RW"
        check_postgres "$DB_URI_RO"
    fi


    if check_localstack "$AWS_ENDPOINT_OVERRIDE"; then
        setup_infrastructure
    fi

    # Check if run configuration generation is requested
    if [ "$GENERATE_CONFIG" = true ]; then
        generate_intellij_run_config "Manifold.run.xml.dist" "Manifold.run.xml"
        generate_intellij_run_config "RunCucumberTests.run.xml.dist" "RunCucumberTests.run.xml"
        generate_intellij_run_config "RunUnitTests.run.xml.dist" "RunUnitTests.run.xml"
    fi
    display_env_variables_for_intellij
}

# Check if the script is being run directly
if [[ "${BASH_SOURCE[0]}" == "${0}" ]]; then
    main "$@"
fi